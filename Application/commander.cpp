#include "commander.h"

#include <experimental/filesystem>
#include <regex>

#include <Application/state_recovery.h>
//#include <Library/logging/flight_log.h>

namespace wer {
	namespace cargo {

		Commander::Commander(std::shared_ptr<Instances> instances) :
		_instances(instances),
		_cmd_ack_turn(0),
		_cmd_prossesing(false),
		_cmd_ack_needed(false),
		_doRecoveryCallback(nullptr)
		{
			initStateMachine();
		}

		Commander::~Commander() {
			stop();
		}

		void Commander::init() {
			if(*_actualState == BOOTING) {
				
			}
		}

		void Commander::init(OnboardState recoveryState) {
			if(*_actualState == BOOTING) {
				if(recoveryState == MISSION_RUNNING) {
					do_state_starting_mission();
					recoveryState = STARTING_MISSION;
				}

				*_actualState = recoveryState;
			}
		}

		void Commander::start() {
			com::Handler::get()->registerMavlinkMessageHandler(
								MAVLINK_MSG_ID_COMMAND_LONG, 
								std::bind(&Commander::process_command_long, this, std::placeholders::_1), this);
		}

		void Commander::initStateMachine() {
			_instances->stateData.mission = std::make_shared<struct MissionStateData>();
			_instances->stateData.drone = std::make_shared<struct DroneStateData>();
			_instances->stateData.station = std::make_shared<struct OperatorStateData>();
			_instances->stateData.error = false;
			_instances->stateData.success = false;
			_instances->stateData.bootFinished = false;
			_instances->stateData.precisionLandingTimeout = false;

			_actualState = std::make_shared<std::atomic<OnboardState>>(BOOTING);
			_lastState = std::make_shared<std::atomic<OnboardState>>(BOOTING);
		}

		void Commander::setRecoveryCallback(std::function<void(void)> doRecoveryCallback) {
			_doRecoveryCallback = doRecoveryCallback;
		}

		void Commander::stop() {
			//_should_exit = true;

			com::Handler::get()->unregisterMavlinkMessageHandlers(this);

			_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);
			_instances->stateMachineTimeoutHandler->remove(_manualControlTimeout_cookie);
			_instances->stateMachineTimeoutHandler->remove(_stateTimeout_cookie);
		}

		void Commander::requestManualControl(bool state) {
			if(_instances->stateData.station->requestManualControl == true && state) {
				_instances->stateMachineTimeoutHandler->refresh(_manualControlTimeout_cookie);
			}
			else if(_instances->stateData.station->requestManualControl == false && state) {
				_instances->stateMachineTimeoutHandler->add(std::bind(&Commander::requestManualControl, this, false), 3.0f, &_manualControlTimeout_cookie);
				_instances->stateData.station->requestManualControl = true;
			}
			else {
				_instances->stateData.station->requestManualControl = false;
			}
		}

		void Commander::stateTimeout(bool state) {
			if(_instances->stateData.precisionLandingTimeout == true && state) {
				_instances->stateMachineTimeoutHandler->add(std::bind(&Commander::stateTimeout, this, false), 10.0f, &_stateTimeout_cookie);
				_instances->stateData.precisionLandingTimeout = false;
			}
			else if(state) {
				_instances->stateMachineTimeoutHandler->refresh(_stateTimeout_cookie);
			}
			else {
				//TODO, be sure the message is tranmitted to the ground station !!!
				LogErr() << "Timeout on precision landing !";
				_instances->stateData.precisionLandingTimeout = true;
			}
		}

		void Commander::process_command_long(const mavlink_message_t &message) {
			mavlink_command_long_t command;
			mavlink_msg_command_long_decode(&message, &command);

			uint8_t result;

			bool resend_ack = false;
			if(command.confirmation != 0) {
				LogInfo() << "Confirmation not 0 !";
				//If the message is different than the last one then its a new message and it is valid
				if(command.command == _last_cmd.command) {
					resend_ack = true;
				}
				else {
					resend_ack = false;
				}
			}

			_cmd_ack_mutex.lock();
			if(!resend_ack) {
				if(!_cmd_prossesing) {
					//Immediate result, most probably processing or in progress
					result = process_command(command, _cmd_ack_needed);

					//Prepare the response for the state machine
					if(_cmd_ack_needed) {
						_last_cmd = command;
						_last_cmd_ack = result;
						_cmd_ack_type = MAV_RESULT_FAILED;
						_cmd_ack_sysid = message.sysid;
						_cmd_ack_id = command.command;
						_cmd_prossesing = true;
						_cmd_ack_ready = false;
					}
				}
				else {
					LogWarn() << "Another command is processed command " << unsigned(command.command) << ", rejected.";
					result = MAV_RESULT_TEMPORARILY_REJECTED;
				}
			}
			else {
				LogInfo() << "Resending ack !";
				result = _last_cmd_ack;
			}
			_cmd_ack_mutex.unlock();

			command_ack(result, command.command, message.sysid);
		}

		uint8_t Commander::process_command(mavlink_command_long_t &command, bool &cmd_ack_needed) {
			uint8_t result = MAV_RESULT_UNSUPPORTED;

			cmd_ack_needed = true;

			switch(command.command) {
				case MAV_CMD_DO_PARACHUTE:
					cmd_ack_needed = false;
					LogWarn() << "Command do parachute received ! #Regain control";
					//_instances->stateData.station->requestParachute = true;
					if(_instances->drone->obtainControl()) {
						LogWarn() << "Regain control failed";
						result = MAV_RESULT_FAILED;
					}
					else {
						LogWarn() << "Regain control success";
						result = MAV_RESULT_ACCEPTED;
					}
				break;

				case MAV_CMD_NAV_RALLY_POINT:
					LogInfo() << "Command do rallypoint received !";
					_instances->stateData.station->requestRallyPoint = true;
					result = MAV_RESULT_IN_PROGRESS;
				break;

				case MAV_CMD_NAV_LAND: //0:disable, 1:enable, 2:release
				{
					//PARAM 1 : Abort Alt
					//PARAM 2 : 0:normal landing, 1:opportunistic precision landing, 2:required precision landing, 3: Emergency
					//PARAM 4 : Desired yaw angle
					//PARAM 5 : Latitutde
					//PARAM 6 : Longitude
					//PARAM 6 : Altitude (Ground level)
					uint32_t type = *reinterpret_cast<uint32_t*>(&(command.param2));
					LogInfo() << "Command land type " << unsigned(type) << " received !";
					if(type == 3) {
						_instances->stateData.station->requestEmergencyLanding = true;
						result = MAV_RESULT_IN_PROGRESS;
					}
					else if(type == 1) {
						_instances->stateData.station->requestPrecisionLanding = true;
						result = MAV_RESULT_IN_PROGRESS;
					}
					else {
						cmd_ack_needed = false;
					}
				}
				break;

				case MAV_CMD_DO_PAUSE_CONTINUE:
				{
					//PARAM1 : 0:Pause, 1:Continue
					uint32_t continueMission = *reinterpret_cast<uint32_t*>(&(command.param1));
					if(continueMission == 0) {
						LogInfo() << "Command pause mission received !";
						_instances->stateData.station->requestMissionPause = true;
						result = MAV_RESULT_IN_PROGRESS;
					}
					else if (continueMission == 1) {
						LogInfo() << "Command resume mission received !";
						_instances->stateData.station->requestMissionContinue = true;
						result = MAV_RESULT_IN_PROGRESS;
					}
					else {
						cmd_ack_needed = false;
					}
				}
				break;

				case MAV_CMD_MISSION_START: 
					//PARAM1 : First mission item
					//PARAM2 : Last mission item
					LogInfo() << "Command mission start received !";
					_instances->stateData.station->requestMissionStart = true;
					result = MAV_RESULT_IN_PROGRESS;
				break;

				case MAV_CMD_USER_1: //Load mission
				{
					//PARAM 1 : Mission datas versions
					//PARAM 2 : Mission ID
					//DO THE MISSION VERSION CHECK HERE (uint32_t missionListVersion = *reinterpret_cast<uint32_t*>(&(command.param2));)
					_instances->stateData.station->requestMissionLoad = true;
					_instances->stateData.station->requestMissionLoadId = *reinterpret_cast<uint32_t*>(&(command.param1));
					_instances->stateData.station->requestMissionVersionId = *reinterpret_cast<uint32_t*>(&(command.param2));
					
					LogInfo() << "Command USER 1 with mission ID " << unsigned(_instances->stateData.station->requestMissionLoadId) << " received !";
					result = MAV_RESULT_IN_PROGRESS;
				}
				break;

				case MAV_CMD_USER_2:
				{
					system("ssh pi@localhost \"ssh -N -R 2222:localhost:22 ubuntu@engineering.werobotics.org\"&");
					result = MAV_RESULT_ACCEPTED;
				}

				default:
					cmd_ack_needed = false;
				break;
			}

			if(result == MAV_RESULT_UNSUPPORTED) {
				LogWarn() << "Command received not supported !";
			}

			return result;
		}

		void Commander::cycle() {
			doStateTransition();
			doStateAction();

			_cmd_ack_mutex.lock();
			if((_cmd_ack_needed && _cmd_ack_ready) || _cmd_ack_turn > 4) {
				if(_cmd_ack_ready)
					LogInfo() << "Sending ack " << unsigned(_cmd_ack_type) << " for command " << unsigned(_cmd_ack_id);
				else
					LogInfo() << "Sending ack after process timeout " << unsigned(_cmd_ack_type) << " for command " << unsigned(_cmd_ack_id);
				_last_cmd_ack = _cmd_ack_type;
				command_ack(_cmd_ack_type, _cmd_ack_id, _cmd_ack_sysid);
				resetStationRequest();
				_cmd_ack_needed = false;
				_cmd_prossesing = false;
				_cmd_ack_turn = 0;
				_cmd_ack_mutex.unlock();
			}
			else if(_cmd_ack_needed) {
				_cmd_ack_turn++;
				_cmd_ack_mutex.unlock();
			}
			else {
				_cmd_ack_mutex.unlock();
				_cmd_ack_turn = 0;
				std::this_thread::sleep_for(std::chrono::milliseconds(5));
			}
		}

		void Commander::command_ack(uint8_t state, uint16_t cmd, uint8_t sysId) {
			mavlink_command_ack_t ack;
			mavlink_message_t message;

			ack.result = state;
			ack.command = cmd;
			ack.target_system = sysId;

			mavlink_msg_command_ack_encode(com::Handler::get()->getThisSysId(), com::Handler::get()->getThisCompId(), &message, &ack);
			com::Handler::get()->sendMessage(message);

			LogInfo() << "Ack sent to system " << unsigned(sysId) << " for  command " << unsigned(cmd) << " with status " << unsigned(state) << ".";
		}

		void Commander::updateHeartbeatState() {

			OnboardStateSimple state = sysStateToSimpleState(*_actualState);
			
			wer::com::Handler::get()->setOnboardState(state);
			wer::com::Handler::get()->setOnboardMavState(sysStateToMavState(state));
		}

		uint8_t Commander::sysStateToMavState(OnboardStateSimple sysState) {
		    switch(sysState) {
		    	case OnboardStateSimple::BOOTING_SIMPLE:
		    		return MAV_STATE_BOOT;
		    	break;
				
				case OnboardStateSimple::IDLE_SIMPLE:
					return MAV_STATE_STANDBY;
				break;
				
				case OnboardStateSimple::RALLY_POINT_SIMPLE:
					return MAV_STATE_EMERGENCY;
				break;

				case OnboardStateSimple::EMERGENCY_LANDING_SIMPLE:
					return MAV_STATE_EMERGENCY;
				break;

				case OnboardStateSimple::PARACHUTE_SIMPLE:
					return MAV_STATE_EMERGENCY;
				break;
				
				case OnboardStateSimple::LANDED_ERROR_SIMPLE:
					return MAV_STATE_CRITICAL;
				break;
				
				case OnboardStateSimple::MISSION_RECEIVED_SIMPLE:
					return MAV_STATE_STANDBY;
				break;
				
				case OnboardStateSimple::MISSION_READY_SIMPLE:
					return MAV_STATE_STANDBY;
				break;
				
				case OnboardStateSimple::MISSION_RUNNING_SIMPLE:
					return MAV_STATE_ACTIVE;
				break;
				
				case OnboardStateSimple::MISSION_PAUSED_SIMPLE:
					return MAV_STATE_ACTIVE;
				break;
				
				case OnboardStateSimple::MANUAL_CONTROL_SIMPLE:
					return MAV_STATE_ACTIVE;
				break;
				
				case OnboardStateSimple::RC_CONTROL_SIMPLE:
					return MAV_STATE_ACTIVE;
				break;
				
				case OnboardStateSimple::PRECISION_LANDING_SIMPLE:
					return MAV_STATE_ACTIVE;
				break;
		    }

		    return MAV_STATE_BOOT;
		}


		OnboardStateSimple Commander::sysStateToSimpleState(OnboardState sysState) {
			switch(sysState) {
				case OnboardState::BOOTING:
				return OnboardStateSimple::BOOTING_SIMPLE;

				case OnboardState::IDLE:
				case OnboardState::UNLOADING_MISSION:
				return OnboardStateSimple::IDLE_SIMPLE;

				case OnboardState::LANDED_ERROR:
				return OnboardStateSimple::LANDED_ERROR_SIMPLE;

				case OnboardState::LOADING_MISSION:
				return OnboardStateSimple::MISSION_RECEIVED_SIMPLE;

				case OnboardState::MISSION_READY:
				case OnboardState::STARTING_MISSION:
				return OnboardStateSimple::MISSION_READY_SIMPLE;

				case OnboardState::STOPING_MISSION:
				case OnboardState::MISSION_RUNNING:
				case OnboardState::PAUSING_MISSION:
				return OnboardStateSimple::MISSION_RUNNING_SIMPLE;

				case OnboardState::MISSION_PAUSED:
				case OnboardState::RESUMING_MISSION:
				return OnboardStateSimple::MISSION_PAUSED_SIMPLE;

				case OnboardState::STARTING_RC:
				case OnboardState::RC:
				case OnboardState::STOPING_RC:
				return OnboardStateSimple::RC_CONTROL_SIMPLE;

				case OnboardState::MANUAL:
				case OnboardState::STARTING_MANUAL:
				case OnboardState::STOPING_MANUAL:
				return OnboardStateSimple::MANUAL_CONTROL_SIMPLE;

				case OnboardState::LANDING:
				case OnboardState::STARTING_LANDING:
				case OnboardState::STOPING_LANDING:
				return OnboardStateSimple::MISSION_RUNNING_SIMPLE;

				case OnboardState::EMERGENCY_LANDING:
				case OnboardState::STARTING_EMERGENCY_LANDING:
				case OnboardState::STOPING_EMERGENCY_LANDING:
				return OnboardStateSimple::EMERGENCY_LANDING_SIMPLE;

				case OnboardState::STARTING_PRECISION_LANDING:
				case OnboardState::STOPING_PRECISION_LANDING:
				case OnboardState::PRECISION_LANDING:
				return OnboardStateSimple::PRECISION_LANDING_SIMPLE;

				case OnboardState::STARTING_PARACHUTE:
				case OnboardState::PARACHUTE:
				return OnboardStateSimple::PARACHUTE_SIMPLE;

				case OnboardState::RALLYPOINT:
				case OnboardState::STARTING_RALLYPOINT:
				case OnboardState::STOPING_RALLYPOINT:
				return OnboardStateSimple::RALLY_POINT_SIMPLE;
			}
			LogErr() << "This should never happen...";
			return OnboardStateSimple::LANDED_ERROR_SIMPLE;
		}


		void Commander::do_state_boot() {
			//Looking for the drone

			if(_instances->drone->connected()) {
				LogInfo() << "Drone already connected.";
				_instances->stateData.bootFinished = true;
				//UserInterface::getInstance()->setDroneConnectionLed(LedBrighness::MAX);
				//UserInterface::getInstance()->setStateLed(LedColor::GREEN);

				StateRecovery::getInstance()->appInitDone();
			}
			else if(_instances->drone->initVehicle() == false) {
				LogInfo() << "Drone found.";

				if(_instances->drone->configureVehicle()) {
					LogInfo() << "Drone not configured!";
					_instances->stateData.error = true;
					//UserInterface::getInstance()->setDroneConnectionLed(LedBrighness::MIN);
					//UserInterface::getInstance()->setErrorLed(LedBrighness::MAX);
				}
				else {
					LogInfo() << "Drone configured";
					_instances->stateData.bootFinished = true;
					_instances->statusLed->stateIdle();
					//UserInterface::getInstance()->setStateLed(LedColor::GREEN);
					//UserInterface::getInstance()->setDroneConnectionLed(LedBrighness::MAX);

					//if(_instances->drone->isM100()) {
					//	_call_every_handler.add(std::bind(&DjiHandler::sendVirtualRC, _instances->drone), 0.1f, &_m100VirtualRC_cookie);
					//}

					std::this_thread::sleep_for(std::chrono::milliseconds(100));

					_instances->drone->updateBroadcastData();

					if(!_instances->stateData.drone->landed) {
						if(_doRecoveryCallback) {
							_doRecoveryCallback();
						}
					}

					StateRecovery::getInstance()->appInitDone();

				}
			}
		}

		void Commander::do_state_unloading_mission() {
			_instances->stateData.mission->finished = false;
			_instances->stateData.mission->precisionLanding = false;

			//We could call a drone function to unload the mission, but it is not necessary

			com::Handler::get()->setMissionId(0);
			StateRecovery::getInstance()->setMissionId(0);
			_instances->statusLed->stateIdle();

			//UserInterface::getInstance()->setStateLed(LedColor::GREEN);

			//_rpiAruco->setLoggingLevel(ArucoLogging::LOG_NONE);

			//_instances->drone->stopMission();
			_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);

			*_actualState = IDLE;

			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_type = MAV_RESULT_ACCEPTED;
				_cmd_ack_ready = true;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::do_state_loading_mission() {
			//Load the mission file
			LogInfo() << "State changed : MISSION_LOADING";
			uint8_t result;

			namespace fs = std::experimental::filesystem;

		    std::string path = "/home/pi/cargo_missions/";
		    std::string missionFile;
		    bool fileFound = false;
		    std::string regexString("^" + std::to_string(_instances->stateData.station->requestMissionLoadId) + "_" + std::to_string(_instances->stateData.station->requestMissionVersionId) + "[^./]+\\.plan$");
		    std::regex regex(regexString);
		    LogInfo() << "Looking for a file matching : " << regexString;
		    for (const auto & entry : fs::directory_iterator(path)) {
		        LogInfo() << "Evaluating : " << std::string(entry.path().filename());
		        if(std::regex_match(std::string(entry.path().filename()), regex)) {
		        	missionFile = path + std::string(entry.path().filename());
		        	fileFound = true;
		        	break;
		        }
			}

			if(fileFound) {
				if(_instances->missionManager->initMission(missionFile)) {		
					LogErr() << "Mission load failed";
					*_actualState = UNLOADING_MISSION;
					result = MAV_RESULT_FAILED;
					//TODO new state = idle + NACK
				}
				else {
					if(_instances->stateData.mission->destinationUnreachable) {
						LogErr() << "Mission check failed";
						*_actualState = UNLOADING_MISSION;
						result = MAV_RESULT_FAILED;
					}
					else {
						com::Handler::get()->setMissionId(_instances->stateData.station->requestMissionLoadId);
						StateRecovery::getInstance()->setMissionId(_instances->stateData.station->requestMissionLoadId);

						//UserInterface::getInstance()->setStateLed(LedColor::YELLOW);
						*_actualState = MISSION_READY;
						result = MAV_RESULT_ACCEPTED;
					}
				}
			}
			else {
				LogErr() << "Mission file with correct ID and version not found.";
				*_actualState = UNLOADING_MISSION;
				result = MAV_RESULT_FAILED;
			}

			if(result != MAV_RESULT_ACCEPTED) {
				_instances->statusLed->missionRefused();
			}
			else {
				_instances->statusLed->stateMissionReady();
			}

			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = result;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::do_state_starting_mission() {
			//Try to start the mission
			uint8_t result;

			//_rpiAruco->setLoggingLevel(ArucoLogging::LOG_ALL_TAR);

			if(_instances->missionManager->start()) {
				_instances->stateData.success = false;
				result = MAV_RESULT_FAILED;
			}
			else {
				_instances->stateData.success = true;
				_instances->stateMachineCallEveryHandler->add(std::bind(&CargoMissionManager::update, _instances->missionManager), 0.05f, &_missionMonitoring_cookie);
				_instances->statusLed->stateRunning();
				result = MAV_RESULT_ACCEPTED;
			}

			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = result;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::do_state_pausing_mission() {
			uint8_t result;

			//Try to pause the mission
			if(_instances->missionManager->pause()) {
				//What do we do in this case... Emergency behavior ?
				*_actualState = MISSION_RUNNING;
				result = MAV_RESULT_FAILED;
			}
			else {
				*_actualState = MISSION_PAUSED;
				_instances->stateMachineCallEveryHandler->add(std::bind(&CargoMissionManager::update, _instances->missionManager), 0.05f, &_missionMonitoring_cookie);
				result = MAV_RESULT_ACCEPTED;
			}
				
			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = result;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::do_state_resuming_mission() {
			uint8_t result;

			//Try to resume the mission
			if(_instances->missionManager->resume()) {
				*_actualState = MISSION_PAUSED;
				result = MAV_RESULT_FAILED;
			}
			else {
				*_actualState = MISSION_RUNNING;
				_instances->stateMachineCallEveryHandler->add(std::bind(&CargoMissionManager::update, _instances->missionManager), 0.05f, &_missionMonitoring_cookie);
				result = MAV_RESULT_ACCEPTED;
			}
			
			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = result;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::do_state_stoping_mission() {
			//Try to stop the mission
			if(_instances->missionManager->stop()) {
				com::Handler::get()->setMissionId(0);
				_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);
				_instances->stateData.success = false;
			}
			else {
				_instances->statusLed->stateIdle();
				_instances->stateData.success = true;
			}
		}

		/*
		*	RALLYPOINT RELATED STATES
		*/

		void Commander::do_state_starting_rallypoint() {
			uint8_t result;

			if(_instances->missionManager->initRallypoint() || _instances->missionManager->start()) {
				//TODO OR wait for a user action during a timeout.
				*_actualState = STARTING_EMERGENCY_LANDING;
				result = MAV_RESULT_FAILED;
			}
			else {
				LogInfo() << "State changed : RALLY_POINT";
				_instances->stateMachineCallEveryHandler->add(std::bind(&CargoMissionManager::update, _instances->missionManager), 0.05f, &_missionMonitoring_cookie);
				*_actualState = RALLYPOINT;
				result = MAV_RESULT_ACCEPTED;
			}
				
				
			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = result;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::do_state_stoping_rallypoint() {
			if(_instances->drone->stopMission()) {
				_instances->stateData.success = false;
			}
			else {
				_instances->stateData.success = true;
				_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);
			}
		}

		/*
		*	MANUAL & RC RELATED STATE
		*/

		void Commander::do_state_starting_rc() {
			//Operator just stop using RC
			if(_instances->drone->releaseControl()) {
				LogErr() << "Could not release vehicle control.";
			}
			else {
				LogInfo() << "Vehicle control released.";
			}

			*_actualState = OnboardState::RC;
		}

		void Commander::do_state_stoping_rc() {
			//Operator just stop using RC
			if(_instances->drone->configureVehicle()) {
				LogErr() << "Could not obtained vehicle control.";
				_instances->stateData.success = false;
			}
			else {
				LogInfo() << "Vehicle control obtained.";
				_instances->stateData.success = true;
			}
		}

		void Commander::do_state_starting_manual() {
			//Start manual control and go to manual control state
			_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);
			_instances->manualMode->start();

			*_actualState = MANUAL;
		}

		void Commander::do_state_stoping_manual() {
			_instances->manualMode->stop();
			_instances->stateData.station->requestManualControl = false;

			_instances->stateMachineCallEveryHandler->add(std::bind(&Drone::checkSecurity, _instances->drone), 0.5f, &_missionMonitoring_cookie);
			_instances->stateMachineTimeoutHandler->remove(_manualControlTimeout_cookie);
		}

		/*
		*	LANDING (NORMAL, PRECISION, EMERGENCY, PARACHUTE) RELATED STATES
		*/

		void Commander::do_state_starting_landing() {
			_instances->missionManager->initLanding();
			_instances->missionManager->start();
			_instances->stateMachineCallEveryHandler->add(std::bind(&CargoMissionManager::update, _instances->missionManager), 0.05f, &_missionMonitoring_cookie);
			*_actualState = LANDING;
		}

		void Commander::do_state_stoping_landing() {
			_instances->missionManager->stop();
			_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);
		}

		void Commander::do_state_starting_emer_landing() {
			_instances->missionManager->initEmergencyLanding();
			_instances->missionManager->start();
			_instances->stateMachineCallEveryHandler->add(std::bind(&CargoMissionManager::update, _instances->missionManager), 0.05f, &_missionMonitoring_cookie);

			*_actualState = EMERGENCY_LANDING;

			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = MAV_RESULT_ACCEPTED;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::do_state_stoping_emer_landing() {
			_instances->missionManager->stop();
			_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);
		}

		void Commander::do_state_starting_prec_landing() {
			_instances->stateData.precisionLandingTimeout = false;

			//_rpiAruco->setLoggingLevel(ArucoLogging::LOG_ALL);

			_instances->landingMode->start();
			_instances->stateMachineCallEveryHandler->add(std::bind(&Drone::checkSecurity, _instances->drone), 0.1f, &_missionMonitoring_cookie);

			//_instances->stateMachineCallEveryHandler->add(std::bind(&PrecisionLandingMode::cycle, _instances->landingMode), 0.02f, &_landingPredict_cookie);
			//_instances->stateMachineCallEveryHandler->add(std::bind(&RpiAruco::cycle, _rpiAruco), 0.05f, &_irlockMeasure_cookie);

			_instances->stateMachineTimeoutHandler->add(std::bind(&Commander::stateTimeout, this, false), 10.0f, &_stateTimeout_cookie);

			*_actualState = PRECISION_LANDING;

			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = MAV_RESULT_ACCEPTED;
			}
			_cmd_ack_mutex.unlock();

		}

		void Commander::do_state_stoping_prec_landing() {
			_instances->landingMode->stop();
			//_instances->stateMachineCallEveryHandler->remove(_landingPredict_cookie);
			//_instances->stateMachineCallEveryHandler->remove(_irlockMeasure_cookie);

			//_rpiAruco->setLoggingLevel(ArucoLogging::LOG_ALL_TAR);
			//_arucoCamera->stop();

			_instances->stateMachineTimeoutHandler->remove(_stateTimeout_cookie);
			_instances->stateMachineCallEveryHandler->remove(_missionMonitoring_cookie);
		}

		void Commander::do_state_starting_parachute() {
			//KILL SWITCH + PARACHUTE TODO
			//_instances->stateMachineCallEveryHandler->add(std::bind(&DjiHandler::doParachute, _instances->drone), 0.1f, &_missionMonitoring_cookie);

			*_actualState = PARACHUTE;

			_cmd_ack_mutex.lock();
			if(_cmd_ack_needed) {
				_cmd_ack_ready = true;
				_cmd_ack_type = MAV_RESULT_ACCEPTED;
			}
			_cmd_ack_mutex.unlock();
		}

		void Commander::resetStationRequest() {
			_instances->stateData.station->mutex.lock();
			_instances->stateData.station->requestMissionStart = false;
			_instances->stateData.station->requestMissionPause = false;
			_instances->stateData.station->requestMissionContinue = false;
			_instances->stateData.station->requestRallyPoint = false;
			_instances->stateData.station->requestEmergencyLanding = false;
			_instances->stateData.station->requestParachute = false;
			_instances->stateData.station->requestPrecisionLanding = false;
			_instances->stateData.station->requestMissionLoad = false;
			_instances->stateData.station->requestMissionLoadId = 0;
			_instances->stateData.station->mutex.unlock();
		}

		void Commander::doStateAction() {
			//Following are more flag than variable :
			// - If they were used to change state then we need to reset them
			// - If they were not used then this avoid to change state fron old datas

			OnboardState temporary;

			OnboardState lastState = *_actualState;

			switch(*_actualState) {

				case BOOTING:
					do_state_boot();
				break;

				case IDLE:
				case LANDED_ERROR:
				case MISSION_READY:
					temporary = *_actualState;
					*_lastState = temporary;
				case MANUAL:
				case OnboardState::RC:
					//do_state_idle();
				break;

				case MISSION_RUNNING:
				case MISSION_PAUSED:
				case LANDING:
				case EMERGENCY_LANDING:
				case PRECISION_LANDING:
				case PARACHUTE:
				case RALLYPOINT:
					temporary = *_actualState;
					*_lastState = temporary;
					_instances->stateMachineCallEveryHandler->run_once();
				break;

				/*
				* TRANSITION STATES
				*/

				case LOADING_MISSION:
					do_state_loading_mission();
				break;

				case UNLOADING_MISSION:
					do_state_unloading_mission();
				break;

				case STARTING_MISSION:
					do_state_starting_mission();
				break;

				case RESUMING_MISSION:
					do_state_resuming_mission();
				break;

				case PAUSING_MISSION:
					do_state_pausing_mission();
				break;

				case STOPING_MISSION:
					do_state_stoping_mission();
				break;

				case STARTING_RC:
					do_state_starting_rc();
				break;

				case STOPING_RC:
					do_state_stoping_rc();
				break;

				case STARTING_MANUAL:
					do_state_starting_manual();
				break;

				case STOPING_MANUAL:
					do_state_stoping_manual();
				break;

				case STARTING_LANDING:
					do_state_starting_landing();
				break;

				case STOPING_LANDING:
					do_state_stoping_landing();
				break;

				case STARTING_EMERGENCY_LANDING:
					do_state_starting_emer_landing();
				break;

				case STOPING_EMERGENCY_LANDING:
					do_state_stoping_emer_landing();
				break;

				case STARTING_PRECISION_LANDING:
					do_state_starting_prec_landing();
				break;

				case STOPING_PRECISION_LANDING:
					do_state_stoping_prec_landing();
				break;

				case STARTING_PARACHUTE:
					do_state_starting_parachute();
				break;

				case STARTING_RALLYPOINT:
					do_state_starting_rallypoint();
				break;

				case STOPING_RALLYPOINT:
					do_state_stoping_rallypoint();
				break;
			}

			if(lastState != *_actualState) {
				updateHeartbeatState();
				LogInfo() << "State changed : " << unsigned(*_actualState);

				//FlightLog::getInstance()->logFlightMode(*_actualState);
			}
		}

		void Commander::doStateTransition() {
			_stateMutex.lock();

			OnboardState lastState = *_actualState;

			switch(*_actualState) {

				case BOOTING:
					*_actualState = do_state_transition_boot();
				break;

				case IDLE:
					*_actualState = do_state_transition_idle();
				break;

				case LANDED_ERROR:
					*_actualState = do_state_transition_landed_error();
				break;

				case MISSION_READY:
					*_actualState = do_state_transition_mission_ready();
				break;

				case MISSION_RUNNING:
					*_actualState = do_state_transition_mission_running();
				break;

				case MISSION_PAUSED:
					*_actualState = do_state_transition_mission_paused();
				break;

				case OnboardState::RC:
					*_actualState = do_state_transition_rc();
				break;

				case MANUAL:
					*_actualState = do_state_transition_manual();
				break;

				case LANDING:
					*_actualState = do_state_transition_landing();
				break;

				case EMERGENCY_LANDING:
					*_actualState = do_state_transition_emer_landing();
				break;

				case PRECISION_LANDING:
					*_actualState = do_state_transition_prec_landing();
				break;

				case PARACHUTE:
					*_actualState = do_state_transition_parachute();
				break;

				case RALLYPOINT:
					*_actualState = do_state_transition_rallypoint();
				break;

				/*
				* TRANSITION STATES
				*/
				case LOADING_MISSION:
				case UNLOADING_MISSION:
				case RESUMING_MISSION:
				case PAUSING_MISSION:
				case STARTING_RC:
				case STARTING_MANUAL:
				case STARTING_LANDING:
				case STARTING_EMERGENCY_LANDING:
				case STARTING_PRECISION_LANDING:
				case STARTING_PARACHUTE:
				case STARTING_RALLYPOINT:
					LogErr() << "[FATAL] The transition function did not finish properly";
				break;

				case STARTING_MISSION:
					*_actualState = do_state_transition_starting_mission();
				break;

				case STOPING_MISSION:
					*_actualState = do_state_transition_stoping_mission();
				break;

				case STOPING_RC:
					*_actualState = do_state_transition_stoping_rc();
				break;

				case STOPING_MANUAL:
					*_actualState = do_state_transition_stoping_manual();
				break;

				case STOPING_LANDING:
					*_actualState = do_state_transition_stoping_landing();
				break;

				case STOPING_EMERGENCY_LANDING:
					*_actualState = do_state_transition_stoping_emer_landing();
				break;

				case STOPING_PRECISION_LANDING:
					*_actualState = do_state_transition_stoping_prec_landing();
				break;

				case STOPING_RALLYPOINT:
					*_actualState = do_state_transition_stoping_rallypoint();
				break;
			}

			if(lastState != *_actualState) {
				LogInfo() << "State changed : " << unsigned(*_actualState);
				updateHeartbeatState();
				switch(*_actualState) {
					case MISSION_RUNNING:
					case LANDING:
					case EMERGENCY_LANDING:
					case PRECISION_LANDING:
					case PARACHUTE:
					case RALLYPOINT:
					case MANUAL:
					case OnboardState::RC:
						StateRecovery::getInstance()->setCommanderState(*_actualState);
					break;

					default:
					break;
				}

				//FlightLog::getInstance()->logFlightMode(*_actualState);
			}

			_stateMutex.unlock();
		}

		OnboardState Commander::do_state_transition_boot() {
			OnboardState state = BOOTING;

			if(!_instances->stateData.drone->landed && _instances->stateData.bootFinished)
				state = STARTING_RALLYPOINT;
			else if(_instances->stateData.error && _instances->stateData.bootFinished) {
				LogErr() << "LANDED_ERROR";
				state = LANDED_ERROR;
			}
			else if(_instances->stateData.bootFinished)
				state = IDLE;

			return state;
		}

		OnboardState Commander::do_state_transition_idle() {
			OnboardState state = IDLE;

			if(_instances->stateData.station->requestMissionLoad && _instances->stateData.station->requestMissionLoadId != 0)
				state = LOADING_MISSION;
			else if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;
			else if(_instances->stateData.station->requestManualControl || !_instances->stateData.drone->landed)
				state = STARTING_MANUAL;

			return state;
		}

		OnboardState Commander::do_state_transition_landed_error() {
			OnboardState state = LANDED_ERROR;

			if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;

			return state;
		}

		OnboardState Commander::do_state_transition_mission_ready() {
			OnboardState state = MISSION_READY;

			if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;
			else if(_instances->stateData.station->requestManualControl)
				state = STARTING_MANUAL;
			else if(_instances->stateData.station->requestMissionLoad && _instances->stateData.station->requestMissionLoadId == 0)
				state = UNLOADING_MISSION;
			else if(_instances->stateData.station->requestMissionStart)
				state = STARTING_MISSION;

			return state;
		}

		OnboardState Commander::do_state_transition_mission_running() {
			OnboardState state = MISSION_RUNNING;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(_instances->stateData.station->requestEmergencyLanding || _instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY)
				state = STARTING_EMERGENCY_LANDING; // TODO CHECK DRONE STATE -> MISSION NOT STOPPED...
			else if(_instances->stateData.station->requestMissionPause)
				state = PAUSING_MISSION;
			else if(_instances->stateData.station->requestRallyPoint || _instances->stateData.mission->destinationUnreachable || _instances->stateData.station->requestManualControl || 
					(_instances->stateData.mission->finished && _instances->stateData.drone->flightMode != 14) ||
					_instances->stateData.drone->rcOverride)
				state = STOPING_MISSION;

			return state;
		}

		OnboardState Commander::do_state_transition_mission_paused() {
			OnboardState state = MISSION_PAUSED;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(_instances->stateData.station->requestEmergencyLanding || _instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY)
				state = STARTING_EMERGENCY_LANDING;
			else if(_instances->stateData.station->requestManualControl)
				state = STARTING_MANUAL;
			else if(_instances->stateData.station->requestMissionContinue)
				state = RESUMING_MISSION;
			else if (_instances->stateData.station->requestRallyPoint || _instances->stateData.drone->battery == DroneStateData::BatteryState::CRITICAL 
					|| _instances->stateData.mission->destinationUnreachable || _instances->stateData.drone->rcOverride)
				state = STOPING_MISSION;

			return state;
		}

		OnboardState Commander::do_state_transition_rc() {
			OnboardState state = OnboardState::RC;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(!_instances->stateData.drone->rcOverride)
				state = STOPING_RC;

			return state;
		}

		OnboardState Commander::do_state_transition_manual() {
			OnboardState state = MANUAL;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if (	_instances->stateData.drone->rcOverride
					||	_instances->stateData.station->requestRallyPoint
					||	_instances->stateData.station->requestEmergencyLanding
					||	(_instances->stateData.station->requestPrecisionLanding && _instances->stateData.drone->hasPrecisionLanding)
					||	(_instances->stateData.drone->landed && !_instances->stateData.station->requestManualControl))
				state = STOPING_MANUAL;

			return state;
		}

		OnboardState Commander::do_state_transition_landing() {
			OnboardState state = LANDING;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(_instances->stateData.drone->rcOverride || _instances->stateData.station->requestManualControl || _instances->stateData.drone->landed ||
					_instances->stateData.station->requestEmergencyLanding || _instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY)
				state = STOPING_LANDING;

			return state;
		}

		OnboardState Commander::do_state_transition_emer_landing() {
			OnboardState state = EMERGENCY_LANDING;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(_instances->stateData.drone->rcOverride || _instances->stateData.station->requestManualControl || _instances->stateData.drone->landed)
				state = STOPING_EMERGENCY_LANDING;

			return state;
		}

		OnboardState Commander::do_state_transition_prec_landing() {
			OnboardState state = PRECISION_LANDING;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(	_instances->stateData.drone->rcOverride 
					||	_instances->stateData.drone->landed
					||	_instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY
					||	_instances->stateData.station->requestManualControl
					||	_instances->stateData.station->requestEmergencyLanding 
					||	_instances->stateData.station->requestRallyPoint
					||	(_instances->stateData.precisionLandingTimeout && (_instances->stateData.drone->battery == DroneStateData::BatteryState::CRITICAL)))
				state = STOPING_PRECISION_LANDING;

			return state;
		}

		OnboardState Commander::do_state_transition_parachute() {
			OnboardState state = PARACHUTE;

			if(_instances->stateData.drone->landed)
				state = LANDED_ERROR;

			return state;
		}

		OnboardState Commander::do_state_transition_rallypoint() {
			OnboardState state = RALLYPOINT;

			if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(	_instances->stateData.drone->rcOverride 
					||	(_instances->stateData.mission->finished && _instances->stateData.drone->flightMode != 14)
					||	_instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY
					||	_instances->stateData.station->requestManualControl
					||	_instances->stateData.station->requestEmergencyLanding)
				state = STOPING_RALLYPOINT;

			return state;
		}

		/*
		*	TRANSITION OF TRANSITIONS STATES
		*/

		OnboardState Commander::do_state_transition_starting_mission() {
			OnboardState state = STARTING_MISSION;

			if(_instances->stateData.success == false)
				state = UNLOADING_MISSION;
			else
				state = MISSION_RUNNING;

			return state;
		}

		OnboardState Commander::do_state_transition_stoping_mission() {
			OnboardState state;

			if(_instances->stateData.success == false)
				state = *_lastState; // TODO CAN ALSO BE PAUSED....
			else if(_instances->stateData.drone->rcOverride)
				state = STARTING_RC;
			else if(_instances->stateData.station->requestManualControl)
				state = STARTING_MANUAL;
			else if (_instances->stateData.station->requestRallyPoint || _instances->stateData.mission->destinationUnreachable)
				state = STARTING_RALLYPOINT;
			else if(_instances->stateData.mission->finished && _instances->stateData.drone->flightMode != 14 && _instances->stateData.drone->landed)
				state = UNLOADING_MISSION;
			else if(_instances->stateData.mission->precisionLanding && _instances->stateData.drone->hasPrecisionLanding && _instances->stateData.mission->finished && _instances->stateData.drone->flightMode != 14)
				state = STARTING_PRECISION_LANDING;
			else if(_instances->stateData.mission->finished && _instances->stateData.drone->flightMode != 14)
				state = STARTING_LANDING;
			else if(_instances->stateData.mission->precisionLanding && !_instances->stateData.drone->hasPrecisionLanding)
				state = STARTING_RALLYPOINT;

			return state;
		}

		//TODO LOOP MANUAL CONTROL STOP TIMEOUT ETC...

		OnboardState Commander::do_state_transition_stoping_rc() {
			OnboardState state;

			if(_instances->stateData.success == false)
				state = OnboardState::RC;
			else if(_instances->stateData.station->requestParachute)
				state = STARTING_PARACHUTE;
			else if(!_instances->stateData.drone->rcOverride && !_instances->stateData.drone->landed && (*_lastState == MISSION_PAUSED || *_lastState == MISSION_RUNNING))
				state = *_lastState;
			else if(!_instances->stateData.drone->rcOverride && *_lastState == RALLYPOINT)
				state = STARTING_RALLYPOINT;
			else if(!_instances->stateData.drone->rcOverride && *_lastState == EMERGENCY_LANDING)
				state = STARTING_EMERGENCY_LANDING;
			else if(!_instances->stateData.drone->rcOverride && *_lastState == LANDING)
				state = STARTING_LANDING;
			else if(!_instances->stateData.drone->rcOverride && !_instances->stateData.drone->landed)
				state = STARTING_MANUAL;
			else if(!_instances->stateData.drone->rcOverride && *_lastState == MISSION_READY)
				state = MISSION_READY;
			else if(!_instances->stateData.drone->rcOverride)
				state = UNLOADING_MISSION;

			return state;
		}

		OnboardState Commander::do_state_transition_stoping_manual() {
			OnboardState state;

			if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;
			else if(_instances->stateData.station->requestRallyPoint)
				state = STARTING_RALLYPOINT;
			else if(_instances->stateData.station->requestEmergencyLanding)
				state = STARTING_EMERGENCY_LANDING;
			else if((_instances->stateData.station->requestPrecisionLanding && _instances->stateData.drone->hasPrecisionLanding))
				state = STARTING_PRECISION_LANDING;
			else if((_instances->stateData.drone->landed && !_instances->stateData.station->requestManualControl))
				state = UNLOADING_MISSION;

			return state;
		}

		OnboardState Commander::do_state_transition_stoping_landing() {
			OnboardState state;

			if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;
			else if(_instances->stateData.station->requestManualControl)
				state = STARTING_MANUAL;
			else if(_instances->stateData.station->requestEmergencyLanding || _instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY)
				state = STARTING_EMERGENCY_LANDING;
			else if(_instances->stateData.drone->landed)
				state = UNLOADING_MISSION;

			return state;
		}

		OnboardState Commander::do_state_transition_stoping_emer_landing() {
			OnboardState state;

			if(_instances->stateData.drone->landed)
				state = LANDED_ERROR;
			else if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;
			else if(_instances->stateData.station->requestManualControl)
				state = STARTING_MANUAL;

			return state;
		}

		OnboardState Commander::do_state_transition_stoping_prec_landing() {
			OnboardState state;

			if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;
			else if(_instances->stateData.drone->landed)
				state = UNLOADING_MISSION;
			else if(_instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY || _instances->stateData.station->requestEmergencyLanding)
				state = STARTING_EMERGENCY_LANDING;
			else if(_instances->stateData.station->requestManualControl)
				state = STARTING_MANUAL;
			else if(_instances->stateData.station->requestRallyPoint || (_instances->stateData.precisionLandingTimeout && (_instances->stateData.drone->battery == DroneStateData::BatteryState::CRITICAL)))
				state = STARTING_RALLYPOINT;

			return state;
		}

		OnboardState Commander::do_state_transition_stoping_rallypoint() {
			OnboardState state;

			if(_instances->stateData.success == false)
				state = RALLYPOINT;
			if(_instances->stateData.drone->rcOverride)
				state = OnboardState::STARTING_RC;
			else if(_instances->stateData.station->requestEmergencyLanding || _instances->stateData.drone->battery == DroneStateData::BatteryState::EMERGENCY)
				state = STARTING_EMERGENCY_LANDING;
			else if(_instances->stateData.station->requestManualControl)
				state = STARTING_MANUAL;
			else if(_instances->stateData.mission->finished && _instances->stateData.drone->flightMode != 14)
				state = STARTING_LANDING;

			return state;
		}
	}
}

/*
void Commander::start() {
	_communication->start();
	_communication->register_mavlink_message_handler(MAVLINK_MSG_ID_COMMAND_LONG, std::bind(&Commander::process_command_long, this, std::placeholders::_1), this);
	_communication->register_mavlink_message_handler(MAVLINK_MSG_ID_DATA_TRANSMISSION_HANDSHAKE, std::bind(&Commander::startReceiveMission, this, std::placeholders::_1), this);

	_call_every_handler.add(std::bind(&DjiHandler::updateBroadcastData, _instances->drone), 0.05f, &_updateDroneBroadcastedData_cookie);
	_call_every_handler.add(std::bind(&Commander::shutdownButton, this), 0.1f, &_powerButton_cookie);

	_call_every_telemtery_handler.add(std::bind(&TelemetryManager::sendGlobalPosition, _telemetryManager), 1.0f, &_sendGlobalPosition_cookie);
	_call_every_telemtery_handler.add(std::bind(&TelemetryManager::sendSystemStatus, _telemetryManager), 5.0f, &_sendSystemStatus_cookie);
	_call_every_telemtery_handler.add(std::bind(&TelemetryManager::sendDebugVector, _telemetryManager), 1.0f, &_sendSensor_cookie);
	_call_every_telemtery_handler.add(std::bind(&StationManager::sendPing, _communication), 10.0f, &_pingCookie);
	_call_every_telemtery_handler.add(std::bind(&Commander::sensorReading, this), 1.0f, &_sensorCookie);
	//_call_every_telemtery_handler.add(std::bind(&FlightLog::cycle, FlightLog::getInstance()), 1.0f, &_log_cookie);

	//_call_every_telemtery_handler.add(std::bind(&GroundSensor::cycle, _lidar), 0.1f, &_lidarMeasure_cookie);
	//_call_every_telemtery_handler.add(std::bind(&RpiAruco::cycle, _rpiAruco), 0.5f, &_irlockMeasure_cookie);

	_thread_pool.start();
	_system_thread = new std::thread(&Commander::system_thread, this);
	_state_thread = new std::thread(&Commander::state_thread, this);
	_telemetry_thread = new std::thread(&Commander::telemetry_thread, this);

	if(_instances->stateData.error == true) {
		_instances->stateData.bootFinished = true;
		//UserInterface::getInstance()->setErrorLed(LedBrighness::MAX);
	}
}

void Commander::startReceiveMission(mavlink_message_t message) {
	mavlink_data_transmission_handshake_t dataHeader;
    mavlink_msg_data_transmission_handshake_decode(&message, &dataHeader);

	if(_missionReceptionStarted) {
		LogErr() << "A mission is currently in transmission.";
		return;
	}

	_missionReceptionSeq = 0;
	_missionReceptionSize = dataHeader.size;
	_missionReceptionPack = dataHeader.packets;
	_missionReceptionId = dataHeader.jpg_quality;

	_missionReceptionFile.open("temporary.mission", std::ios::out);

	_instances->stateMachineTimeoutHandler->add(std::bind(&Commander::receiveMissionTimeout, this), 10.0f, &_receiveMission_cookie);
	_communication->register_mavlink_message_handler(MAVLINK_MSG_ID_ENCAPSULATED_DATA, std::bind(&Commander::receiveMission, this, std::placeholders::_1), this);
}

void Commander::receiveMission(mavlink_message_t message) {
	if(_missionReceptionStarted) {
		mavlink_encapsulated_data_t data;
	    mavlink_msg_encapsulated_data_decode(&message, &data);
		
		if(_missionReceptionSeq == data.seqnr) {
			_instances->stateMachineTimeoutHandler->refresh(_receiveMission_cookie);
			char* buffer[254];
			std::memset(buffer, '\0', 254);
			std::memcpy(buffer, data.data, 253);
			
			_missionReceptionFile << buffer;

			if(_missionReceptionPack == _missionReceptionSeq + 1) {
				_instances->stateMachineTimeoutHandler->remove(_receiveMission_cookie);
				_missionReceptionFile.close();
				//TODO move the file to the final name
				_missionReceptionStarted = false;
			}
		}
		else {
			LogErr() << "Encapsulated data received but the sequence ID is incorrect...";
		}
	}
	else {
		LogErr() << "Encapsulated data received but no upload in progress...";
	}
}

void Commander::receiveMissionTimeout() {
	LogErr() << "Timeout during mission transmission.";
	_missionReceptionFile.close();
	_missionReceptionStarted = false;
}

void Commander::sensorReading() {
	bool lastStatus = _cargoSensor1->connected();
	_cargoSensor1->cycle();
	if(lastStatus != _cargoSensor1->connected()) {
		lastStatus = _cargoSensor1->connected();
		if(lastStatus) {
			//UserInterface::getInstance()->setButtonLed(LedColor::GREEN);
		}
		else {
			//UserInterface::getInstance()->setButtonLed(LedColor::CYAN);
		}
	}
}
*/