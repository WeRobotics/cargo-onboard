#include "configuration.h"

#include <string>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <Library/logging/log.h>

#include <Hardware/drivers/raspberry/tcp_connection.h>
#include <Hardware/drivers/raspberry/serial_connection.h>

#include <Hardware/drivers/terra_ranger.h>
#include <Hardware/drivers/lidar_lite.h>
#include <Hardware/drivers/i2cLinux.h>
#include <Hardware/drivers/lp55231.h>

#include <Application/LandingTarget/camera_aruco_detector.h>
#include <Application/LandingTarget/rpi_aruco_detector.h>
#include <Application/LandingTarget/irlock.h>

#include <Application/Drones/dji/dji_loader.h>
#include <Application/Drones/dji/dji_handler.h>
#include <Application/Drones/modes/manual_mode.h>
#include <Application/Drones/modes/precision_landing.h>

#include <Application/state_recovery.h>

#include "handler.h"
#include "connection_link.h"
#include "digi_radio_api.h"

wer::cargo::Configuration::Configuration() :
_config(),
_dataLoaded(false)
{
	_instances = std::make_shared<Instances>();
	open_log("/home/pi/cargo_log/");
}

wer::cargo::Configuration::~Configuration()
{
	close_log();
}

wer::lib::ProcessResult wer::cargo::Configuration::load(int argc, char const *argv[]) {
	if(argc >= 2) {
		return load(argv[1]);
	}

	LogErr() << "Not enough arguments provided to the configurator.";

	return wer::lib::ProcessResult::INVALID_ARGUMENTS;
}

wer::lib::ProcessResult wer::cargo::Configuration::load(std::string path) {
	LogInfo() << "Loading configuration file : " << path;
	
	try{
		_config = YAML::LoadFile(path);
		_dataLoaded = true;
	}
	catch(std::exception &e){
		LogErr() << "Error while reading config file.";
		LogErr() << e.what();
		return wer::lib::ProcessResult::PARSING_ERROR;
	}

	createHandler();

	createHardware();

	wer::com::Handler::get()->init(_instances->communicationTimeoutHandler, 
								   _instances->communicationCallEveryHandler, 
		 						   getSystemId(), getComponentId());

	wer::com::Handler::get()->setMavType(MAV_TYPE_ONBOARD_CONTROLLER);
	wer::com::Handler::get()->setPilotType(MAV_AUTOPILOT_GENERIC);

	createLinks();

	return createInstances();
}

std::shared_ptr<wer::cargo::Instances> wer::cargo::Configuration::getInstances() {
	return _instances;
}

std::shared_ptr<wer::cargo::Commander> wer::cargo::Configuration::getCommander() {
	return _commander;
}

void wer::cargo::Configuration::createHandler() {
	_instances->communicationTimeoutHandler = std::make_shared<TimeoutHandler>(_instances->time);
	_instances->communicationCallEveryHandler = std::make_shared<CallEveryHandler>(_instances->time);

	_instances->stateMachineTimeoutHandler = std::make_shared<TimeoutHandler>(_instances->time);
	_instances->stateMachineCallEveryHandler = std::make_shared<CallEveryHandler>(_instances->time);

	_instances->sensorsTimeoutHandler = std::make_shared<TimeoutHandler>(_instances->time);
	_instances->sensorsCallEveryHandler = std::make_shared<CallEveryHandler>(_instances->time);

	_instances->droneTimeoutHandler = std::make_shared<TimeoutHandler>(_instances->time);
	_instances->droneCallEveryHandler = std::make_shared<CallEveryHandler>(_instances->time);
}

void wer::cargo::Configuration::createHardware() {
	if(_config["hardware"]["i2c"]) {
		_i2c = std::make_shared<wer::hal::I2cLinux>(_config["hardware"]["i2c"].as<std::string>().c_str());
	}
	else {
		_i2c = std::make_shared<wer::hal::I2cLinux>();
	}

	LogInfo() << "Init the led status";
	if(_config["hardware"]["led"]) {
		std::shared_ptr<wer::hal::lp55231> led = std::make_shared<wer::hal::lp55231>(_i2c, _config["hardware"]["led"].as<int>());
		led->enable();
		_led = led;
	}
	else {
		std::shared_ptr<wer::hal::lp55231> led = std::make_shared<wer::hal::lp55231>(_i2c);
		led->enable();
		_led = led;
	}
	LogInfo() << "Led init done";
}

void wer::cargo::Configuration::createLinks() {
	if(_dataLoaded) {
		auto comLinks = _config["communications"];
		for(auto item = comLinks.begin(); item != comLinks.end(); item++) {
			wer::hal::Connection* newConnection = nullptr;
			wer::com::Link* newLink = nullptr;

			std::string name = (*item)["name"].as<std::string>();
			std::string type = (*item)["type"].as<std::string>();
			std::string api = (*item)["api"].as<std::string>();
			uint8_t priority = (*item)["priority"].as<unsigned int>();

			if(priority > wer::com::HIGHEST_PRIORITY) {
				priority = wer::com::HIGHEST_PRIORITY;
			}

			if(type.compare("serial") == 0) {
				std::string serialPath = (*item)["path"].as<std::string>();
				int serialBaudrate = (*item)["baudrate"].as<int>();

				newConnection = new wer::hal::SerialConnection(serialPath, serialBaudrate);
			}
			else if(type.compare("tcp") == 0) {
				std::string ipAddress = (*item)["ip"].as<std::string>();
				int portNumber = (*item)["port"].as<int>();

				newConnection = new wer::hal::TcpConnection(ipAddress, portNumber);
			}
			else {
				LogWarn() << "While configuring \"" << name << "\" : type \"" << type << "\" is not implemented.";
				continue;
			}

			if(api.compare("none") == 0) {
				newLink = new wer::com::ConnectionLink(newConnection, priority, name);
			}
			else if(api.compare("digiApi") == 0) {
				newLink = new wer::com::DigiRadioApi(newConnection, priority, name);
			}
			else {
				delete(newConnection);
				LogWarn() << "While configuring \"" << name << "\" : API type \"" << api << "\" is not implemented.";
				continue;
			}

			wer::hal::ConnectionResult result = newConnection->start();

			if(result != wer::hal::ConnectionResult::SUCCESS && result != wer::hal::ConnectionResult::SOCKET_CONNECTION_ERROR) {
				delete(newLink);
				LogWarn() << "While configuring \"" << name << "\" : Could not start the connection.";
				continue;
			}
			else if(wer::com::Handler::get()->addLink(newLink) == wer::hal::ConnectionResult::SUCCESS) {
				LogInfo() << "Connection \"" << name << "\" successfuly added.";
			}
			else {
				delete(newLink);
				continue;
			}
		}
	}
}

uint8_t wer::cargo::Configuration::getSystemId() {
	if(_dataLoaded && _config["systemId"]) {
		return _config["systemId"].as<unsigned int>();
	}
	else {
		LogWarn() << "Unable to found system ID in configuration. (Using: 0)";
		return 0;
	}
}
uint8_t wer::cargo::Configuration::getComponentId() {
	if(_dataLoaded && _config["componentId"]) {
		return _config["componentId"].as<unsigned int>();
	}
	else {
		LogWarn() << "Unable to found component ID in configuration. (Using: 0)";
		return 0;
	}
}

wer::lib::ProcessResult wer::cargo::Configuration::createInstances() {
	/*
	auto gpios = _config["hardware"]["gpios"];
	for(auto item = sensors.begin(); item != sensors.end(); item++) {
		std::string name = (*item)["name"].as<std::string>();
		std::string dir = (*item)["direction"].as<std::string>();

		if(dir.compare("out") && (*item)["default"]) {
			uint8_t value = (*item)["default"].as<std::string>();
		}
	}

	auto gpios = _config["hardware"]["i2c"];
	for(auto item = sensors.begin(); item != sensors.end(); item++) {
		std::string name = (*item)["name"].as<std::string>();
		std::string type = (*item)["direction"].as<std::string>();
		std::string component = (*item)["component"].as<std::string>();
		if(type.compare("direction"))
	}
	*/

	_instances->statusLed = std::make_shared<StatusLed>(_led, _instances->sensorsCallEveryHandler);
	wer::com::Handler::get()->setSystemEventCallback(std::bind(&StatusLed::newSystemEvent, _instances->statusLed, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

	_commander = std::make_shared<Commander>(_instances);

	auto sensors = _config["sensors"];
	for(auto item = sensors.begin(); item != sensors.end(); item++) {
		std::string name = (*item)["name"].as<std::string>();
		std::string type = (*item)["type"].as<std::string>();
		std::string component = (*item)["component"].as<std::string>();

		if(type.compare("GroundSensor") == 0 && _instances->groundSensor == nullptr) {
			std::shared_ptr<wer::hal::DistanceSensor> sensor;

			if(component.compare("LidarLite") == 0) {
				if((*item)["address"]) {
					uint8_t address = (*item)["address"].as<unsigned int>();
					sensor = std::make_shared<wer::hal::LidarLiteI2C>(_instances->sensorsCallEveryHandler, _i2c, address);
				}
				else {
					sensor = std::make_shared<wer::hal::LidarLiteI2C>(_instances->sensorsCallEveryHandler, _i2c);
				}
			}

			else if(component.compare("TerraRanger") == 0) {
				if((*item)["address"]) {
					uint8_t address = (*item)["address"].as<unsigned int>();
					sensor = std::make_shared<wer::hal::TeraRanger>(_instances->sensorsCallEveryHandler, _i2c, address);
				}
				else {
					sensor = std::make_shared<wer::hal::TeraRanger>(_instances->sensorsCallEveryHandler, _i2c);
				}
			}

			else {
				LogWarn() << "While configuring \"" << name << "\" : Component \"" << component << "\" not implemented.";
				continue;
			}

			if(sensor->init() != lib::ProcessResult::SUCCESS) {
				LogWarn() << "While configuring \"" << name << "\" : Failed to initialize the sensor.";
			}
			else {
				_instances->groundSensor = sensor;
				LogInfo() << name << " configured successfuly.";
			}
		}

		else if(type.compare("LandingTarget") == 0 && _instances->landingTarget == nullptr) {
			std::shared_ptr<LandingTarget> landingTarget;

			if(component.compare("ArucoCamera") == 0) {
				if((*item)["offset_x"] && (*item)["offset_y"]) {
					float offset_x = (*item)["offset_x"].as<float>();
					float offset_y = (*item)["offset_y"].as<float>();
					LogInfo() << "Custom offset";
					landingTarget = std::make_shared<CameraArucoDetector>(offset_x, offset_y);
				}
				else {
					LogInfo() << "No offset";
					landingTarget = std::make_shared<CameraArucoDetector>();
				}
			}
/*
			else if(component.compare("ArucoRaspberry")) {

			}
*/
/*
			else if(component.compare("IrLock")) {

			}
*/
			else {
				LogWarn() << "While configuring \"" << name << "\" : Component \"" << component << "\" not implemented.";
				continue;
			}

			if(landingTarget->init() == wer::lib::ProcessResult::SUCCESS) {
				_instances->landingTarget = landingTarget;
				LogInfo() << name << " configured successfuly.";
			}
			else {
				LogWarn() << "While configuring \"" << name << "\" : Failed to initialize the sensor.";
			}
		}

		else if(type.compare("CargoTemperature") == 0 && _instances->cargoSensor == nullptr) {

			if(component.compare("SHT21") == 0) {
				if((*item)["address"]) {
					uint8_t address = (*item)["address"].as<unsigned int>();
					_instances->cargoSensor = std::make_shared<TempHumSensor>(_instances->sensorsCallEveryHandler, _i2c, address);
					LogInfo() << name << " configured successfuly.";
				}
				else {
					LogWarn() << "While configuring \"" << name << "\" : Missing address.";
				}
			}

			else {
				LogWarn() << "While configuring \"" << name << "\" : Component \"" << component << "\" not implemented.";
				continue;
			}
		}

		else {
			LogWarn() << "While reading config file : \"" << type << "\" type not implemented.";
		}
	}

	if(_config["drone"]) {
		if(_config["drone"]["type"].as<std::string>().compare("M600") == 0) {
			int appId = _config["drone"]["appId"].as<int>();
			std::string encKey = _config["drone"]["encKey"].as<std::string>();
			std::string port = _config["drone"]["port"].as<std::string>();
			unsigned int baudrate = _config["drone"]["baudrate"].as<unsigned int>();

			//Create the dji loader and give it to the drone.
			std::shared_ptr<DjiLoader> djiLoader = std::make_shared<DjiLoader>();
			djiLoader->setParams(appId, encKey, port, baudrate);

			std::shared_ptr<Drone> drone = std::make_shared<DjiHandler>(djiLoader, _instances->stateData.drone);
			if(drone->testSerialConnection() == false) {
				LogErr() << "While creating drone component : Error with serial port.";
			}
			else {
				_instances->drone = drone;
				LogInfo() << "Drone \"M600\" configured successfuly.";
			}
		}
	}

	if(_instances->groundSensor && _instances->drone) {
		_instances->missionManager = std::make_shared<CargoMissionManager>(_instances->groundSensor, 
																		   _instances->drone, 
																		   _instances->stateData.mission);

		_instances->telemetryManager = std::make_shared<TelemetryManager>(_instances->drone, 
																		  _instances->missionManager, 
																		  _instances->cargoSensor);

		_instances->manualMode = std::make_shared<ManualMode>(_instances->groundSensor, 
															  _instances->drone, 
															  std::bind(&Commander::requestManualControl, 
															  			_commander, 
															  			true));

		set_log_callback(std::bind(&TelemetryManager::sendStatusText, _instances->telemetryManager, std::placeholders::_1, std::placeholders::_2));

		if(_instances->landingTarget) {
			bool param_valid = false;
			TargetEstimatorParams params;

			if(_config["targetEstimator"]) {
				auto targetParams = _config["targetEstimator"];

				if(	   targetParams["vel_unc"] && targetParams["meas_unc"] && targetParams["scale_x"] 
					&& targetParams["scale_y"] && targetParams["max_var"]) {

					params.vel_unc = targetParams["vel_unc"].as<float>();
					params.meas_unc = targetParams["meas_unc"].as<float>();
					params.scale_x = targetParams["scale_x"].as<float>();
					params.scale_y = targetParams["scale_y"].as<float>();
					params.max_var = targetParams["max_var"].as<float>();
					param_valid = true;
				}
				else {
					param_valid = false;
				}
			}

			std::shared_ptr<PrecisionLandingMode> landingMode;
			if(param_valid) {
				landingMode = std::make_shared<PrecisionLandingMode>(_instances->drone, _instances->groundSensor, 
																	 _instances->landingTarget, 
																	 std::bind(&Commander::stateTimeout, _commander, true),
																	 _instances->stateMachineCallEveryHandler,
																	 &params
																	 );
			}
			else {
				landingMode = std::make_shared<PrecisionLandingMode>(_instances->drone, _instances->groundSensor, 
																	 _instances->landingTarget, 
																	 std::bind(&Commander::stateTimeout, _commander, true),
																	 _instances->stateMachineCallEveryHandler
																	 );
			}

			_instances->landingTarget->setMeasureCallback(std::bind(&PrecisionLandingMode::setMeasurement, 
													  landingMode, 
													  std::placeholders::_1));

			_instances->stateData.drone->hasPrecisionLanding = true;
			_instances->landingMode = landingMode;
		}
		else {
			_instances->stateData.drone->hasPrecisionLanding = false;
		}
	}
	else {
		if(_instances->drone == nullptr) {
			LogErr() << "Required component not found : No valid drone.";
		}

		if(_instances->groundSensor == nullptr) {
			LogErr() << "Required component not found : No valid ground sensor.";
		}

		return wer::lib::ProcessResult::NOT_CONNECTED;
	}

	return initAfterAllInstanceCreated();
}

wer::lib::ProcessResult wer::cargo::Configuration::initAfterAllInstanceCreated() {
	_commander->init();
	_commander->setRecoveryCallback(std::bind(&Configuration::recoveryStart, this));

	return wer::lib::ProcessResult::SUCCESS;
}

void wer::cargo::Configuration::recoveryStart() {
	LogWarn() << "Try an in air recovery.";
	// 1. Try to open the file which should contain the data to recover the state
	namespace pt = boost::property_tree;
	pt::ptree root;
	
	try{
		pt::read_json("/home/pi/inFlightRecovery.dat", root);
		LogInfo() << "Reading json successful";
	}
	catch(std::exception &e){
		LogErr() << "Error while reading recovery file.";
		LogErr() << e.what();
		return;
	}

	uint8_t missionId = root.get<uint8_t>("mission.id");
	uint8_t missionIndex = root.get<uint8_t>("mission.index");
	float terrainAltitude = root.get<float>("mission.terrainAltitude");

	uint8_t missionIsDji = root.get<uint8_t>("mission.isDji");
	uint8_t missionIsFinished = root.get<uint8_t>("mission.isFinished");

	GpsPosition takeoffPosition;
	takeoffPosition.altitude = root.get<double>("drone.takeoff.altitude");
	takeoffPosition.latitude = root.get<double>("drone.takeoff.latitude");
	takeoffPosition.longitude = root.get<double>("drone.takeoff.longitude");

	float altOffset = root.get<double>("drone.altOffset");

	uint8_t commanderState = root.get<uint8_t>("commander.state");

	// 2. Let's use the data which are independent of the state
	_instances->drone->recoverStart(takeoffPosition, altOffset);

	//Let's wait one second to get all telemetry update from drone, specialy the battery level
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	if(missionId) {
		std::string missionFile = std::to_string(missionId) + std::string(".plan");
		if(_instances->missionManager->recoverMission(missionFile, missionIndex, terrainAltitude, missionIsDji, missionIsFinished)) {
			return;
		}
		com::Handler::get()->setMissionId(missionId);

		StateRecovery::getInstance()->setMissionId(missionId);
	}

	// 3. Let's reinit the state we were in
	switch(commanderState) {
		case MISSION_RUNNING:
			_commander->init(STARTING_MISSION);
		break;

		case RALLYPOINT:
			_commander->init(STARTING_RALLYPOINT);
		break;

		case OnboardState::RC:
		case MANUAL:
			_commander->init(STARTING_MANUAL);
		break;

		case LANDING:
			_commander->init(STARTING_LANDING);
		break;

		case EMERGENCY_LANDING:
			_commander->init(STARTING_EMERGENCY_LANDING);
		break;

		case PRECISION_LANDING:
			_commander->init(STARTING_PRECISION_LANDING);
		break;

		case PARACHUTE:
			_commander->init(STARTING_PARACHUTE);
		break;
	}
}

/*
_communication->register_mavlink_message_handler(MAVLINK_MSG_ID_DATA_TRANSMISSION_HANDSHAKE, std::bind(&Commander::startReceiveMission, this, std::placeholders::_1), this);
_call_every_handler.add(std::bind(&Commander::shutdownButton, this), 0.1f, &_powerButton_cookie);
_call_every_handler.remove(_powerButton_cookie);
_call_every_telemtery_handler.add(std::bind(&FlightLog::cycle, FlightLog::getInstance()), 1.0f, &_log_cookie);
_call_every_telemtery_handler.remove(_log_cookie);
_call_every_telemtery_handler.add(std::bind(&StationManager::sendPing, _communication), 10.0f, &_pingCookie);
_call_every_telemtery_handler.remove(_pingCookie);
*/
