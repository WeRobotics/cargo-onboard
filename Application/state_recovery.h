#pragma once

#include <iostream>

#include <Application/state.h>
#include <Application/Drones/location_struct.h>

struct RecoveryData {
  uint8_t missionId;
  uint8_t missionIndex;
  float terrainAltitude;
  uint8_t missionIsDji;
  uint8_t missionIsFinished;
  GpsPosition takeoffPosition;
  float altOffset;
  OnboardState commanderState;
};

class StateRecovery {
  public:
    static StateRecovery* getInstance(){
      if (!instance){
          instance = new StateRecovery();
      }
      return instance;
    };
    StateRecovery(StateRecovery const&)   = delete;
    void operator=(StateRecovery const&)  = delete;
  private:
    static StateRecovery* instance;
    StateRecovery();
    ~StateRecovery();

  public:
    void setMissionId(uint8_t missionId);
    void setMissionIndex(uint8_t missionIndex);
    void setTerrainAltitude(float terrainAltitude);
    void setMissionIsDji(bool missionIsDji);
    void setMissionIsFinished(bool missionIsFinished);
    void setTakeoffPosition(GpsPosition takeoffPosition);
    void setAltOffset(float altOffset);
    void setCommanderState(OnboardState commanderState);

    void appInitDone();

	private:
		RecoveryData _datas;
    bool _appInitDone;

    void save();
};
