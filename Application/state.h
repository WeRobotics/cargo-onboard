#pragma once

#include <mutex>
#include <memory>

enum OnboardState{	BOOTING, 						IDLE,							// 0
					LANDED_ERROR, 					MISSION_READY,					// 2
					MISSION_RUNNING, 				MISSION_PAUSED,					// 4
					RC, 							MANUAL,							// 6
					LANDING, 						EMERGENCY_LANDING,				// 8
					PRECISION_LANDING, 				PARACHUTE,						// 10
					RALLYPOINT, 					LOADING_MISSION,				// 12
					UNLOADING_MISSION, 				STARTING_MISSION,				// 14
					RESUMING_MISSION, 				PAUSING_MISSION,				// 16
					STOPING_MISSION, 				STOPING_RC,						// 18
					STARTING_MANUAL, 				STOPING_MANUAL,					// 20
					STARTING_LANDING, 				STOPING_LANDING,				// 22
					STARTING_EMERGENCY_LANDING, 	STOPING_EMERGENCY_LANDING,		// 24
					STARTING_PRECISION_LANDING, 	STOPING_PRECISION_LANDING,		// 26
					STARTING_PARACHUTE, 			STARTING_RALLYPOINT,			// 28
					STOPING_RALLYPOINT,				STARTING_RC};					// 30

enum OnboardStateSimple{BOOTING_SIMPLE, 			IDLE_SIMPLE, 
						RALLY_POINT_SIMPLE, 		EMERGENCY_LANDING_SIMPLE, 
						PARACHUTE_SIMPLE, 			LANDED_ERROR_SIMPLE, 
						MISSION_RECEIVED_SIMPLE, 	MISSION_READY_SIMPLE, 
						MISSION_RUNNING_SIMPLE, 	MISSION_PAUSED_SIMPLE, 
						MANUAL_CONTROL_SIMPLE, 		RC_CONTROL_SIMPLE, 
						PRECISION_LANDING_SIMPLE};


struct MissionStateData {
	MissionStateData() :
	isDjiMission(false),
	finished(false),
	destinationUnreachable(false),
	precisionLanding(false),
	cruiseSpeed(0.0f)
	{}

	bool isDjiMission;
	bool finished;
	bool destinationUnreachable;
	bool precisionLanding;
	float cruiseSpeed;
};

struct DroneStateData {
	DroneStateData() :
	landed(true),
	rcOverride(false),
	hasPrecisionLanding(true),
	flightMode(0),
	battery(NORMAL)
	{}

	enum BatteryState{NORMAL, WARNING, CRITICAL, EMERGENCY};

	//bool connected;
	//bool controlMode;
	//bool status;//OK to be tested
	bool landed;//OK to be tested
	bool rcOverride;//OK to be tested
	//bool destinationUnreachable;
	//bool missionFinished;
	//bool requestPrecisionLanding;
	bool hasPrecisionLanding;
	uint8_t flightMode;
	BatteryState battery;
};

struct OperatorStateData {
	OperatorStateData() :
	requestManualControl(false),
	requestMissionStart(false),
	requestMissionPause(false),
	requestMissionContinue(false),
	requestMissionLoad(false),
	requestMissionLoadId(0),
	requestRallyPoint(false),
	requestEmergencyLanding(false),
	requestPrecisionLanding(false),
	requestParachute(false)
	{}

	bool requestManualControl;//OK
	bool requestMissionStart;//OK
	bool requestMissionPause;//OK
	bool requestMissionContinue;//OK
	bool requestMissionLoad;
	uint8_t requestMissionLoadId;
	uint8_t requestMissionVersionId;
	bool requestRallyPoint; //OK
	bool requestEmergencyLanding; //OK
	bool requestPrecisionLanding; //OK
	bool requestParachute; //OK

	std::mutex mutex;
};

struct GlobalStateData {
	std::shared_ptr<struct MissionStateData> mission;
	std::shared_ptr<struct DroneStateData> drone;
	std::shared_ptr<struct OperatorStateData> station;
	std::shared_ptr<struct EmergencyStateData> emergency;
	bool error;
	bool success;
	bool bootFinished;
	bool precisionLandingTimeout;
};
