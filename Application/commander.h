#pragma once

#include <unistd.h>

#include <iostream>
#include <fstream>
#include <thread>
#include <atomic>

#include <Library/logging/log.h>

//#include <Hardware/include/hardware.h>
#include <Communication/handler.h>
#include <Application/instances.h>

namespace wer {
    namespace cargo {

		class Commander {
		public :
			Commander(std::shared_ptr<Instances> instances);
			~Commander();

			void init();
			void start();
			void stop();
			void cycle();

			//Recovery functions
			void init(OnboardState recoveryState);
			void setRecoveryCallback(std::function<void(void)> doRecoveryCallback);

			void requestManualControl(bool state);
			void stateTimeout(bool state);
			void resetStationRequest();

			//Proccess operator command to update state datas
			void process_command_long(const mavlink_message_t &message);
			void command_ack(uint8_t state, uint16_t cmd, uint8_t sysId);

		private :

			void updateHeartbeatState();
			uint8_t sysStateToMavState(OnboardStateSimple sysState);
			OnboardStateSimple sysStateToSimpleState(OnboardState sysState);
			
			void initStateMachine();
			void updateState();
			void doStateAction();
			void doStateTransition();

			OnboardState do_state_transition_boot();
			OnboardState do_state_transition_idle();
			OnboardState do_state_transition_landed_error();
			OnboardState do_state_transition_mission_ready();
			OnboardState do_state_transition_mission_running();
			OnboardState do_state_transition_mission_paused();
			OnboardState do_state_transition_rc();
			OnboardState do_state_transition_manual();
			OnboardState do_state_transition_landing();
			OnboardState do_state_transition_emer_landing();
			OnboardState do_state_transition_prec_landing();
			OnboardState do_state_transition_parachute();
			OnboardState do_state_transition_rallypoint();
			OnboardState do_state_transition_starting_mission();
			OnboardState do_state_transition_stoping_mission();
			OnboardState do_state_transition_stoping_rc();
			OnboardState do_state_transition_stoping_manual();
			OnboardState do_state_transition_stoping_landing();
			OnboardState do_state_transition_stoping_emer_landing();
			OnboardState do_state_transition_stoping_prec_landing();
			OnboardState do_state_transition_stoping_rallypoint();

			void do_state_boot();
			void do_state_unloading_mission();
			void do_state_loading_mission();
			void do_state_starting_mission();
			void do_state_pausing_mission();
			void do_state_resuming_mission();
			void do_state_stoping_mission();
			void do_state_starting_rallypoint();
			void do_state_stoping_rallypoint();
			void do_state_starting_rc();
			void do_state_stoping_rc();
			void do_state_starting_manual();
			void do_state_stoping_manual();
			void do_state_starting_landing();
			void do_state_stoping_landing();
			void do_state_starting_emer_landing();
			void do_state_stoping_emer_landing();
			void do_state_starting_prec_landing();
			void do_state_stoping_prec_landing();
			void do_state_starting_parachute();

			//Helper functions
			uint8_t process_command(mavlink_command_long_t &command, bool &cmd_ack_needed);
			bool tryDroneConnection();

			std::shared_ptr<Instances> _instances;

			std::mutex _stateMutex;
			std::shared_ptr<std::atomic<OnboardState>>	_actualState;
			std::shared_ptr<std::atomic<OnboardState>>	_lastState;

			// Command ack stuff
			std::mutex _cmd_ack_mutex;
			uint8_t _cmd_ack_turn;
			bool _cmd_prossesing;
			bool _cmd_ack_needed;
			bool _cmd_ack_ready;
			uint8_t _cmd_ack_type; // TO BE UPDATED BY THE STATE MACHINE, DEFAULT IS DENIED
			uint8_t _cmd_ack_sysid;
			uint16_t _cmd_ack_id;
			uint8_t _last_cmd_ack = MAV_RESULT_UNSUPPORTED;
			mavlink_command_long_t _last_cmd;

			std::function<void(void)> _doRecoveryCallback;

			void* _manualControl_cookie;
			void* _manualControlTimeout_cookie;
			void* _stateTimeout_cookie;
			void* _missionMonitoring_cookie;
		};
	}
}
