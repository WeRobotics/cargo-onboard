#include "rpi_aruco_detector.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>

#include <cstring>
#include <iostream>
#include <iomanip>

#include <Library/logging/log.h>
//#include <Library/logging/flight_log.h>
#include <Library/global_include.h>

static constexpr uint8_t ARUCO_ID =            0x0A;
static constexpr uint8_t ARUCO_ID_REG =        0x01;
static constexpr uint8_t ARUCO_DATA_REG =      0x02;

static constexpr uint8_t ARUCO_CMD_MASK =      0x10;
static constexpr uint8_t ARUCO_CMD_NO_LOG =    0x00;
static constexpr uint8_t ARUCO_CMD_RT_LOG =    0x01;
static constexpr uint8_t ARUCO_CMD_RA_LOG =    0x02;
static constexpr uint8_t ARUCO_CMD_PT_LOG =    0x04;
static constexpr uint8_t ARUCO_CMD_PA_LOG =    0x08;

static constexpr uint16_t ARUCO_RES_X = 1280;
static constexpr uint16_t ARUCO_RES_Y = 960;

static constexpr uint16_t ARUCO_CENTER_X = (ARUCO_RES_X/2);            // the x-axis center pixel position
static constexpr uint16_t ARUCO_CENTER_Y = (ARUCO_RES_Y/2);            // the y-axis center pixel position

static constexpr float ARUCO_FOV_X = (61.92f*M_PI_F/180.0f);
static constexpr float ARUCO_FOV_Y = (48.46f*M_PI_F/180.0f);

static constexpr float ARUCO_TAN_HALF_FOV_X = 0.60; // tan(0.5 * 60 * pi/180)
static constexpr float ARUCO_TAN_HALF_FOV_Y = 0.45; // tan(0.5 * 35 * pi/180)

static constexpr float ARUCO_TAN_ANG_PER_PIXEL_X =  (2*ARUCO_TAN_HALF_FOV_X/ARUCO_RES_X);
static constexpr float ARUCO_TAN_ANG_PER_PIXEL_Y =  (2*ARUCO_TAN_HALF_FOV_Y/ARUCO_RES_Y);

static constexpr uint8_t ARUCO_PAYLOAD_SIZE = 16;

static constexpr float ARUCO_TAG_SIZE_1 = 1.5f;
static constexpr float ARUCO_TAG_SIZE_2 = 0.7f;
static constexpr float ARUCO_TAG_SIZE_3 = 0.3f;

/** constructor **/
RpiArucoDetector::RpiArucoDetector(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<wer::hal::I2c> i2c, uint8_t address) :
_address(address),
_sensor_ok(false),
_read_failures(0),
_i2c_errors(0),
_same_image(0),
_outdated(0),
_collect_phase(false),
_lastImgNb(255),
_lastValidData(0x00),
_lastOutdated(false),
_call_every_handler(call_every_handler),
_i2c(i2c)
{
}

/** destructor **/
RpiArucoDetector::~RpiArucoDetector()
{
    LogInfo() << "ARUCO report - read failure : " << unsigned(_read_failures) << ", i2c errors : " << unsigned(_i2c_errors) << ", _same_image : " << unsigned(_same_image) << ", _outdated : " << unsigned(_outdated);
}

/** initialise driver to communicate with sensor **/
wer::lib::ProcessResult RpiArucoDetector::init()
{
    if(probe()) {
        return wer::lib::ProcessResult::NOT_CONNECTED;
    }

    _sensor_ok = true;

    return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult RpiArucoDetector::start() {
    _call_every_handler->add(std::bind(&RpiArucoDetector::cycle, this), 0.5f, &_timer_cookie);

    return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult RpiArucoDetector::stop() {
    _call_every_handler->remove(_timer_cookie);

    return wer::lib::ProcessResult::SUCCESS;
}

void RpiArucoDetector::setLoggingLevel(ArucoLogging logLevel) {
    if(_sensor_ok) {
        uint8_t byte = ARUCO_CMD_MASK;

        switch(logLevel) {
            case LOG_NONE:
                byte |= ARUCO_CMD_NO_LOG;
            break;

            case LOG_ALL:
                byte |= ARUCO_CMD_RA_LOG;
                byte |= ARUCO_CMD_PA_LOG;
            break;

            case LOG_ALL_TAR:
                byte |= ARUCO_CMD_RT_LOG;
                byte |= ARUCO_CMD_PT_LOG;
            break;

            case LOG_RAW_TAR:
                byte |= ARUCO_CMD_RT_LOG;
            break;

            case LOG_RAW_ALL:
                byte |= ARUCO_CMD_RA_LOG;
            break;

            case LOG_RES_TAR:
                byte |= ARUCO_CMD_PT_LOG;
            break;

            case LOG_RES_ALL:
                byte |= ARUCO_CMD_PA_LOG;
            break;
        }

        _i2c->set(_address, byte);
    }
}

void RpiArucoDetector::setMeasureCallback(std::function<void(struct wer::cargo::landing_report_s)> callback) {
    _callback = callback;
}

void RpiArucoDetector::cycle() {
    if(_collect_phase) {
        _collect_phase = false;
        uint8_t validTarget = 0;
        if(read_device(validTarget)) {
            return;
        }

        if(validTarget > 0) {
            //FlightLog::getInstance()->logPrecisionLandingMeasurment(_lastValidData, _reports.height, _lastImgNb);

            LogLanding() << ";MEASURE;" << _report.targets[0].pos_x 
                         << ";" << _report.targets[0].pos_y 
                         //<< ";" << (_report.validTargets > 1 ? _report.targets[1].pos_x : 0.0f)
                         //<< ";" << (_report.validTargets > 1 ? _report.targets[1].pos_y : 0.0f)
                         << ";" << _report.distance
                         << ";" << _report.orientation
                         << ";" << unsigned(_lastImgNb);

            if(_callback) {
                _callback(_report);
            }
        }
    }
    else {
        uint8_t byte = ARUCO_DATA_REG;

        if(_i2c->set(_address, byte)) {
            return;
        }

        _collect_phase = true;


        _lastDemandTime = std::chrono::steady_clock::now();
    }
}

/** probe the device is on the I2C bus **/
int RpiArucoDetector::probe()
{
    uint8_t byte = ARUCO_ID_REG;

    //Let empty the slave buffer
    uint8_t discard[25];
    _i2c->get(_address, discard, 25);

    _i2c->set(_address, byte);

    usleep(1000);

    if (_i2c->get(_address, &byte, 1) || byte != ARUCO_ID) {
        LogWarn() << "Unable to write to Urco RPI or ID mismatch";
        return 1;
    }

    return 0;
}

/** read all available frames from sensor **/
int RpiArucoDetector::read_device(uint8_t &validTarget)
{
    _report.epochOfMeasure = std::chrono::steady_clock::now();

    if (read_device_block(validTarget)) {
        return 1;
    }

    return 0;
}

/** read a single block (a full frame) from sensor **/
int RpiArucoDetector::read_device_block(uint8_t &nbValidTarget)
{
    static bool _failed = false;

    uint8_t buffer[ARUCO_PAYLOAD_SIZE];
    memset(buffer, 0, sizeof buffer);

    int status = _i2c->get(_address, buffer, ARUCO_PAYLOAD_SIZE);

    if(status) {
        _i2c_errors++;
        return status;
    }

    uint16_t pixel_x[2];
    uint16_t pixel_y[2];
    uint16_t orientation, size;
    uint8_t validTarget, imgNb, delay, checksum;

    validTarget = buffer[0];
    imgNb = buffer[1];
    pixel_x[0] = buffer[3] << 8 | buffer[2];
    pixel_y[0] = buffer[5] << 8 | buffer[4];
    pixel_x[1] = buffer[7] << 8 | buffer[6];
    pixel_y[1] = buffer[9] << 8 | buffer[8];
    orientation = buffer[11] << 8 | buffer[10];
    size = buffer[13] << 8 | buffer[12];
    delay = buffer[14];
    checksum = buffer[15];

    //Compute checksum of received message anc compare to received one
    uint8_t recvChecksum = 0;
    for(int i = 0; i < ARUCO_PAYLOAD_SIZE - 1; i++) {
        recvChecksum += buffer[i];
    }

    if(recvChecksum != checksum) {
        //If checksum mismatch, let's do a dummy read to clear the slave buffer
        uint8_t dummy[25];
        _i2c->get(_address, dummy, 25);

        if(_failed == false) {
            LogWarn() << "Checksum mismatch, received : " << unsigned(checksum) << " but calculate : " << unsigned(recvChecksum);
        }

        _failed = true;
        _read_failures++;
        return 1;
    }
    else if(_failed) {
        LogWarn() << "Checksum match again";
        _failed = false;
    }

    //If image is same as last one, let's discard that measurment
    if(imgNb == _lastImgNb) {
        _same_image++;
        return 1;
    }
    else {
        _lastImgNb = imgNb;
    }

    //If the delay is bigger than 1s (20ms * 50) discard the measurment
    if(delay > 50) {
        _outdated++;
        if(_lastOutdated == false) {
            _lastOutdated = true;
            LogWarn() << "Rejecting measurment : outdated > 1s";
        }
        return 1;
    }
    else if(_lastOutdated) {
        _lastOutdated = false;
        LogWarn() << "New measurment : with dt < 1s received.";
    }

    for(int i = 0; i < wer::cargo::MAX_LANDING_TARGETS; i++) {
        _report.targets[i].valid = false;
        //_report.targets[i].index = 0;
    }

    nbValidTarget = 0;
    if(validTarget) {
        _report.orientation = orientation;
        _report.epochOfMeasure = _lastDemandTime - std::chrono::milliseconds(delay*20);
        if(validTarget & 0x04) {
            /** convert to angles **/
            //_report.targets[0].index = 0;
            _report.targets[0].pos_x = (pixel_x[0] - ARUCO_CENTER_X) * ARUCO_TAN_ANG_PER_PIXEL_X;
            _report.targets[0].pos_y = (pixel_y[0] - ARUCO_CENTER_Y) * ARUCO_TAN_ANG_PER_PIXEL_Y;
            _report.targets[0].valid = true;
            nbValidTarget++;

            _report.distance = (ARUCO_TAG_SIZE_3 * ARUCO_RES_X / 2) / (ARUCO_TAN_HALF_FOV_X * size);
        }
        else {

        }

        if(validTarget & 0x02) {
            //_report.targets[1].index = 1;
            _report.targets[1].pos_x = (pixel_x[nbValidTarget] - ARUCO_CENTER_X) * ARUCO_TAN_ANG_PER_PIXEL_X;
            _report.targets[1].pos_y = (pixel_y[nbValidTarget] - ARUCO_CENTER_Y) * ARUCO_TAN_ANG_PER_PIXEL_Y;
            _report.targets[1].valid = true;
            nbValidTarget++;

            _report.distance = (ARUCO_TAG_SIZE_2 * ARUCO_RES_X / 2) / (ARUCO_TAN_HALF_FOV_X * size);
        }
        if(validTarget & 0x01) {
            if(nbValidTarget < 2) {
                /** convert to angles **/
                //_report.targets[2].index = 2;
                _report.targets[2].pos_x = (pixel_x[nbValidTarget] - ARUCO_CENTER_X) * ARUCO_TAN_ANG_PER_PIXEL_X;
                _report.targets[2].pos_y = (pixel_y[nbValidTarget] - ARUCO_CENTER_Y) * ARUCO_TAN_ANG_PER_PIXEL_Y;
                _report.targets[2].valid = true;
                nbValidTarget++;
            }

            _report.distance = (ARUCO_TAG_SIZE_1 * ARUCO_RES_X / 2) / (ARUCO_TAN_HALF_FOV_X * size);
        }

        //_reports.validtargets = nbValidTarget;
    }
    else {
        //_reports.validtargets = 0;
        _report.orientation = 0;
        _report.distance = 0;
    }

    if(_lastValidData != validTarget) {
        LogInfo() << "Aruco detected tags : " << unsigned(validTarget);
        _lastValidData = validTarget;
    }
    
    return 0;
}
