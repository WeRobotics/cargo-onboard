#include "irlock.h"

#include <cstring>
#include <iostream>
#include <iomanip>

#include <Library/logging/log.h>
#include <Library/global_include.h>

static constexpr uint16_t IRLOCK_SYNC   =   0xAA55;
static constexpr uint16_t IRLOCK_RESYNC =   0x5500;
static constexpr uint8_t IRLOCK_ADJUST  =   0xAA;

static constexpr uint16_t IRLOCK_RES_X = 320;
static constexpr uint16_t IRLOCK_RES_Y = 200;

static constexpr uint16_t IRLOCK_CENTER_X = (IRLOCK_RES_X/2);            // the x-axis center pixel position
static constexpr uint16_t IRLOCK_CENTER_Y = (IRLOCK_RES_Y/2);            // the y-axis center pixel position

static constexpr float IRLOCK_FOV_X = (60.0f*M_PI_F/180.0f);
static constexpr float IRLOCK_FOV_Y = (35.0f*M_PI_F/180.0f);

static constexpr float IRLOCK_TAN_HALF_FOV_X = 0.57735026919f; // tan(0.5 * 60 * pi/180)
static constexpr float IRLOCK_TAN_HALF_FOV_Y = 0.31529878887f; // tan(0.5 * 35 * pi/180)

static constexpr float IRLOCK_TAN_ANG_PER_PIXEL_X = (2*IRLOCK_TAN_HALF_FOV_X/IRLOCK_RES_X);
static constexpr float IRLOCK_TAN_ANG_PER_PIXEL_Y = (2*IRLOCK_TAN_HALF_FOV_Y/IRLOCK_RES_Y);

/** constructor **/
Irlock::Irlock(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<wer::hal::I2c> i2c, uint8_t address) :
_address(address),
_reports_index(0),
_sensor_ok(false),
_read_failures(0),
_call_every_handler(call_every_handler),
_i2c(i2c)
{
    memset(_reports, 0, sizeof _reports);
}

/** destructor **/
Irlock::~Irlock()
{

}

/** initialise driver to communicate with sensor **/
wer::lib::ProcessResult Irlock::init()
{
    if(probe()) {
        return wer::lib::ProcessResult::NOT_CONNECTED;
    }

    _sensor_ok = true;

    return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult Irlock::start() {
    _call_every_handler->add(std::bind(&Irlock::cycle, this), 0.5f, &_timer_cookie);

    return wer::lib::ProcessResult::SUCCESS;
}

wer::lib::ProcessResult Irlock::stop() {
    _call_every_handler->remove(_timer_cookie);

    return wer::lib::ProcessResult::SUCCESS;
}

void Irlock::setMeasureCallback(std::function<void(struct irlock_report_s)> callback) {
    _callback = callback;
}

/*
void Irlock::setBackupCallback(std::function<void(void)> callback) {
    _backupCallback = callback;
}*/

//std::function<void(struct irlock_report_s)> process
void Irlock::cycle() {
    if(read_device()) {
        return;
    }
    else {
        if(/*process && */_reports[_reports_index].num_targets > 0) {
            struct irlock_report_s report;

            report.timestamp = _reports[_reports_index].timestamp;
            report.target.signature = _reports[_reports_index].targets[0].signature;
            report.target.pos_x     = _reports[_reports_index].targets[0].pos_x;
            report.target.pos_y     = _reports[_reports_index].targets[0].pos_y;
            report.target.size_x    = _reports[_reports_index].targets[0].size_x;
            report.target.size_y    = _reports[_reports_index].targets[0].size_y;

            if(_callback) {
                _callback(report);
            }
		/*
		//std::cout << "Timestamp : " << duration_cast<milliseconds>(report.timestamp.time_since_epoch()) << std::endl;
		std::cout << "Signature : " << unsigned(report.target.signature) << std::endl;
		std::cout << "Pos_x : " << report.target.pos_x << std::endl;
		std::cout << "Pos_y : " << report.target.pos_y << std::endl;
		std::cout << "Size_x : " << report.target.size_x << std::endl;
		std::cout << "Size_y : " << report.target.size_y << std::endl;
		*/
	
            //process(report);
        }
    }
}

/** probe the device is on the I2C bus **/
int Irlock::probe()
{
    /*
     * IRLock defaults to sending 0x00 when there is no block
     * data to return, so really all we can do is check to make
     * sure a transfer completes successfully.
     **/
    uint8_t byte;

    if (_i2c->get(_address, &byte, 1)) {
        return 1;
    }

    return 0;
}

/** test driver **/
int Irlock::test()
{
    /** exit immediately if sensor is not healty **/
    if (!_sensor_ok) {
        LogErr() << "Sensor is not healthy.";
        return 1;
    }

    /** output all objects found **/
    for (uint8_t i = 0; i < _reports[_reports_index].num_targets; i++) {
        LogWarn() << "sig:" << (int)_reports[_reports_index].targets[i].signature
                  << " X:" << (double)_reports[_reports_index].targets[i].pos_x
                  << " Y:" << (double)_reports[_reports_index].targets[i].pos_y
                  << " width:" << (double)_reports[_reports_index].targets[i].size_x
                  << " height:" << (double)_reports[_reports_index].targets[i].size_y;
    }
    return 0;
}

/** sync device to ensure reading starts at new frame*/
bool Irlock::sync_device()
{
    uint8_t sync_byte;
    uint16_t sync_word;

    if (read_device_word(&sync_word)) {
        return false;
    }

    if (sync_word == IRLOCK_RESYNC) {
        _i2c->get(_address, &sync_byte, 1);

        if (sync_byte == IRLOCK_ADJUST) {
            return true;
        }

    } else if (sync_word == IRLOCK_SYNC) {
        return true;
    }

    return false;
}

/** read all available frames from sensor **/
int Irlock::read_device()
{

    /** if we sync, then we are starting a new frame, else fail **/
    if (!sync_device()) {
        return 1;
    }

    struct irlock_s report;

    report.timestamp = std::chrono::steady_clock::now();

    report.num_targets = 0;

    while (report.num_targets < IRLOCK_OBJECTS_MAX) {
        if (!sync_device() || read_device_block(&report.targets[report.num_targets])) {
            break;
        }

        report.num_targets++;
    }

    _reports_index = (_reports_index + 1) % REPORTS_OBJECTS_MAX;
    memcpy(&(_reports[_reports_index]), &report, sizeof report);

    return 0;
}

/** read a word (two bytes) from sensor **/
int Irlock::read_device_word(uint16_t *word)
{
    uint8_t bytes[2];
    memset(bytes, 0, sizeof bytes);

    int status = _i2c->get(_address, bytes, 2);

    *word = bytes[1] << 8 | bytes[0];

    return status;
}

/** read a single block (a full frame) from sensor **/
int Irlock::read_device_block(struct irlock_target_s *block)
{
    uint8_t bytes[12];
    memset(bytes, 0, sizeof bytes);

    int status = _i2c->get(_address, bytes, 12);

    uint16_t checksum = bytes[1] << 8 | bytes[0];
    uint16_t signature = bytes[3] << 8 | bytes[2];
    uint16_t pixel_x = bytes[5] << 8 | bytes[4];
    uint16_t pixel_y = bytes[7] << 8 | bytes[6];
    uint16_t pixel_size_x = bytes[9] << 8 | bytes[8];
    uint16_t pixel_size_y = bytes[11] << 8 | bytes[10];

    /** crc check **/
    if (signature + pixel_x + pixel_y + pixel_size_x + pixel_size_y != checksum) {
        _read_failures++;
        return 1;
    }

    /** convert to angles **/
    block->signature = signature;
    block->pos_x = (pixel_x - IRLOCK_CENTER_X) * IRLOCK_TAN_ANG_PER_PIXEL_X;
    block->pos_y = (pixel_y - IRLOCK_CENTER_Y) * IRLOCK_TAN_ANG_PER_PIXEL_Y;
    block->size_x = pixel_size_x * IRLOCK_TAN_ANG_PER_PIXEL_X;
    block->size_y = pixel_size_y * IRLOCK_TAN_ANG_PER_PIXEL_Y;
    return status;
}
