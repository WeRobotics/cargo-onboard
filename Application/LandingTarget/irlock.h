#pragma once

#include <chrono>
#include <functional>

#include <Library/timer/call_every_handler.h>

#include <Hardware/include/i2c.h>

#include "landing_target.h"

/** Configuration Constants **/
#define IRLOCK_I2C_ADDRESS      0x54 /** 7-bit address (non shifted) **/

#define IRLOCK_OBJECTS_MAX  5   /** up to 5 objects can be detected/reported **/

#define REPORTS_OBJECTS_MAX 2

struct irlock_target_s {
    uint16_t signature; /** target delay in 20ms // signature **/
    float pos_x;    /** x-axis distance from center of image to center of target in units of tan(theta) **/
    float pos_y;    /** y-axis distance from center of image to center of target in units of tan(theta) **/
    float size_x;   /** size of target along x-axis in units of tan(theta) **/
    float size_y;   /** size of target along y-axis in units of tan(theta) **/
};

struct irlock_report_s {
    std::chrono::time_point<std::chrono::steady_clock> timestamp;
    struct irlock_target_s target;
};

/** irlock_s structure returned from read calls **/
struct irlock_s {
    std::chrono::time_point<std::chrono::steady_clock> timestamp; /** microseconds since system start **/
    uint8_t num_targets;
    struct irlock_target_s targets[IRLOCK_OBJECTS_MAX];
};

class Irlock : public wer::cargo::LandingTarget {
public:
    Irlock(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<wer::hal::I2c> i2c, uint8_t address = IRLOCK_I2C_ADDRESS);
    ~Irlock();

    wer::lib::ProcessResult init();
    wer::lib::ProcessResult start();
    wer::lib::ProcessResult stop();

    int test();

    void cycle();
    int probe();

    void setMeasureCallback(std::function<void(struct irlock_report_s)> callback);
    void setBackupCallback(std::function<void(void)> callback);

private:

    /** low level communication with sensor **/
    int         read_device();
    bool        sync_device();
    int         read_device_word(uint16_t *word);
    int         read_device_block(struct irlock_target_s *block);

    /** internal variables **/
    uint8_t _address;

    struct irlock_s _reports[REPORTS_OBJECTS_MAX];
    uint8_t _reports_index;

    bool _sensor_ok;
    uint32_t _read_failures;

    std::function<void(struct irlock_report_s)> _callback;

    std::shared_ptr<CallEveryHandler> _call_every_handler;
    void* _timer_cookie;

    std::shared_ptr<wer::hal::I2c> _i2c;

    //std::function<void(void)> _backupCallback;
};
