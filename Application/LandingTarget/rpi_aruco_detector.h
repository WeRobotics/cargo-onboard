#pragma once

#include <chrono>
#include <functional>
#include <atomic>
#include <thread>
#include <chrono>

#include <Library/timer/call_every_handler.h>
#include <Hardware/include/i2c.h>

#include "landing_target.h"

/** Configuration Constants **/
#define ARUCO_I2C_ADDRESS      0x13 /** 7-bit address (non shifted) **/

enum ArucoLogging
{
    LOG_NONE,
    LOG_ALL,
    LOG_ALL_TAR,
    LOG_RAW_TAR,
    LOG_RAW_ALL,
    LOG_RES_TAR,
    LOG_RES_ALL
};

enum ArucoParam {
    ARUCO_MAX_TARGET = 3,
};

class RpiArucoDetector : public wer::cargo::LandingTarget {
public:
    RpiArucoDetector(std::shared_ptr<CallEveryHandler> call_every_handler, std::shared_ptr<wer::hal::I2c> i2c, uint8_t address = ARUCO_I2C_ADDRESS);
    ~RpiArucoDetector();

    wer::lib::ProcessResult init();
    wer::lib::ProcessResult start();
    wer::lib::ProcessResult stop();

    void cycle();
    int probe();

    void setMeasureCallback(std::function<void(struct wer::cargo::landing_report_s)> callback);
    void setLoggingLevel(ArucoLogging logLevel);

private:

    /** low level communication with sensor **/
    int read_device(uint8_t &validTarget);
    int read_device_block(uint8_t &nbValidTarget);

    /** internal variables **/
    uint8_t _address;

    struct wer::cargo::landing_report_s _report;

    bool _sensor_ok;
    uint32_t _read_failures;
    uint32_t _i2c_errors;
    uint32_t _same_image;
    uint32_t _outdated;

    std::function<void(struct wer::cargo::landing_report_s)> _callback;
    bool _collect_phase;

    uint8_t _lastImgNb;
    uint8_t _lastValidData;
    uint8_t _lastOutdated;

    std::chrono::time_point<std::chrono::steady_clock> _lastDemandTime;

    std::shared_ptr<CallEveryHandler> _call_every_handler;
    void* _timer_cookie;

    std::shared_ptr<wer::hal::I2c> _i2c;
};
