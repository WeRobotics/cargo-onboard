#pragma once

#include <string>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>
#include <limits>
#include <functional>
#include <map>

#include <opencv2/core.hpp>
#include <opencv2/aruco.hpp>


#include <raspicam/raspicam_still_cv.h>
#include <raspicam/raspicam_cv.h>

//#include "cameraParameters.h"
#include "landing_target.h"

using namespace std;
using namespace cv;

namespace wer {
	namespace cargo {

		constexpr float CAM_OFFSET_X = 0.0f;
		constexpr float CAM_OFFSET_Y = 0.0f;
		//#define OFFSET_Y -0.03

		enum ArUcoTerminalLogging {
			NONE	= 0,
			CSV		= 1,
			STATS	= 2
		};

		class CameraArucoDetector: public LandingTarget {
		public:
			CameraArucoDetector(float offset_x = CAM_OFFSET_X, float offset_y = CAM_OFFSET_Y);	//if =0, save no images; if set, the bits have the following meanings : byte0=log_raw_image_if_detection ; byte1=always_log_raw_image (if set, byte 0 doesn't matters) ; byte2=log_resulting_image_if_detection (ie with squares around tags and IDs) ; byte3=always_log_resulting_image (if set, byte 2 doesn't matters) ; 
			~CameraArucoDetector();

			struct tagData {
				uint16_t xCenter;
				uint16_t yCenter;
				float distance;
				float orientation;
			};

			struct imageReport {
				uint16_t imageId;
				std::chrono::time_point<std::chrono::steady_clock> pictureTimestamp;
				bool valid[wer::cargo::MAX_LANDING_TARGETS]; 
				struct tagData tags[wer::cargo::MAX_LANDING_TARGETS];
			};
			
			void generateArUcoMarker(Mat &returnMarker, int marker_ID);
			void generateAndSaveArUcoMarkers();

			void setMeasureCallback(std::function<void(struct wer::cargo::landing_report_s)> callback);

			wer::lib::ProcessResult init();
			wer::lib::ProcessResult start();
			wer::lib::ProcessResult stop();

			//We are working with an internal thread. No need to have a cycle function implemented
			void cycle() {};
			
			std::map<uint8_t, struct tagData> processImage(Mat& image);

			void runOnFolder(std::string path, ArUcoTerminalLogging logLevel);
			void getFileImage(Mat &image, std::string filename);

		private:
			const float _offset_x;
			const float _offset_y;

			void workingTread();

			uint8_t writeReport(std::map<uint8_t, struct tagData> tags, uint16_t imageId, std::chrono::time_point<std::chrono::steady_clock> timestamp);
			void sendRepport();
			
			void getCameraImage(Mat &image);
			raspicam::RaspiCam_Cv _camera;
			
			void printReportHeader();
			void printReport();
			void printStats(float totalProcessTimeMs, uint16_t processImages, std::vector<float> processTimeMs, std::vector<float> captureTimeMs, uint16_t tagCounter);
			void computeStats(std::vector<float> data, float &min, float &max, float &mean, float &std);

			float computeTagOrientation(vector<Point2f> &markerCorners);
			float computeTagDistance(vector<Point2f> corners, float tagSize);

			static std::map<uint8_t, float> _tags;
			static std::map<uint8_t, uint8_t> _tagToTagId;

			Ptr<aruco::Dictionary> _dictionary;

			std::chrono::time_point<std::chrono::steady_clock> _startTime;

			std::atomic<bool> _started;
			std::atomic<bool> _shouldExit;
			std::thread *_ArUcoDetection_thread{nullptr};

			std::vector<float> _processTime;
			std::vector<float> _captureTime;
			uint16_t _imageCounter;
			uint16_t _tagCounter;

			struct imageReport _imageReport;
			mutex _report_mtx;

			std::function<void(struct wer::cargo::landing_report_s)> _callback;
		};
	}
}