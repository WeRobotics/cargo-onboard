#pragma once

#include <chrono>
#include <functional>

#include <Library/process_result.h>

namespace wer {
    namespace cargo {
        constexpr uint8_t MAX_LANDING_TARGETS = 3;

        struct landing_target_s {
            //uint8_t targetId;
            bool valid;
            float pos_x;
            float pos_y;
            float landTargetOffset_x;
            float landTargetOffset_y;
        };

        struct landing_report_s {
            std::chrono::time_point<std::chrono::steady_clock> epochOfMeasure;
            float distance;
            float orientation;

            struct landing_target_s targets[MAX_LANDING_TARGETS];
        };

        class LandingTarget
        {
        public:

            virtual void setMeasureCallback(std::function<void(struct landing_report_s)> callback) = 0;

            virtual wer::lib::ProcessResult init() = 0;
            virtual wer::lib::ProcessResult start() = 0;
            virtual wer::lib::ProcessResult stop() = 0;

            virtual void cycle() = 0;
        };
    }
}