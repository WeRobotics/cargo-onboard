#include "camera_aruco_detector.h"

#include <sys/time.h>

#include <iostream>
#include <string>
#include <regex>
#include <cmath>
#include <experimental/filesystem>

//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui.hpp>
#include "opencv2/imgproc.hpp"

#include <raspicam/raspicam_still_cv.h>
#include <raspicam/raspicam_cv.h>

#include <Library/logging/log.h>

//#include "pigpio.h"

//#define USE_CORNERS_AND_EDGES //comment if you rather use the center of the tags
//#define SHOW_IMAGES false


using namespace cv;
using namespace std;
namespace fs = std::experimental::filesystem;

#define M_PI_F (float) M_PI

#define CAMERA_MAX_RES_X 1920
#define CAMERA_MAX_RES_Y 1440

#define CAMERA_FOV_X (61.92f*M_PI_F/180.0f)
#define CAMERA_FOV_Y (48.46f*M_PI_F/180.0f)

#define CAMERA_TAN_HALF_FOV_X 0.60 // tan(0.5 * 60 * pi/180)
#define CAMERA_TAN_HALF_FOV_Y 0.45 // tan(0.5 * 35 * pi/180)

#define CAMERA_TAN_ANG_PER_PIXEL_X  (2*CAMERA_TAN_HALF_FOV_X/CAMERA_MAX_RES_X)
#define CAMERA_TAN_ANG_PER_PIXEL_Y  (2*CAMERA_TAN_HALF_FOV_Y/CAMERA_MAX_RES_Y)

#define DOWNSAMPLE_LEVEL 0

//Tag are sorted from bigger to smalller (This is used later in the code)
std::map<uint8_t, float> wer::cargo::CameraArucoDetector::_tags = {{2, 1.5f}, {3, 0.7f}, {4, 0.36f}};
std::map<uint8_t, uint8_t> wer::cargo::CameraArucoDetector::_tagToTagId = {{2, 2}, {1, 3}, {0, 4}};


wer::cargo::CameraArucoDetector::CameraArucoDetector(float offset_x, float offset_y) :
_offset_x(offset_x),
_offset_y(offset_y),
_started(false),
_shouldExit(false),
_imageCounter(0),
_tagCounter(0),
_callback(nullptr)
{
  _dictionary  =  aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);

  LogInfo() << "Camera will use offsets : X=" << _offset_x << " Y=" << offset_y;
}

wer::cargo::CameraArucoDetector::~CameraArucoDetector() {
  _shouldExit = true;
  if(_ArUcoDetection_thread != nullptr && _ArUcoDetection_thread->joinable()) {
    _ArUcoDetection_thread->join();
    delete(_ArUcoDetection_thread);
    _ArUcoDetection_thread = nullptr;
  }
}

wer::lib::ProcessResult wer::cargo::CameraArucoDetector::init() {

  _camera.set(CAP_PROP_FRAME_WIDTH, CAMERA_MAX_RES_X);
  _camera.set(CAP_PROP_FRAME_HEIGHT, CAMERA_MAX_RES_Y);

  if (!_camera.open()) {
    LogErr() << "Error opening the camera";
    return wer::lib::ProcessResult::NOT_CONNECTED;
  }


  _shouldExit = false;
  _ArUcoDetection_thread  =  new std::thread(&wer::cargo::CameraArucoDetector::workingTread, this);

  return wer::lib::ProcessResult::SUCCESS;
}

void wer::cargo::CameraArucoDetector::generateArUcoMarker(Mat &returnMarker, int marker_ID) {
  Ptr<aruco::Dictionary> dictionary  =  aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_50);
  cv::aruco::drawMarker(dictionary, marker_ID, 200, returnMarker, 1);
}

void wer::cargo::CameraArucoDetector::generateAndSaveArUcoMarkers() {
  Mat m;

  for(auto it = _tags.begin(); it != _tags.end(); it++) {
    this->generateArUcoMarker(m, it->first);
    String path = "./marker_id_" + std::to_string(it->first) + "_size_" + std::to_string(it->second) + ".jpg";
    imwrite(path, m);
  }
}


wer::lib::ProcessResult wer::cargo::CameraArucoDetector::start() {
  _processTime.clear();
  _captureTime.clear();
  _imageCounter = 0;
  _tagCounter = 0;

  _startTime = std::chrono::steady_clock::now();

  if(_camera.isOpened() /* && thread running ?*/) {
    _started = true; 
    return wer::lib::ProcessResult::SUCCESS;
  }
  else {
    return wer::lib::ProcessResult::NOT_CONNECTED;
  }

  return wer::lib::ProcessResult::UNKNOWN;
}

wer::lib::ProcessResult wer::cargo::CameraArucoDetector::stop() {
  _started = false;

  auto endTime = std::chrono::steady_clock::now();
  float dt = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - _startTime).count();

  printStats(dt, _imageCounter, _processTime, _captureTime, _tagCounter);

  return wer::lib::ProcessResult::SUCCESS;
}

float wer::cargo::CameraArucoDetector::computeTagOrientation(vector<Point2f> &markerCorners) {
  float M1_x = (markerCorners[0].x + markerCorners[1].x) / 2;
  float M1_y = (markerCorners[0].y + markerCorners[1].y) / 2;
  float M2_x = (markerCorners[3].x + markerCorners[4].x) / 2;
  float M2_y = (markerCorners[3].y + markerCorners[4].y) / 2;
  float vect_M2M1_x = M1_x - M2_x;
  float vect_M2M1_y = M1_y - M2_y;

  return atan2(vect_M2M1_y, vect_M2M1_x) * 180 / M_PI;
}

float wer::cargo::CameraArucoDetector::computeTagDistance(vector<Point2f> corners, float tagSize) {
  //Assumption : The camera is parrallel to the tag surface.
  //cout << "Tag size :" << tagSize << endl;
  float distance = 0.0f;
  for(int i = 0; i < 4; i++) {
    //cout << "Corner " << i << " : x = " << corners[i].x << "\t y = " << corners[i].y << endl;
    int id1 = i;
    int id2 = i + 1 % 4;
    float xSize = tan(CAMERA_FOV_X / 2.0f - CAMERA_FOV_X / CAMERA_MAX_RES_X * corners[id1].x) - tan(CAMERA_FOV_X / 2.0f - CAMERA_FOV_X / CAMERA_MAX_RES_X * corners[id2].x);
    float ySize = tan(CAMERA_FOV_Y / 2.0f - CAMERA_FOV_Y / CAMERA_MAX_RES_Y * corners[id1].y) - tan(CAMERA_FOV_Y / 2.0f - CAMERA_FOV_Y / CAMERA_MAX_RES_Y * corners[id2].y);
    distance += tagSize / sqrtf(pow(xSize, 2) + pow(ySize, 2));
  }

  //cout << "Disance = " << distance / 4.0f << endl;
  return distance / 4.0f;
}

std::map<uint8_t, struct wer::cargo::CameraArucoDetector::tagData> wer::cargo::CameraArucoDetector::processImage(Mat& image) {
  vector<int> markerIds;
  vector<vector<Point2f>> markerCorners, rejectedCandidates;
  Ptr<aruco::DetectorParameters> parameters = aruco::DetectorParameters::create();
  aruco::detectMarkers(image, _dictionary, markerCorners, markerIds, parameters, rejectedCandidates);

  std::map<uint8_t, struct tagData> detectedTags;

  struct tagData detectedTag;

  for(unsigned int i = 0; i < markerIds.size(); i++) {
    if(_tags.find(markerIds[i]) != _tags.end()) {
      detectedTag.xCenter = (markerCorners[i][0].x + markerCorners[i][1].x + markerCorners[i][2].x + markerCorners[i][3].x) / 4;
      detectedTag.yCenter = (markerCorners[i][0].y + markerCorners[i][1].y + markerCorners[i][2].y + markerCorners[i][3].y) / 4;;
      detectedTag.distance = computeTagDistance(markerCorners[i], _tags[markerIds[i]]);
      detectedTag.orientation = computeTagOrientation(markerCorners[i]);

      detectedTags[markerIds[i]] = detectedTag;
    }
  }

  return detectedTags;
}

void wer::cargo::CameraArucoDetector::getCameraImage(Mat &image) {
  _camera.grab();
  _camera.retrieve(image);
}

void wer::cargo::CameraArucoDetector::getFileImage(Mat &image, std::string filename) {
  image = imread(filename.c_str(), IMREAD_COLOR);
}

void wer::cargo::CameraArucoDetector::runOnFolder(std::string path, ArUcoTerminalLogging logLevel) {
  Mat image;

  //Let initialise the stats variables
  _processTime.clear();
  _captureTime.clear();
  _imageCounter = 0;
  _tagCounter = 0;

  std::chrono::time_point<std::chrono::steady_clock> startTime = std::chrono::steady_clock::now();

  if(logLevel == ArUcoTerminalLogging::CSV) {
    printReportHeader();
  }

  std::regex regex(std::string("^initial_[0-9]+\\.jpg$"));
  for (const auto & entry : fs::directory_iterator(path)) {
    if(std::regex_match(std::string(entry.path().filename()), regex)) {

      //Load the image
      std::chrono::time_point<std::chrono::steady_clock>  startImageLoadTS = std::chrono::steady_clock::now();
      getFileImage(image, entry.path());

      //Process the images
      std::chrono::time_point<std::chrono::steady_clock>  startImageProcessTS = std::chrono::steady_clock::now();

      for(int i = 0; i < DOWNSAMPLE_LEVEL; i++)
      {
        pyrDown(image, image);  //downsample
      }

      std::map<uint8_t, struct tagData> tags = processImage(image);
      _processTime.push_back(static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startImageProcessTS).count()));
      _captureTime.push_back(static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(startImageProcessTS - startImageLoadTS).count()));

      _tagCounter += writeReport(tags, _imageCounter, startImageProcessTS);

      if(logLevel == ArUcoTerminalLogging::CSV) {
        printReport();
      }

      _imageCounter++;
    }
  }

  std::chrono::time_point<std::chrono::steady_clock> endTime = std::chrono::steady_clock::now();

  float dt = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();

  printStats(dt, _imageCounter, _processTime, _captureTime, _tagCounter);
}

uint8_t wer::cargo::CameraArucoDetector::writeReport(std::map<uint8_t, struct tagData> tags, uint16_t imageId, std::chrono::time_point<std::chrono::steady_clock> timestamp) {
  static const struct tagData nanTag = {0xFFFF, 0xFFFF, std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN()};
  uint8_t nbTag = 0;


  _report_mtx.lock();
  _imageReport.imageId = imageId;
  _imageReport.pictureTimestamp = timestamp;
  for(int i = 0; i < wer::cargo::MAX_LANDING_TARGETS; i++) {

    _imageReport.valid[i] = tags.find(_tagToTagId[i]) != tags.end();

    if(_imageReport.valid[i]) {
      _imageReport.tags[i] = tags[_tagToTagId[i]];
      nbTag++;
    }
    else {
      _imageReport.tags[i] = nanTag;
    }

  }
  _report_mtx.unlock();

  return nbTag;
}

void wer::cargo::CameraArucoDetector::setMeasureCallback(std::function<void(struct wer::cargo::landing_report_s)> callback) {
    _callback = callback;
}

void wer::cargo::CameraArucoDetector::sendRepport() {
  if(_callback) {
    struct wer::cargo::landing_report_s report;
    _report_mtx.lock();
    report.epochOfMeasure = _imageReport.pictureTimestamp;
    report.distance = 0.0f;
    report.orientation = 0.0f;

    int j = 0;
    for(int i = 0; i < wer::cargo::MAX_LANDING_TARGETS; i++) {
      if(_imageReport.valid[i]) {
        report.distance += _imageReport.tags[i].distance;
        report.orientation += _imageReport.tags[i].orientation;
        report.targets[i].pos_x = (_imageReport.tags[i].xCenter - CAMERA_MAX_RES_X / 2.0f) * CAMERA_TAN_ANG_PER_PIXEL_X + _offset_x;
        report.targets[i].pos_y = (_imageReport.tags[i].yCenter - CAMERA_MAX_RES_Y / 2.0f) * CAMERA_TAN_ANG_PER_PIXEL_Y + _offset_y;
        report.targets[i].valid = true;
        j++;
      }
      else {
        report.targets[i].valid = false;
      }
    }
    
    report.distance /= j;
    report.orientation /= j;
    _report_mtx.unlock();
    _callback(report);
  }
}

void wer::cargo::CameraArucoDetector::printReportHeader() {
  cout << "image_id;"
       << "valid_1;x_center_1;y_center_1;dist_1;angle_1;"
       << "valid_2;x_center_2;y_center_2;dist_2;angle_2;"
       << "valid_3;x_center_3;y_center_3;dist_3;angle_3" << endl;
}

void wer::cargo::CameraArucoDetector::printReport() {
  cout << unsigned(_imageReport.imageId) << ";" 
       << unsigned(_imageReport.valid[0]) << ";" << unsigned(_imageReport.tags[0].xCenter) << ";" << unsigned(_imageReport.tags[0].yCenter) << ";" << _imageReport.tags[0].distance << ";" << _imageReport.tags[0].orientation << ";"
       << unsigned(_imageReport.valid[1]) << ";" << unsigned(_imageReport.tags[1].xCenter) << ";" << unsigned(_imageReport.tags[1].yCenter) << ";" << _imageReport.tags[1].distance << ";" << _imageReport.tags[1].orientation << ";"
       << unsigned(_imageReport.valid[2]) << ";" << unsigned(_imageReport.tags[2].xCenter) << ";" << unsigned(_imageReport.tags[2].yCenter) << ";" << _imageReport.tags[2].distance << ";" << _imageReport.tags[2].orientation << endl;
}

void wer::cargo::CameraArucoDetector::printStats(float totalProcessTimeMs, uint16_t processImages, std::vector<float> processTimeMs, std::vector<float> captureTimeMs, uint16_t tagCounter) {
  float meanProcessTime, stdProcessTime, maxProcessTime, minProcessTime;
  float meanCaptureTime, stdCaptureTime, maxCaptureTime, minCaptureTime;

  computeStats(processTimeMs, minProcessTime, maxProcessTime, meanProcessTime, stdProcessTime);

  cout << processImages << " images processed in " << totalProcessTimeMs << "ms. Total of " << tagCounter << " tags detected." << endl 
       << "Process time - Mean : " << meanProcessTime << "ms, Std : " << stdProcessTime << "ms, Max : " << maxProcessTime << "ms, Min : " << minProcessTime << "ms," << endl;

  if(captureTimeMs.size() > 0) {
    computeStats(captureTimeMs, minCaptureTime, maxCaptureTime, meanCaptureTime, stdCaptureTime);
    cout << "Capture time - Mean : " << meanCaptureTime << "ms, Std : " << stdCaptureTime << "ms, Max : " << maxCaptureTime << "ms, Min : " << minCaptureTime << "ms," << endl;
  }
}

void wer::cargo::CameraArucoDetector::computeStats(std::vector<float> data, float &min, float &max, float &mean, float &std) {
  std = 0.0f;
  mean = 0.0f;
  min = std::numeric_limits<float>::quiet_NaN();
  max = std::numeric_limits<float>::quiet_NaN();

  for(auto it = data.begin(); it != data.end(); it++) {
     mean += *it;
     min = min < *it ? min : *it;
     max = max > *it ? max : *it;
  }
  mean /= data.size();

  for(auto it = data.begin(); it != data.end(); it++) {
     std += pow(*it - mean, 2);
  }
  std = sqrtf(std / data.size());
}

void wer::cargo::CameraArucoDetector::workingTread() {
  Mat image;
  
  while(!_shouldExit)
  {
    if(_started)
    {
      auto pictureCaptureTime = std::chrono::steady_clock::now();

      getCameraImage(image);

      auto pictureProcessTime = std::chrono::steady_clock::now();
      std::map<uint8_t, struct tagData> tags = processImage(image);
      
      _processTime.push_back(static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - pictureProcessTime).count()));
      _captureTime.push_back(static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(pictureProcessTime - pictureCaptureTime).count()));

      uint8_t tagDetected = writeReport(tags, _imageCounter, pictureCaptureTime);
      _tagCounter += tagDetected;

      //printReport();
      if(tagDetected > 0) {
        sendRepport();
      }

      _imageCounter++;
    }
    else {
      //Do not overload the system when this thread is not needed.
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
  }
}

/*
if(SHOW_IMAGES) {
  imshow("marker", image);
  
  waitKey(200);
}

if(_logLevel&0b0010 || (_logLevel&0b0001 && markerIds.size()))
{
  String path = "/home/pi/log_images/initial_"+to_string(_imageCounter)+".jpg";
  imwrite(path, image );
}
if(_logLevel&0b1000 || (_logLevel&0b0100 && markerIds.size()))
{
  aruco::drawDetectedMarkers(image, markerCorners, markerIds);
  String path2 = "/home/pi/log_images/result_"+to_string(_imageCounter)+".jpg";
  imwrite(path2, image );
}
*/
