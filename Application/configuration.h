#pragma once

#include <iostream>
#include <string>

#include <yaml-cpp/yaml.h>

#include <Library/process_result.h>
#include <Hardware/include/i2c.h>
#include <Hardware/include/led.h>

#include "commander.h"
#include "instances.h"

namespace wer {
    namespace cargo {

        class Configuration {
          public:
            Configuration();
            ~Configuration();

            wer::lib::ProcessResult load(int argc, char const *argv[]);
            wer::lib::ProcessResult load(std::string path);

            uint8_t getSystemId();
            uint8_t getComponentId();

            std::shared_ptr<Instances> getInstances();
            std::shared_ptr<Commander> getCommander();

            void recoveryStart();

          private:
            void createHandler();
            void createHardware();
            void createLinks();

            wer::lib::ProcessResult createInstances();
            wer::lib::ProcessResult initAfterAllInstanceCreated();
            wer::lib::ProcessResult checkAllInstanceInitialised();

            YAML::Node _config;
            std::shared_ptr<Instances> _instances;
            std::shared_ptr<Commander> _commander;
            bool _dataLoaded;

            std::shared_ptr<wer::hal::I2c> _i2c;
            std::shared_ptr<wer::hal::Led> _led;
        };
    }
}

/*
#include <boost/property_tree/ptree.hpp>
boost::property_tree::ptree _config;
*/