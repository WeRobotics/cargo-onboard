#include "telemetry_manager.h"

#include <stdio.h>
#include <fcntl.h>

#include <iostream>
#include <limits>

#include <Library/logging/log.h>
//#include <Library/logging/flight_log.h>

#include <Application/Drones/geo_helper.h>

using namespace std::placeholders;

TelemetryManager::TelemetryManager(std::shared_ptr<Drone> drone, std::shared_ptr<CargoMissionManager> missionManager, std::shared_ptr<TempHumSensor> sensor) :
_drone(drone),
_missionManager(missionManager),
_sensor(sensor)
{
    //wer::com::Handler::get()->register_mavlink_message_handler(MAVLINK_MSG_ID_COMMAND_LONG, std::bind(&TelemetryManager::process_command_long, this, _1), this);
}

TelemetryManager::~TelemetryManager() {

}

void TelemetryManager::sendGlobalPosition() {
	if(_drone->connected()) {
		float32_t vx, vy, vz, latitude, longitude, altitude;
		float32_t roll, pitch, yaw;

		_drone->getVelocity(vx, vy, vz);
		_drone->getPosition(latitude, longitude, altitude);
		_drone->getAttitude(roll, pitch, yaw);

		mavlink_attitude_t attitude;

		//attitude.time_boot_ms = ;
		attitude.roll = roll;
		attitude.pitch = pitch;
		attitude.yaw = yaw;

		mavlink_global_position_int_t globalPosition;
		//globalPosition time_boot_ms = ;
		globalPosition.lat = (int32_t) (latitude * 10000000.f);
		globalPosition.lon = (int32_t) (longitude * 10000000.f);
		globalPosition.alt = (int32_t) (altitude * 1000.f);
		//globalPosition.relative_alt = ;
		globalPosition.vx = (int16_t) (vx * 100.f);
		globalPosition.vy = (int16_t) (vy * 100.f);
		globalPosition.vz = (int16_t) (vz * 100.f);
		//globalPosition.hdg = ;

		mavlink_message_t message;
		mavlink_msg_global_position_int_encode(wer::com::Handler::get()->getThisSysId(), wer::com::Handler::get()->getThisCompId(), &message, &globalPosition);

		mavlink_message_t message_2;
		mavlink_msg_attitude_encode(wer::com::Handler::get()->getThisSysId(), wer::com::Handler::get()->getThisCompId(), &message_2, &attitude);

		wer::com::Handler::get()->sendMessage(message);
		wer::com::Handler::get()->sendMessage(message_2);
	}
}

void TelemetryManager::sendSystemStatus() {
	if(_drone->connected()) {
		uint8_t battery, errorCount, error;

		_drone->getStatus(battery, errorCount, error);

		mavlink_sys_status_t systemStatus;

		systemStatus.battery_remaining = battery;
		systemStatus.errors_count1 = errorCount;
		systemStatus.errors_count2 = error;

		mavlink_message_t message;
		mavlink_msg_sys_status_encode(wer::com::Handler::get()->getThisSysId(), wer::com::Handler::get()->getThisCompId(), &message, &systemStatus);

		wer::com::Handler::get()->sendMessage(message);
	}
}

void TelemetryManager::sendDebugVector() {
	if(_drone->connected()) {
		float cpuTemp = std::numeric_limits<float>::quiet_NaN();

		FILE* infile = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
		if(infile) {
			fscanf(infile, "%f",&cpuTemp);
			cpuTemp /= 1000.0f;
			//LogInfo() << "CPU temp : " << cpuTemp << "C";
			fclose(infile); 
		}

		//FlightLog::getInstance()->logCpuTemp(cpuTemp);

		float distance = std::numeric_limits<float>::quiet_NaN();
		uint64_t tof = 0;

		if(_missionManager) {
			_missionManager->getRemainingDistanceAndTime(distance, tof);
		}

		mavlink_debug_vect_t debugVector;
		
		debugVector.time_usec = tof;
		debugVector.x = distance;

		if(_sensor && _sensor->connected()) {
			debugVector.y = _sensor->temperature();
			debugVector.z = _sensor->humidity();
		}
		else {
			debugVector.y = cpuTemp;
			//debugVector.y = std::numeric_limits<float>::quiet_NaN();
			debugVector.z = std::numeric_limits<float>::quiet_NaN();
		}

		mavlink_message_t message;
		mavlink_msg_debug_vect_encode(wer::com::Handler::get()->getThisSysId(), wer::com::Handler::get()->getThisCompId(), &message, &debugVector);

		wer::com::Handler::get()->sendMessage(message);
	}
}

void TelemetryManager::sendStatusText(std::string statusMessage, LogLevel level)
{
	mavlink_message_t message;
    mavlink_statustext_t statustext;

    switch (level) {
        case LogLevel::Debug:
        	statustext.severity = MAV_SEVERITY_ERROR;
            break;
        case LogLevel::Info:
        	statustext.severity = MAV_SEVERITY_WARNING;
            break;
        case LogLevel::Warn:
        	statustext.severity = MAV_SEVERITY_INFO;
            break;
        case LogLevel::Err:
        	statustext.severity = MAV_SEVERITY_DEBUG;
            break;

        default:
        	statustext.severity = MAV_SEVERITY_INFO;
        	break;
    }

    uint8_t length = statusMessage.length();
    length = length > 49 ? 49 : length;
    strncpy(statustext.text, statusMessage.c_str(), length);
    statustext.text[length] = '\0';

    //If no Ground station are connected we could have a buffer with the last five status messages
    //This would enable the operator to see boot message when he get connected

    mavlink_msg_statustext_encode(wer::com::Handler::get()->getThisSysId(), wer::com::Handler::get()->getThisCompId(), &message, &statustext);
    wer::com::Handler::get()->sendMessage(message);
}
