#include "status_led.h"

StatusLed::StatusLed(std::shared_ptr<wer::hal::Led> led, std::shared_ptr<CallEveryHandler> callEveryHandler) :
_led(led),
_callEveryHandler(callEveryHandler),
_active(true),
_blinkProcedureActive(false),
_state(0),
_serverConnected(false),
_radioConnected(false),
_serverConnections(0),
_radioConnections(0),
_stuffChanged(true)
{
	_callEveryHandler->add(std::bind(&StatusLed::cycle, this), 0.1f, &_timerCookie);
}

StatusLed::~StatusLed() {
	_callEveryHandler->remove(_timerCookie);
}

// STATE

void StatusLed::newSystemEvent(uint8_t systemId, uint8_t interfaceId, wer::com::SystemEvent eventType) {
	switch(eventType) {
		case wer::com::SystemEvent::CONNECTION:
		case wer::com::SystemEvent::RECONNECTION:
			if(interfaceId == 0) {
				_radioConnections++;
				newSystemConnectedViaRadio();
			}
			else {
				_serverConnections++;
				newSystemConnectedViaServer();
			}
		break;

		case wer::com::SystemEvent::DISCONNECTION:
			if(interfaceId == 0) {
				_radioConnections--;
			}
			else {
				_serverConnections--;
			}
		break;
	}
	
	if(_radioConnections > 0) {
		_radioConnected = true;
	}
	else {
		_radioConnected = false;
	}

	if(_serverConnections > 0) {
		_serverConnected = true;
	}
	else {
		_serverConnected = false;
	}

	_stuffChanged = true;
}

void StatusLed::stateIdle() {
	_state = 1;
	_stuffChanged = true;
}

void StatusLed::stateMissionReady() {
	_state = 2;
	_stuffChanged = true;
}

void StatusLed::stateRunning() {
	_state = 3;
	_stuffChanged = true;
}

// EVENT

void StatusLed::newSystemConnectedViaServer() {
	startBlinkProcedure(1, 3, 3, WER::HW::Color::ORANGE, WER::HW::Color::BLACK);
}

void StatusLed::newSystemConnectedViaRadio() {
	startBlinkProcedure(1, 3, 3, WER::HW::Color::MARRON, WER::HW::Color::BLACK);
}

void StatusLed::missionRefused() {
	startBlinkProcedure(2, 2, 3, WER::HW::Color::RED, WER::HW::Color::BLACK);
}

void StatusLed::setLedAccordingToState() {

	if(_state == 0) {
		_led->set(WER::HW::Color::BLUE, WER::HW::BlinkType::CONTINOUS);
	}
	else if(_state == 1) {
		if(_serverConnected && _radioConnected) {
			_led->set(WER::HW::Color::WHITE, WER::HW::BlinkType::CONTINOUS);
		}
		else if(_radioConnected) {
			_led->set(WER::HW::Color::MARRON, WER::HW::BlinkType::CONTINOUS);
		}
		else if(_serverConnected) {
			_led->set(WER::HW::Color::ORANGE, WER::HW::BlinkType::CONTINOUS);
		}
		else {
			_led->set(WER::HW::Color::YELLOW, WER::HW::BlinkType::CONTINOUS);
		}
	}
	else if(_state == 2) {
		_led->set(WER::HW::Color::GREEN, WER::HW::BlinkType::CONTINOUS);
	}
	else if(_state == 3) {
		_led->set(WER::HW::Color::RED, WER::HW::BlinkType::CONTINOUS);
	}
	else {
		_led->set(WER::HW::Color::BLACK, WER::HW::BlinkType::CONTINOUS);
	}
}

void StatusLed::turnOff() {
	_active = false;
	_stuffChanged = true;
}

void StatusLed::turnOn() {
	_active = true;
	_stuffChanged = true;
}

void StatusLed::cycle() {
	if(_active && _blinkProcedureActive) {
		_blinkCounter--;

		if(_blinkCounter == 0) {
			_blinkProcedureCounter--;
			if(_blinkProcedureCounter == 0) {
				_blinkProcedureActive = false;
				setLedAccordingToState();
			}
			else {
				if(_blinkProcedureCounter % 2 == 0) {
					// Even number color 1, time 1
					_blinkCounter = _blinkTime1;
					_led->set(_blinkColor1, WER::HW::BlinkType::CONTINOUS);
				}
				else {
					//Odd number color 2, time 2
					_blinkCounter = _blinkTime2;
					_led->set(_blinkColor2, WER::HW::BlinkType::CONTINOUS);
				}
			}
		}
	}
	else if(_stuffChanged) {
		if(_active) {
			setLedAccordingToState();
			_stuffChanged = false;
		}
		else {
			_led->set(WER::HW::Color::BLACK, WER::HW::BlinkType::CONTINOUS);
		}
	}
}

void StatusLed::startBlinkProcedure(uint8_t blinkTime1, uint8_t blinkTime2, uint8_t blinkCount, WER::HW::Color blinkColor1, WER::HW::Color blinkColor2) {
	if(_blinkProcedureActive == false && blinkCount > 0) {
		_blinkProcedureCounter = blinkCount * 2;

		_blinkTime1 = blinkTime1;
		_blinkTime2 = blinkTime2;
		_blinkColor1 = blinkColor1;
		_blinkColor2 = blinkColor2;

		_blinkCounter = blinkTime1;
		_led->set(_blinkColor1, WER::HW::BlinkType::CONTINOUS);

		_blinkProcedureActive = true;
	}
}