#pragma once

#include <thread>
#include <mutex>
#include <atomic>

#include <Library/logging/log_level.h>
#include <Hardware/include/tempHumSensor.h>

#include <Communication/handler.h>
#include <Application/Drones/drone.h>
#include <Application/Drones/mission_manager.h>

class TelemetryManager {
public:
	TelemetryManager(std::shared_ptr<Drone> drone, std::shared_ptr<CargoMissionManager> missionManager, std::shared_ptr<TempHumSensor> sensor);
	~TelemetryManager();

	void sendGlobalPosition();
	void sendSystemStatus();
	void sendDebugVector();
	void sendStatusText(std::string statusMessage, LogLevel level);
private:
	std::shared_ptr<Drone> _drone;
	std::shared_ptr<CargoMissionManager> _missionManager;
	std::shared_ptr<TempHumSensor> _sensor;
};