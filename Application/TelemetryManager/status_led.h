#pragma once

#include <atomic>
#include <cstdint>

#include <Hardware/include/led.h>
#include <Library/timer/call_every_handler.h>
#include <Communication/system.h>


class StatusLed {
public:
	StatusLed(std::shared_ptr<wer::hal::Led> led, std::shared_ptr<CallEveryHandler> callEveryHandler);
	~StatusLed();

	void newSystemEvent(uint8_t systemId, uint8_t interfaceId, wer::com::SystemEvent eventType);

	void newSystemConnectedViaServer();
	void newSystemConnectedViaRadio();

	void missionRefused();

	void stateIdle();
	void stateMissionReady();
	void stateRunning();

	void turnOff();
	void turnOn();

	void cycle();
private:
	void startBlinkProcedure(uint8_t binkTime1, uint8_t blinkTime2, uint8_t blinkCount, WER::HW::Color blinkColor1, WER::HW::Color blinkColor2);
	void setLedAccordingToState();

	std::shared_ptr<wer::hal::Led> _led;
	std::shared_ptr<CallEveryHandler> _callEveryHandler;

	std::atomic<uint8_t> _active;

	std::atomic<uint8_t> _blinkProcedureActive;
	uint8_t _blinkCounter;
	uint8_t _blinkProcedureCounter;

	uint8_t _blinkTime1;
	uint8_t _blinkTime2;
	WER::HW::Color _blinkColor1;
	WER::HW::Color _blinkColor2;

	std::atomic<uint8_t> _state;
	std::atomic<uint8_t> _serverConnected;
	std::atomic<uint8_t> _radioConnected;

	int _serverConnections;
	int _radioConnections;

	std::atomic<uint8_t> _stuffChanged;

	void* _timerCookie;
	
};