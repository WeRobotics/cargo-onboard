#include "application.h"

#include <unistd.h>

#include <chrono>

#include <Library/logging/log.h>
#include <Communication/handler.h>

wer::cargo::Application::Application(std::shared_ptr<wer::cargo::Instances> instances,
									 std::shared_ptr<wer::cargo::Commander> commander, 
						 			 std::atomic<uint8_t>* shouldExit) :
_shouldExit(shouldExit),
_instances(instances),
_commander(commander),
_sensorsThread(nullptr),
_systemThread(nullptr),
_stateThread(nullptr)
{
		//PWR_FAIL
		//PG_4V
		//CHG_STAT
		//CHG_FAIL
	uint8_t code = WER::Hardware::get()->gpio()->registerCallback(WER::HW::IO::PWR_BUT, 
													  			  WER::HW::INTERFACE::GpioEvent::BOTH_EDGE, 
													  			  std::bind(&Application::cargoShieldMonitoring, this, std::placeholders::_1, std::placeholders::_2));
	
	if(code) {
		LogErr() << "Could not configure the power button callback, error : " << unsigned(code);
	}
	else {
		LogInfo() << "Callback power button configured";
	}


	code = WER::Hardware::get()->gpio()->digitalWrite(WER::HW::IO::BCK_ON, 1);
	
	if(code) {
		LogErr() << "Could not activate the power supply backup, error : " << unsigned(code);
	}
	else {
		LogInfo() << "Backup power supply activated.";
	}
}

wer::cargo::Application::~Application() {
	set_log_callback(nullptr);
}

void wer::cargo::Application::run() {
	start();
	
	while(!(*_shouldExit)) {
		//Do regular check on system health
		usleep(10000);
	}

	stop();
}

void wer::cargo::Application::start() {
	//commander.start();
	_sensorsThread = new std::thread(&wer::cargo::Application::sensorsThread, this);
	_systemThread = new std::thread(&wer::cargo::Application::systemThread, this);
	_stateThread = new std::thread(&wer::cargo::Application::stateThread, this);
	_droneThread = new std::thread(&wer::cargo::Application::droneThread, this);
	
	//Start sending the heartbeat
	wer::com::Handler::get()->start();

	if(_instances->cargoSensor) {
		_instances->cargoSensor->enable();
	}

	_instances->droneCallEveryHandler->add(std::bind(&Drone::updateBroadcastData, _instances->drone), 
												  0.05f, &_updateDroneDataCookie);

	_instances->communicationCallEveryHandler->add(std::bind(&TelemetryManager::sendGlobalPosition, _instances->telemetryManager), 
												   1.0f, &_sendGlobalPositionCookie);

	_instances->communicationCallEveryHandler->add(std::bind(&TelemetryManager::sendSystemStatus, _instances->telemetryManager), 
												   5.0f, &_sendSystemStatusCookie);

	_instances->communicationCallEveryHandler->add(std::bind(&TelemetryManager::sendDebugVector, _instances->telemetryManager), 
												   1.0f, &_sendSensorCookie);

	_commander->start();
}

void wer::cargo::Application::stop() {
	//commander.stop();
	_instances->communicationCallEveryHandler->remove(_updateDroneDataCookie);
	_instances->communicationCallEveryHandler->remove(_sendGlobalPositionCookie);
	_instances->communicationCallEveryHandler->remove(_sendSystemStatusCookie);
	_instances->communicationCallEveryHandler->remove(_sendSensorCookie);

	wer::com::Handler::get()->stop();

	if (_stateThread != nullptr) {
		_stateThread->join();
		delete _stateThread;
		_stateThread = nullptr;
		LogInfo() << "State thread stopped.";
	}

	if (_systemThread != nullptr) {
		_systemThread->join();
		delete _systemThread;
		_systemThread = nullptr;
		LogInfo() << "System thread stopped.";
	}

	if (_sensorsThread != nullptr) {
		_sensorsThread->join();
		delete _sensorsThread;
		_sensorsThread = nullptr;
		LogInfo() << "Sensor thread stopped.";
	}

	if (_droneThread != nullptr) {
		_droneThread->join();
		delete _droneThread;
		_droneThread = nullptr;
		LogInfo() << "Drone thread stopped.";
	}

	if(_instances->cargoSensor) {
		_instances->cargoSensor->disable();
	}
}

void wer::cargo::Application::systemThread()
{
    while (!(*_shouldExit)) {
        _instances->communicationTimeoutHandler->run_once();
        _instances->communicationCallEveryHandler->run_once();

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void wer::cargo::Application::droneThread()
{
    while (!(*_shouldExit)) {
        _instances->droneTimeoutHandler->run_once();
        _instances->droneCallEveryHandler->run_once();

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void wer::cargo::Application::sensorsThread() {
    while (!(*_shouldExit)) {
        _instances->sensorsTimeoutHandler->run_once();
        _instances->sensorsCallEveryHandler->run_once();

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void wer::cargo::Application::stateThread() {
    while (!(*_shouldExit)) {
        _instances->stateMachineTimeoutHandler->run_once();
        _instances->stateMachineCallEveryHandler->run_once();

        _commander->cycle();

        //std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void wer::cargo::Application::cargoShieldMonitoring(WER::HW::IO pin, WER::HW::INTERFACE::GpioEvent event) {
	if(static_cast<int>(pin) == 22) {
		std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();

		static std::chrono::time_point<std::chrono::steady_clock> pressed_time = now;
		if(event == WER::HW::INTERFACE::GpioEvent::FALLING_EDGE) {
			pressed_time = now;

			_instances->sensorsTimeoutHandler->add(std::bind(&Application::buttonInterval, this, true), 3.0f, &_button_int_cookie);
		}
		else {
			auto dt = (std::chrono::duration_cast<std::chrono::milliseconds>(now - pressed_time)).count();
			if(dt < 200) {
				LogInfo() << "Spurious button pressed : " << static_cast<int>(dt) << "ms.";
				_instances->sensorsTimeoutHandler->remove(_button_int_cookie);
			}
			else {
				LogInfo() << "Button pressed for " << static_cast<int>(dt) << "ms.";
			}

			if(3000 < dt && dt < 5000) {
				shutdownButtonPressed();
			}
		}
	}

	//LogInfo() << "Shield event : pin=" << static_cast<int>(pin) << ", event=" << static_cast<int>(event);
}

void wer::cargo::Application::buttonInterval(uint8_t interval) {
	if(interval) {
		_instances->statusLed->turnOff();
		_instances->sensorsTimeoutHandler->add(std::bind(&Application::buttonInterval, this, false), 2.0f, &_button_int_cookie);
	}
	else {
		_instances->statusLed->turnOn();
	}
}

void wer::cargo::Application::shutdownButtonPressed() {
	if(_instances->drone->landed()) {
		LogWarn() << "Onboard will proceed to poweroff.";
		*_shouldExit = 2;
	}
}
/*
	if(_stateData.error == true) {
		_stateData.bootFinished = true;
		UserInterface::getInstance()->setErrorLed(LedBrighness::MAX);
	}
*/

/*
UserInterface::getInstance()->setStateLed(LedColor::BLUE);
UserInterface::getInstance()->setButtonLed(LedColor::CYAN);
UserInterface::getInstance()->setErrorLed(LedBrighness::MIN);
UserInterface::getInstance()->setDroneConnectionLed(LedBrighness::MIN);

UserInterface::getInstance()->setStateLed(LedColor::BLACK);
UserInterface::getInstance()->setButtonLed(LedColor::BLACK);
UserInterface::getInstance()->setErrorLed(LedBrighness::MIN);
UserInterface::getInstance()->setDroneConnectionLed(LedBrighness::MIN);
*/