#pragma once

#include <Library/timer/timeout_handler.h>
#include <Library/timer/call_every_handler.h>

#include <Hardware/include/tempHumSensor.h>
#include <Hardware/include/distance_sensor.h>

#include <Application/LandingTarget/landing_target.h>
#include <Application/TelemetryManager/telemetry_manager.h>
#include <Application/TelemetryManager/status_led.h>
#include <Application/Drones/drone.h>
#include <Application/Drones/control_mode.h>
#include <Application/Drones/mission_manager.h>
#include <Application/state.h>

namespace wer {
	namespace cargo {

		struct Instances {
			std::shared_ptr<Drone> drone;
			std::shared_ptr<ControlMode> manualMode;
			std::shared_ptr<ControlMode> landingMode;
			std::shared_ptr<CargoMissionManager> missionManager;

			std::shared_ptr<StatusLed> statusLed;
			std::shared_ptr<TelemetryManager> telemetryManager;

			GlobalStateData stateData;

			//Need to be here ?
			std::shared_ptr<wer::hal::DistanceSensor> groundSensor;
			std::shared_ptr<LandingTarget> landingTarget;
			std::shared_ptr<TempHumSensor> cargoSensor;

			std::shared_ptr<TimeoutHandler> communicationTimeoutHandler;
            std::shared_ptr<CallEveryHandler> communicationCallEveryHandler;

			std::shared_ptr<TimeoutHandler> stateMachineTimeoutHandler;
            std::shared_ptr<CallEveryHandler> stateMachineCallEveryHandler;

            std::shared_ptr<TimeoutHandler> sensorsTimeoutHandler;
            std::shared_ptr<CallEveryHandler> sensorsCallEveryHandler;

			std::shared_ptr<TimeoutHandler> droneTimeoutHandler;
            std::shared_ptr<CallEveryHandler> droneCallEveryHandler;

            wer::lib::Time time;
		};
	}
}