#include "state_recovery.h"

#include <limits>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <Library/logging/log.h>

StateRecovery* StateRecovery::instance = nullptr;

StateRecovery::StateRecovery() {
  _datas.missionId = 0;
  _datas.missionIndex = 0;
  _datas.terrainAltitude = std::numeric_limits<float>::quiet_NaN();
  //TODO check the drone mode instead of the file
  _datas.missionIsDji = false;
  _datas.missionIsFinished = false;
  _datas.takeoffPosition.altitude = std::numeric_limits<float>::quiet_NaN();
  _datas.takeoffPosition.latitude = std::numeric_limits<float>::quiet_NaN();
  _datas.takeoffPosition.longitude = std::numeric_limits<float>::quiet_NaN();
  _datas.altOffset = 0.0f;
  _datas.commanderState = MANUAL;

  _appInitDone = false;

  LogInfo() << "Init the state recovery";
}

void StateRecovery::appInitDone() {
  _appInitDone = true;
}

StateRecovery::~StateRecovery() {
  system("rm /home/pi/inFlightRecovery.dat");
}

void StateRecovery::setMissionId(uint8_t missionId) {
  _datas.missionId = missionId;
  save();
}

void StateRecovery::setMissionIndex(uint8_t missionIndex) {
  _datas.missionIndex = missionIndex;
  save();
}

void StateRecovery::setTerrainAltitude(float terrainAltitude) {
  _datas.terrainAltitude = terrainAltitude;
  save();
}

void StateRecovery::setMissionIsDji(bool missionIsDji) {
  _datas.missionIsDji = missionIsDji;
  save();
}

void StateRecovery::setMissionIsFinished(bool missionIsFinished) {
  _datas.missionIsFinished = missionIsFinished;
  save();
}

void StateRecovery::setTakeoffPosition(GpsPosition takeoffPosition) {
  _datas.takeoffPosition = takeoffPosition;
  save();
}

void StateRecovery::setAltOffset(float altOffset) {
  _datas.altOffset = altOffset;
  save();
}

void StateRecovery::setCommanderState(OnboardState commanderState) {
  _datas.commanderState = commanderState;
  save();
}

void StateRecovery::save() {
  if(_appInitDone) {
    namespace pt = boost::property_tree;
    pt::ptree root;
    pt::ptree mission;
    pt::ptree drone;
    pt::ptree takeoff;
    pt::ptree commander;

    mission.put("id", _datas.missionId);
    mission.put("index", _datas.missionIndex);
    mission.put("terrainAltitude", _datas.terrainAltitude);
    mission.put("isDji", _datas.missionIsDji);
    mission.put("isFinished", _datas.missionIsFinished);

    takeoff.put("altitude", _datas.takeoffPosition.altitude);
    takeoff.put("latitude", _datas.takeoffPosition.latitude);
    takeoff.put("longitude", _datas.takeoffPosition.longitude);
    
    drone.put("altOffset", _datas.altOffset);
    drone.add_child("takeoff", takeoff);

    commander.put("state", static_cast<uint8_t>(_datas.commanderState));

    root.add_child("mission", mission);
    root.add_child("drone", drone);
    root.add_child("commander", commander);

    try{
      pt::write_json("/home/pi/inFlightRecovery.dat", root);
      LogInfo() << "Writting recovery datas successful.";
    }
    catch(std::exception &e){
      LogErr() << "Error while writing recovery data on json file";
      LogErr() << e.what();
    }
  }
}
