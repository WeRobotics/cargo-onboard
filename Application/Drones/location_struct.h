#pragma once

enum WaypointType {WP_TAKEOFF, WP_WAYPOINT, WP_LANDING, WP_PRECISION_LANDING, WP_NOT_DEFINED};

struct GpsPosition {
	float latitude;
	float longitude;
	float altitude;
};

struct Waypoint {
	GpsPosition location;	// Location of the target landing point
	float groundAltitude;	// Ground level altitude at waypoint
	float speed;			// In meter per second to go to the waypoint
	float radius;			// Bending radius to next waypoint
	WaypointType type;
};