#pragma once

#include "location_struct.h"

//Math related funcitons
float toRadians(float angleDeg);
float toDegree(float anglerad);
float limit(float data, float limit);
float calculateDistance(GpsPosition pos1, GpsPosition pos2);