#pragma once

#include <iostream>
#include <mutex>
#include <vector>
#include <atomic>
#include <memory>

#include <dji_vehicle.hpp>

#include <Library/eigen/Eigen/Geometry>
#include <Application/state.h>

#include "drone.h"
#include "dji_status.hpp"
#include "location_struct.h"
#include "dji_loader.h"

using namespace DJI::OSDK;
using namespace DJI::OSDK::Telemetry;

enum EmergencyState{NOT_ACTIVE, WARNING, CRITICAL, EMERGENCY};
enum DroneMode {MODE_MANUAL_CTRL 		= 0, 	MODE_ATTITUDE   = 1, 
				MODE_P_GPS      		= 6, 	MODE_HOTPOINT_MODE = 9, 
				MODE_ASSISTED_TAKEOFF 	= 10, 	MODE_AUTO_TAKEOFF = 11, 
				MODE_NAVI_GO_HOME 		= 15, 	MODE_NAVI_SDK_CTRL  = 17, 
				MODE_FORCE_AUTO_LANDING = 33, 	MODE_SEARCH_MODE = 40, 
				MODE_ENGINE_START = 41};

class DjiHandler : public Drone {
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	DjiHandler(std::shared_ptr<DjiLoader> loader, std::shared_ptr<struct DroneStateData> stateData);
	~DjiHandler();

	bool testSerialConnection();
	bool initVehicle();
	bool configureVehicle();
	bool releaseControl();
	bool obtainControl();
	void setCargoFreqDefaults(uint8_t* freq);

	void computeGpsOffset();
	void activateOffset();
	void disactivateOffset();

	void recoverStart(GpsPosition takeoffPosition, float offset);

	bool isM100();
	void sendVirtualRC();
	bool connected();
	bool landed();

	void initParachute();
	void triggerParachute();

	void makeAltitudeOffsetDroneToMap(float groundAltitude);
	void setTerrainAltitudeFromMap(float altitude);
	void setTerrainAltitudeFromDrone(float altitude);

	// Control functions
	void setManualSp(float32_t velX, float32_t velY, float32_t velZ, float32_t yawRate);
	void setManualSpAbs(float32_t velX, float32_t velY, float32_t velZ, float32_t yawRate);

	// Drone command functions
	uint8_t takeoff();
	uint8_t land();
	uint8_t disarm();
	uint8_t arm();
	uint8_t goHome();
	void emergencyBrake();
	uint8_t killSwitch();

	//Telemetry related functions
	void updateBroadcastData();
	bool dataValid();
	void printTelemetry();
	void getAcceleration(float32_t& ax, float32_t& ay, float32_t& az);
	void getVelocity(float32_t& vx, float32_t& vy, float32_t& vz);
	void getPosition(float32_t& latitude, float32_t& longitude, float32_t& altitude);
	void getAttitude(float32_t& roll, float32_t& pitch, float32_t& yaw);
	void getQuaternion(Eigen::Quaternion<float, Eigen::DontAlign> &q);
	void getStatus(uint8_t &battery, uint8_t &errorCount, uint8_t &errorStatus);
	void getMode(uint8_t &flightStatus, uint8_t &controlMode);
	void getTimeOfFlight(uint64_t &tof);
	void getEstimatedGroundDistance(float &groundDistance);

	void setCbModeChanged(std::function<void(uint8_t, uint8_t)> callback);

	// Waypoint callback functions
	static void waypointReachedInterface(Vehicle* vehicle, RecvContainer recvFrame, UserData userData);
	void setCbWaypointReached(std::function<void(uint8_t)> callback);
	void waypointReached(uint8_t index);

	// Mission command related functions
	uint8_t startMission();
	uint8_t pauseMission();
	uint8_t resumeMission();
	uint8_t stopMission();

	uint8_t uploadWaypoints(std::vector<WayPointSettings>& wp_list, WayPointInitSettings& missionSettings);

	//Security related functions
	bool checkSecurity();

private:
	std::shared_ptr<DjiLoader> _loader;

	std::mutex _vehicle_mutex;
	Vehicle* _vehicle = nullptr;

	std::shared_ptr<struct DroneStateData> _stateData;
	std::shared_ptr<std::atomic<OnboardState>> _actualState;

	std::mutex _telemetry_mutex;
	struct VehicleTelemetry {
		Telemetry::Status         	status;
		Telemetry::Status         	lastStatus;
		Telemetry::GlobalPosition 	globalPosition;
		Telemetry::GlobalPosition 	lastGlobalPosition;
		Telemetry::Vector3f			acceleration;
		Telemetry::Vector3f       	velocity;
		Telemetry::Vector3f       	lastVelocity;
		Telemetry::TimeStamp		timestamp;
		Telemetry::RC				rc;
		Telemetry::Battery			battery;
		Telemetry::GPSInfo			rawGpsPosition;

		float 						correctedAltitude;
		
		Eigen::Quaternion<float, Eigen::DontAlign>			quaternion;
		//this declaration is OK
		Eigen::Matrix<float, 3, 1, Eigen::DontAlign> 	euler;
	} _vehicleTelemetry;

	std::chrono::time_point<std::chrono::steady_clock> _lastTelemChange;

	VirtualRCData _virtualRCdata;

	std::function<void(uint8_t, uint8_t)> _cbModeChanged;
	std::function<void(uint8_t)> _wpReachedCb;

	std::atomic<EmergencyState> _emergencyState;

	std::atomic<uint8_t> _errorCount;

	GpsPosition _takeoffPosition;
	std::chrono::time_point<std::chrono::steady_clock> _takeoffTime;

	float _altitudeOffset;
	float _terrainAltitude;

	std::chrono::time_point<std::chrono::steady_clock> _latOffsetComputeTime;
	float _latOffset;
	float _lonOffset;
	bool _posOffsetValid;

	bool _offsetActivated;

	bool printIfError(ACK::ErrorCode error);
};