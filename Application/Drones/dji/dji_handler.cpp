#include "dji_handler.h"

#include <iomanip>

#include <Library/logging/log.h>
//#include <Library/logging/flight_log.h>
#include <Library/geo.h>

#include <Application/state_recovery.h>

#include "dji_flight_control.h"


#define DJI_TIMEOUT 5

DjiHandler::DjiHandler(std::shared_ptr<DjiLoader> loader, std::shared_ptr<struct DroneStateData> stateData) :
_loader(loader),
_stateData(stateData),
_emergencyState(NOT_ACTIVE),
_errorCount(0),
_altitudeOffset(0.0f),
_terrainAltitude(0.0f),
_latOffset(0.0f),
_lonOffset(0.0f),
_posOffsetValid(false),
_offsetActivated(false)
{
	_lastTelemChange = std::chrono::steady_clock::now();
	_latOffsetComputeTime = std::chrono::steady_clock::now();
}

DjiHandler::~DjiHandler() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);
	
	_vehicle = nullptr;
}

bool DjiHandler::testSerialConnection() {
	//return true if everythings id fine
	return _loader->testSerialConnection();
}

bool DjiHandler::initVehicle() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle == nullptr && _loader->validateSerialPort() && _loader->initVehicle()) {
		_vehicle = _loader->getVehicle();

		return false;
	}

	return true;
}

bool DjiHandler::configureVehicle() {
	bool error = false;

	if(_vehicle->isM100()) {

		_vehicle->virtualRC->resetVRCData();
		_virtualRCdata = _vehicle->virtualRC->getVRCData();
		_virtualRCdata.mode = 496;

		if(_vehicle->virtualRC->isVirtualRC()) {
			LogInfo() << "In virtualRC.";
		}
		else {
			LogInfo() << "Not in virtualRC.";
		}
		usleep(10000);
		if(_vehicle->virtualRC->isVirtualRC()) {
			LogInfo() << "In virtualRC.";
		}
		else {
			LogInfo() << "Not in virtualRC.";
		}

		_vehicle->virtualRC->setControl(true, VirtualRC::CutOff_ToRealRC);
		_vehicle->virtualRC->sendData(_virtualRCdata);

		usleep(100);
	}

	uint8_t frequencyBuffer[16];
	setCargoFreqDefaults(frequencyBuffer);

	error |= printIfError(_vehicle->obtainCtrlAuthority(DJI_TIMEOUT));
	error |= printIfError(_vehicle->broadcast->setBroadcastFreq(frequencyBuffer, DJI_TIMEOUT));

	initParachute();

	return error;
}

void DjiHandler::setCargoFreqDefaults(uint8_t* freq)
{
  /* Channels definition for A3/N3/M600
   * 0 - Timestamp
   * 1 - Attitude Quaterniouns
   * 2 - Acceleration
   * 3 - Velocity (Ground Frame)
   * 4 - Angular Velocity (Body Frame)
   * 5 - Position
   * 6 - GPS Detailed Information
   * 7 - RTK Detailed Information
   * 8 - Magnetometer
   * 9 - RC Channels Data
   * 10 - Gimbal Data
   * 11 - Flight Statusack
   * 12 - Battery Level
   * 13 - Control Information
   */
  freq[0]  = DataBroadcast::FREQ_50HZ;
  freq[1]  = DataBroadcast::FREQ_50HZ;
  freq[2]  = DataBroadcast::FREQ_50HZ;
  freq[3]  = DataBroadcast::FREQ_50HZ;
  freq[4]  = DataBroadcast::FREQ_50HZ;
  freq[5]  = DataBroadcast::FREQ_50HZ;
  freq[6]  = DataBroadcast::FREQ_10HZ; // Don't send GPS details
  freq[7]  = DataBroadcast::FREQ_0HZ; // Don't send RTK
  freq[8]  = DataBroadcast::FREQ_0HZ; // Don't send Mag
  freq[9]  = DataBroadcast::FREQ_50HZ;
  freq[10] = DataBroadcast::FREQ_50HZ;
  freq[11] = DataBroadcast::FREQ_10HZ;
  freq[12] = DataBroadcast::FREQ_1HZ;
  freq[13] = DataBroadcast::FREQ_1HZ;
}

void DjiHandler::recoverStart(GpsPosition takeoffPosition, float offset) {
	_takeoffPosition = takeoffPosition;
	StateRecovery::getInstance()->setTakeoffPosition(_takeoffPosition);
	_altitudeOffset = offset;
	StateRecovery::getInstance()->setAltOffset(_altitudeOffset);

	setTerrainAltitudeFromDrone(takeoffPosition.altitude);
}

bool DjiHandler::isM100() {
	if(_vehicle) {
		return _vehicle->isM100();
	}
	else {
		return false;
	}
}

void DjiHandler::sendVirtualRC() {
	if(_vehicle && _vehicle->isM100()) {
		_vehicle->virtualRC->sendData(_virtualRCdata);
		//isVirtualRC(); //True || False
	}
}

void DjiHandler::initParachute() {
  if(_vehicle != nullptr){
    uint32_t initOnTimeUs = 2000; // us
    uint16_t pwmFreq      = 50;    // Hz

    LogInfo() << "Parachute configred.";

   // _vehicle->mfio->config(MFIO::MODE_PWM_OUT, MFIO::CHANNEL_7, initOnTimeUs, pwmFreq);
  }
}

void DjiHandler::triggerParachute() {
  if(_vehicle != nullptr){
    LogWarn() << "Parachute deployed !";

    uint32_t initOnTimeUs = 1000; // us
    uint16_t pwmFreq = 50;    // Hz

    _vehicle->mfio->config(MFIO::MODE_PWM_OUT, MFIO::CHANNEL_7, initOnTimeUs, pwmFreq);
  }
}

void DjiHandler::makeAltitudeOffsetDroneToMap(float groundAltitude) {
	if(_stateData->landed) {
		std::lock_guard<std::mutex> lock_t(_telemetry_mutex);
		_altitudeOffset = groundAltitude - _vehicleTelemetry.globalPosition.altitude;
		StateRecovery::getInstance()->setAltOffset(_altitudeOffset);
		LogInfo() << "New offset is " << _altitudeOffset << " drone alt is " << _vehicleTelemetry.globalPosition.altitude << " ground alt is " << groundAltitude;
	}
	else {
		LogWarn() << "Drone is not landed can not compute altitude offset.";
	}
}

void DjiHandler::setTerrainAltitudeFromMap(float altitude) {
	if(std::isfinite(_altitudeOffset)) {
		_terrainAltitude = altitude - _altitudeOffset;
		LogInfo() << "New terrain level in drone ref " << _terrainAltitude << ". Calculated with offset " << _altitudeOffset << "and terrain altitude " << altitude;
	}
	else {
		LogWarn() << "Could not set the terrain altitude from the map as the offset is unknown.";
	}
}

void DjiHandler::setTerrainAltitudeFromDrone(float altitude) {
		_terrainAltitude = altitude;
}

void DjiHandler::getEstimatedGroundDistance(float &groundDistance) {
	groundDistance = _vehicleTelemetry.globalPosition.altitude - _terrainAltitude;
}

void DjiHandler::setManualSp(float32_t velX, float32_t velY, float32_t velZ, float32_t yawRate) {
	uint8_t ctrl_flag = (Control::VERTICAL_VELOCITY | Control::HORIZONTAL_VELOCITY | 
						 Control::YAW_RATE | Control::HORIZONTAL_BODY | Control::STABLE_ENABLE);
	Control::CtrlData data(ctrl_flag, velX, velY, velZ, yawRate);

	if(_vehicle) {
		std::lock_guard<std::mutex> lock(_vehicle_mutex);

		_vehicle->control->flightCtrl(data);

		//double setpoint[4] = {velX, velY, velZ, yawRate};
		//FlightLog::getInstance()->logSetpoint(setpoint);
		//_vehicle->control->velocityAndYawRateCtrl(velX, velY, velZ, yawRate);
	}
	else {
		LogErr() << "Drone not connected, unable to send manual setpoint.";
	}
}

void DjiHandler::setManualSpAbs(float32_t velX, float32_t velY, float32_t velZ, float32_t yawRate) {
	uint8_t ctrl_flag = (Control::VERTICAL_VELOCITY | Control::HORIZONTAL_VELOCITY | 
						 Control::YAW_RATE | Control::HORIZONTAL_GROUND | Control::STABLE_ENABLE);
	Control::CtrlData data(ctrl_flag, velX, velY, velZ, yawRate);

	if(_vehicle) {
		if(dataValid()) {
			std::lock_guard<std::mutex> lock(_vehicle_mutex);

			_vehicle->control->flightCtrl(data);

			//double setpoint[4] = {velX, velY, velZ, yawRate};
			//FlightLog::getInstance()->logSetpoint(setpoint);
		}
		//else log something ? not each time having  flag ?
	}
	else {
		LogErr() << "Drone not connected, unable to send manual setpoint.";
	}
}

bool DjiHandler::releaseControl() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle) {
		ACK::ErrorCode ack = _vehicle->releaseCtrlAuthority(DJI_TIMEOUT);
		return printIfError(ack);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
		return true;
	}
}

bool DjiHandler::obtainControl() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle) {
		ACK::ErrorCode ack = _vehicle->obtainCtrlAuthority(DJI_TIMEOUT);
		return printIfError(ack);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
		return true;
	}
}

uint8_t DjiHandler::takeoff() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle) {
		LogInfo() << "Setting home location at current position.";
		ErrorCode::ErrorCodeType ack = _vehicle->flightController->setHomeLocationUsingCurrentAircraftLocationSync(DJI_TIMEOUT);
		if(ack  != ErrorCode::SysCommonErr::Success) {
			LogErr() << "Could not set home location.";
			return true;
		}
		LogInfo() << "Sending takeoff control command.";
		ACK::ErrorCode ack2 = _vehicle->control->takeoff(DJI_TIMEOUT);
		return printIfError(ack2);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
		return true;
	}
}

uint8_t DjiHandler::land() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle) {
		LogInfo() << "Sending land control command.";
		ACK::ErrorCode ack = _vehicle->control->land(DJI_TIMEOUT);
		return printIfError(ack);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
		return true;
	}
}

uint8_t DjiHandler::arm() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle) {
		LogInfo() << "Sending arming control command.";
		ACK::ErrorCode ack = _vehicle->control->armMotors(DJI_TIMEOUT);
		return printIfError(ack);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
		return true;
	}
}

uint8_t DjiHandler::disarm() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle) {
		LogInfo() << "Sending disarming control command.";
		ACK::ErrorCode ack = _vehicle->control->disArmMotors(DJI_TIMEOUT);
		return printIfError(ack);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
		return true;
	}
}

uint8_t DjiHandler::goHome() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	if(_vehicle) {
		LogInfo() << "Sending go home control command.";
		ACK::ErrorCode ack = _vehicle->control->goHome(DJI_TIMEOUT);
		return printIfError(ack);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
		return true;
	}
}

void DjiHandler::emergencyBrake() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);
	if(_vehicle) {
		LogInfo() << "Sending emergency brake control command.";
		_vehicle->control->emergencyBrake();
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
	}
}

uint8_t DjiHandler::killSwitch() {
	//TODO : Only in parachute, do we want to check this ?
	std::lock_guard<std::mutex> lock(_vehicle_mutex);
	if(_vehicle) {
		ACK::ErrorCode ack = _vehicle->control->killSwitch(Control::KillSwitch::ENABLE, DJI_TIMEOUT, "Emergency");
		LogInfo() << "Sending killSwitch control command.";
		return printIfError(ack);
	}
	else {
		LogErr() << "Drone not connected, unable to send command.";
	}
	return true;
}

bool DjiHandler::printIfError(ACK::ErrorCode error) {
	if (ACK::getError(error) != ACK::SUCCESS)
	{
		ACK::getErrorCodeMessage(error, __func__);
		return true;
	}
	return false;
}

void DjiHandler::computeGpsOffset() {
	//TODO save offset at every waypoint

	if(!_stateData->landed && _offsetActivated) {
		//Time constant should be arround 20s so filtering freq is 0.008Hz
		auto now = std::chrono::steady_clock::now();
		float dt = std::chrono::duration_cast<std::chrono::milliseconds>(now - _latOffsetComputeTime).count() / 1000.0f;
		_latOffsetComputeTime = now;

		float rawLat = _vehicleTelemetry.rawGpsPosition.latitude / 1.0e7f / 180.0f * M_PI;
		float rawLon = _vehicleTelemetry.rawGpsPosition.longitude / 1.0e7f / 180.0f * M_PI;
		
		float validDelta = 0.0001f, maxOffset = 0.001f;
		float latError = rawLat - _vehicleTelemetry.globalPosition.latitude;
		float lonError = rawLon - _vehicleTelemetry.globalPosition.longitude;

		if(fabsf(latError - _latOffset) < validDelta && fabsf(lonError - _lonOffset) < validDelta) {
			float fc = 0.05f;	//Frequency cutoff
			float alpha = 2.0f * M_PI * fc * dt / (2.0f * M_PI * fc * dt + 1.0f);

			_latOffset = alpha * latError + (1.0f - alpha) * _latOffset;
			_lonOffset =  alpha * lonError + (1.0f - alpha) * _lonOffset;
		}

		_posOffsetValid = fabsf(_latOffset) < maxOffset && fabsf(_latOffset) < maxOffset;
	}
	else {
		_latOffsetComputeTime = std::chrono::steady_clock::now();
		_latOffset = 0.0f;
		_lonOffset = 0.0f;
	}
}

void DjiHandler::activateOffset() {
	_offsetActivated = true;
}

void DjiHandler::disactivateOffset() {
	_offsetActivated = false;
}

void DjiHandler::updateBroadcastData()
{
	std::lock_guard<std::mutex> lock_t(_telemetry_mutex);

	if(_vehicle) {

		//printTelemetry();

		_vehicleTelemetry.status		 = _vehicle->broadcast->getStatus();

		_vehicleTelemetry.globalPosition = _vehicle->broadcast->getGlobalPosition();
		_vehicleTelemetry.correctedAltitude = _vehicleTelemetry.globalPosition.altitude + _altitudeOffset;

		_vehicleTelemetry.rawGpsPosition = _vehicle->broadcast->getGPSInfo();

		computeGpsOffset();

		if(!_stateData->landed && _posOffsetValid && _offsetActivated) {
			//Use the offset if valid
			_vehicleTelemetry.globalPosition.latitude += _latOffset;
			_vehicleTelemetry.globalPosition.longitude += _lonOffset;
		}

		_vehicleTelemetry.acceleration	 = _vehicle->broadcast->getAcceleration(); 
		_vehicleTelemetry.velocity		 = _vehicle->broadcast->getVelocity();
		_vehicleTelemetry.battery		 = _vehicle->broadcast->getBatteryInfo();
		_vehicleTelemetry.rc 			 = _vehicle->broadcast->getRC();
		//_vehicleTelemetry.rcWithFlagData = _vehicle->broadcast->getRCWithFlagData();
		_vehicleTelemetry.timestamp 	 = _vehicle->broadcast->getTimeStamp();

		Telemetry::Quaternion q_dji 	 = _vehicle->broadcast->getQuaternion();
		_vehicleTelemetry.quaternion 	 = Eigen::Quaternionf(q_dji.q0, q_dji.q1, q_dji.q2, q_dji.q3);
		_vehicleTelemetry.euler 		 = _vehicleTelemetry.quaternion.toRotationMatrix().eulerAngles(0, 1, 2);

		float globalVelocity = sqrtf(pow(_vehicleTelemetry.velocity.x, 2) + pow(_vehicleTelemetry.velocity.y, 2) + pow(_vehicleTelemetry.velocity.z, 2));
		if(	_vehicleTelemetry.globalPosition.latitude == _vehicleTelemetry.lastGlobalPosition.latitude &&
			_vehicleTelemetry.globalPosition.longitude == _vehicleTelemetry.lastGlobalPosition.longitude &&
			_vehicleTelemetry.globalPosition.altitude == _vehicleTelemetry.lastGlobalPosition.altitude &&
			_vehicleTelemetry.velocity.x == _vehicleTelemetry.lastVelocity.x &&
			_vehicleTelemetry.velocity.y == _vehicleTelemetry.lastVelocity.y &&
			_vehicleTelemetry.velocity.z == _vehicleTelemetry.lastVelocity.z &&
			connected() && !_stateData->landed && globalVelocity > 2.0f) {
			//We might have a situation :P let's test it during a timeout
			if(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _lastTelemChange).count() > 10000) {
				//Houston we have a problem...
				LogErr() << "Detecting communication issue for 10 seconds. Houston we have a problem...";
				system("ssh pi@localhost \"sudo reboot\"");
			}
		}
		else {
			_lastTelemChange = std::chrono::steady_clock::now();
			_vehicleTelemetry.lastGlobalPosition = _vehicleTelemetry.globalPosition;
			_vehicleTelemetry.lastVelocity = _vehicleTelemetry.velocity;
		}

		//Check and print any change in systems status.
		if(_vehicleTelemetry.status.flight != _vehicleTelemetry.lastStatus.flight || 
		   _vehicleTelemetry.status.mode != _vehicleTelemetry.lastStatus.mode ||
		   _vehicleTelemetry.status.gear != _vehicleTelemetry.lastStatus.gear ||
		   _vehicleTelemetry.status.error != _vehicleTelemetry.lastStatus.error) {
		   	_stateData->flightMode = _vehicleTelemetry.status.mode;
			LogInfo() << "[DJI Handler] Vehicle status changed "
					  << " Flight = " << unsigned(_vehicleTelemetry.status.flight)
					  << "\tMode = " << unsigned(_vehicleTelemetry.status.mode)
					  << "\tGear = " << unsigned(_vehicleTelemetry.status.gear)
					  << "\tError = " << unsigned(_vehicleTelemetry.status.error);

			_vehicleTelemetry.lastStatus = _vehicleTelemetry.status;
		}

		//_vehicleTelemetry.rcWithFlagData.groundConnected && 
		if(_vehicleTelemetry.rc.mode < -4000)
			_stateData->rcOverride = true;
		else
			_stateData->rcOverride = false;


		bool landed;
		if(_vehicle->isM100()){
			landed = _vehicleTelemetry.status.flight == VehicleStatus::M100FlightStatus::ON_GROUND_STANDBY;
		}
//		else if(_vehicle->isLegacyM600()) {
//			LogInfo() << "Legacy M600";
//		}
		else {
			landed = (_vehicleTelemetry.status.flight == VehicleStatus::FlightStatus::ON_GROUND || _vehicleTelemetry.status.flight == VehicleStatus::FlightStatus::STOPED);
		}

		//std::cout << "landed: " << (unsigned)_vehicleTelemetry.status.flight << std::endl;

		if(_stateData->landed != landed) {
			_stateData->landed = landed;
			if(landed == false) {
				//LogInfo() << "Takeoff detected.";
				LogInfo() << "[DJI drone] Takeoff detected.";
				//We just takeoff, let's remember this position
				_takeoffPosition.latitude = _vehicleTelemetry.globalPosition.latitude;
				_takeoffPosition.longitude = _vehicleTelemetry.globalPosition.longitude;
				_takeoffPosition.altitude = _vehicleTelemetry.correctedAltitude;
				_takeoffTime = std::chrono::steady_clock::now();

				StateRecovery::getInstance()->setTakeoffPosition(_takeoffPosition);

				setTerrainAltitudeFromDrone(_vehicleTelemetry.globalPosition.altitude);

				//double position[3] = {_takeoffPosition.latitude, _takeoffPosition.longitude, _takeoffPosition.altitude};
				//FlightLog::getInstance()->logTakeoffPosition(position);
			}
			else {
				LogInfo() << "[DJI drone] Landed detected.";
			}
		}
/*
		static int i = 0;
		if(i == 10) {
			i = 0;

			double attitude[3] = {_vehicleTelemetry.euler(0, 0), _vehicleTelemetry.euler(1, 0),_vehicleTelemetry.euler(2, 0)};
			double position[3] = {_vehicleTelemetry.globalPosition.latitude, _vehicleTelemetry.globalPosition.longitude, _vehicleTelemetry.globalPosition.altitude};
			double velocity[3] = {_vehicleTelemetry.velocity.x, _vehicleTelemetry.velocity.y, _vehicleTelemetry.velocity.z};
			double gpsOffset[2] = {_latOffset, _lonOffset};

			std::string date = std::to_string(_vehicleTelemetry.rawGpsPosition.time.date);
			date.insert(4, "-");
			date.insert(7, "-");
			std::string time = std::to_string(_vehicleTelemetry.rawGpsPosition.time.time);
			if(time.size() < 5) time = "0" + time;
			time.insert(2,":");
			time.insert(5, ":");
			std::string timestamp = date + "T" + time + "Z";

			//FlightLog::getInstance()->logTelemetry(position, velocity, gpsOffset, attitude, timestamp);
		}
		else {
			i++;
		}*/
	}
}

bool DjiHandler::dataValid() {
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _lastTelemChange).count() < 1000;
}

void DjiHandler::printTelemetry() {
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

    std::cout << "-------" << std::endl;
    std::cout << "Flight Status            = " << (unsigned)_vehicleTelemetry.status.flight << std::endl;
    std::cout << "Position (LLA)           = " 
    	<< _vehicleTelemetry.globalPosition.latitude	<< ", " 
    	<< _vehicleTelemetry.globalPosition.longitude	<< ", " 
    	<< _vehicleTelemetry.correctedAltitude	<< std::endl;
    std::cout << "Velocity (vx,vy,vz)      = " 
    	<< _vehicleTelemetry.velocity.x << ", " 
    	<< _vehicleTelemetry.velocity.y << ", " 
    	<< _vehicleTelemetry.velocity.z << std::endl;
    std::cout << "Attitude (Roll,Pitch,Yaw)      = " 
    	<< _vehicleTelemetry.euler(0, 0) << ", " 
    	<< _vehicleTelemetry.euler(1, 0) << ", " 
    	<< _vehicleTelemetry.euler(2, 0) << std::endl;
    std::cout << "-------" << std::endl << std::endl;
}

void DjiHandler::getAcceleration(float32_t& ax, float32_t& ay, float32_t& az) {
	//Acceleration is updated at higher rate than updateBroadcast is called, let's do a copy ourself
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

	_vehicleTelemetry.acceleration	 = _vehicle->broadcast->getAcceleration();

	ax = _vehicleTelemetry.acceleration.x;
	ay = _vehicleTelemetry.acceleration.y;
	az = _vehicleTelemetry.acceleration.z;
}

void DjiHandler::getVelocity(float32_t& vx, float32_t& vy, float32_t& vz) {
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

	vx = _vehicleTelemetry.velocity.x;
	vy = _vehicleTelemetry.velocity.y;
	vz = _vehicleTelemetry.velocity.z;
}

void DjiHandler::getPosition(float32_t& latitude, float32_t& longitude, float32_t& altitude) {
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

	latitude = _vehicleTelemetry.globalPosition.latitude;
	longitude = _vehicleTelemetry.globalPosition.longitude;
	altitude = _vehicleTelemetry.correctedAltitude;
}

void DjiHandler::getAttitude(float32_t& roll, float32_t& pitch, float32_t& yaw) {
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

	 roll = _vehicleTelemetry.euler(0, 0);
	 pitch = _vehicleTelemetry.euler(1, 0);
	 if(roll < M_PI / 2.0f) {
	 	yaw = _vehicleTelemetry.euler(2, 0);
	 }
	 else {
	 	yaw = wrap_pi<float32_t>(_vehicleTelemetry.euler(2, 0) + M_PI);
	 }
	 
}

void DjiHandler::getQuaternion(Eigen::Quaternion<float, Eigen::DontAlign> &q) {
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

	q = _vehicleTelemetry.quaternion;
}

void DjiHandler::getStatus(uint8_t &battery, uint8_t &errorCount, uint8_t &errorStatus) {
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

	battery = _vehicleTelemetry.battery.percentage;
	errorCount = _errorCount;
	errorStatus = _vehicleTelemetry.status.error;
}

void DjiHandler::getMode(uint8_t &flightStatus, uint8_t &controlMode) {
	std::lock_guard<std::mutex> lock(_telemetry_mutex);

	flightStatus = _vehicleTelemetry.status.flight;
	controlMode = _vehicleTelemetry.status.mode;
}

void DjiHandler::getTimeOfFlight(uint64_t &tof) {
	if(_stateData->landed) {
		tof = 0;
	}
	else {
		tof = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - _takeoffTime).count();
	}
}

void DjiHandler::setCbModeChanged(std::function<void(uint8_t, uint8_t)> callback) {
	_cbModeChanged = callback;
}

bool DjiHandler::connected() {
	std::lock_guard<std::mutex> lock_v(_vehicle_mutex);
	return _vehicle != nullptr ? true : false;
}

bool DjiHandler::landed() {
	return _stateData->landed;
}

void DjiHandler::setCbWaypointReached(std::function<void(uint8_t)> callback) {
	_wpReachedCb = callback;
}

void DjiHandler::waypointReached(uint8_t index) {
	_wpReachedCb(index);
}

void DjiHandler::waypointReachedInterface(Vehicle* vehicle, RecvContainer recvFrame, UserData userData) {
	
	//std::cout << "waypoint reached" << std::endl;
	int waypointIndex = (unsigned)recvFrame.recvData.wayPointReachedData.waypoint_index;
	//std::cout << "index " << waypointIndex << std::endl;
	//int currentStatus = (unsigned)recvFrame.recvData.wayPointReachedData.current_status;
	//std::cout << "current status " << currentStatus << std::endl;

	//LogInfo() << "Waypoint " << waypointIndex << " reached with status " << (unsigned)recvFrame.recvData.wayPointReachedData.current_status;
	if((unsigned)recvFrame.recvData.wayPointReachedData.current_status == 4 
		|| (unsigned)recvFrame.recvData.wayPointReachedData.current_status == 3 
		|| (unsigned)recvFrame.recvData.wayPointReachedData.current_status == 6)
		((DjiHandler*) userData)->waypointReached(waypointIndex);

}

bool DjiHandler::checkSecurity() {
	if(_vehicleTelemetry.battery.percentage < EMERGENCY_BAT_THRESHOLD) {
		if(_stateData->battery != DroneStateData::BatteryState::EMERGENCY) {
			_stateData->battery = DroneStateData::BatteryState::EMERGENCY;
			LogErr() << "[EMERGENCY] Battery state switched to EMERGENCY.";
		}
		_emergencyState = EMERGENCY;
		return 1;
	}
	else if(_vehicleTelemetry.battery.percentage < CRITICAL_BAT_THRESHOLD) {
		if(_stateData->battery != DroneStateData::BatteryState::CRITICAL) {
			_stateData->battery = DroneStateData::BatteryState::CRITICAL;
			LogWarn() << "[CRIT] Battery state switched to CRITICAL.";
		}
		_emergencyState = CRITICAL;
		return 1;
	}
	else if(_vehicleTelemetry.battery.percentage < WARNING_BAT_THRESHOLD) {
		if(_stateData->battery != DroneStateData::BatteryState::WARNING) {
			_stateData->battery = DroneStateData::BatteryState::WARNING;
			LogWarn() << "[WARN] Battery state switched to WARNING.";
		}
		_emergencyState = WARNING;
		return 1;
	}
	else {
		if(_stateData->battery != DroneStateData::BatteryState::NORMAL) {
			_stateData->battery = DroneStateData::BatteryState::NORMAL;
			LogInfo() << "Battery state switched to NORMAL.";
		}
		return 0;
	}
}

uint8_t DjiHandler::startMission() {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	// Waypoint Mission: Start
	ACK::ErrorCode startAck = _vehicle->missionManager->wpMission->start(DJI_TIMEOUT);
	ACK::getErrorCodeMessage(startAck, __func__);
	if (ACK::getError(startAck)) {
		ACK::getErrorCodeMessage(startAck, __func__);
		LogInfo() << "Starting waypoint mission failed";
		return true;
	}
	else {
		LogInfo() << "Starting the mission";
		return false;
	}
}

uint8_t DjiHandler::pauseMission() {
	if(_vehicleTelemetry.status.mode == 14) {
		std::lock_guard<std::mutex> lock(_vehicle_mutex);

		ACK::ErrorCode ack = _vehicle->missionManager->wpMission->pause(DJI_TIMEOUT);

		return printIfError(ack);
	}
	else {
		return false;
	}
}

uint8_t DjiHandler::resumeMission() {
	if(_vehicleTelemetry.status.mode == 14) {
		std::lock_guard<std::mutex> lock(_vehicle_mutex);

		ACK::ErrorCode ack = _vehicle->missionManager->wpMission->resume(DJI_TIMEOUT);

		return printIfError(ack);
	}
	else {
		return false;
	}
}

uint8_t DjiHandler::stopMission() {
	if(_vehicleTelemetry.status.mode == 14) {
		std::lock_guard<std::mutex> lock(_vehicle_mutex);

		ACK::ErrorCode ack;
		uint8_t stop = 1;

		_vehicle->protocolLayer->send(2, _vehicle->getEncryption(), 
									  OpenProtocolCMD::CMDSet::Mission::waypointSetStart, &stop,
									  sizeof(stop), 500, 2, false, 2);

		ack = *((ACK::ErrorCode*)_vehicle->waitForACK(
		OpenProtocolCMD::CMDSet::Mission::waypointSetStart, DJI_TIMEOUT));

		return printIfError(ack);
	}
	else {
		return false;
	}
}

uint8_t DjiHandler::uploadWaypoints(std::vector<WayPointSettings>& wp_list, WayPointInitSettings& missionSettings) {
	std::lock_guard<std::mutex> lock(_vehicle_mutex);

	bool errorFlag = false;

	//DEBUG
	if(_vehicle->missionManager->wpMission == nullptr || 1) {
		LogInfo() << "Initializing waypoint mission manager..";

		ACK::ErrorCode initAck = _vehicle->missionManager->init(DJI_MISSION_TYPE::WAYPOINT, DJI_TIMEOUT, &missionSettings);
		if (ACK::getError(initAck)) {
			ACK::getErrorCodeMessage(initAck, __func__);
			LogErr() << "Init Waypoint Mission FAILED.";
			errorFlag = true;
		}

		//set callback for waypoint events
		_vehicle->missionManager->wpMission->setWaypointCallback(waypointReachedInterface, this);
	}
	else {
		LogInfo() << "Updating waypoint mission settings with " << unsigned(missionSettings.indexNumber) << " waypoints.";
		ACK::ErrorCode updateAck = _vehicle->missionManager->wpMission->init(&missionSettings, DJI_TIMEOUT);
		if (ACK::getError(updateAck)) {
			ACK::getErrorCodeMessage(updateAck, __func__);
			LogErr() << "Update waypoint mission settings FAILED.";
			errorFlag = true;
		}
	}

	_vehicle->missionManager->printInfo();

	// Waypoint Mission: Upload the waypoints
	LogInfo() << "Uploading Waypoints..";
	
	for (std::vector<WayPointSettings>::iterator wp = wp_list.begin(); wp != wp_list.end(); ++wp) {

		LogInfo() << "Waypoint created at (LLA): " << unsigned(wp->index) << "\t" << static_cast<float>(wp->latitude) << "\t" << static_cast<float>(wp->longitude) << "\t" << static_cast<float>(wp->altitude) << "\t" << wp->actionTimeLimit;
		
		ACK::WayPointIndex wpDataACK = _vehicle->missionManager->wpMission->uploadIndexData(&(*wp), DJI_TIMEOUT);

		if(ACK::getError(wpDataACK.ack)){
			LogErr() << "Upload failed. Try again!";
			errorFlag = true;
		}

		ACK::getErrorCodeMessage(wpDataACK.ack, __func__);
	}

	return errorFlag;
}
