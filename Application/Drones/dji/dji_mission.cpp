#include "dji_mission.h"

DjiMission::DjiMission(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::function<void(uint8_t)> wpReachedCb) :
BaseMission(lidar, drone, wpReachedCb),
_inRecovery(false)
{
	_drone->setCbWaypointReached(std::bind(&DjiMission::waypointReached, this, std::placeholders::_1));
}

uint8_t DjiMission::start() {
	if(_inRecovery) {
		_inRecovery = false;
		return 0;
	}
	else {
		return _drone->startMission();
	}
}

uint8_t DjiMission::pause() {
	return _drone->pauseMission();
}

uint8_t DjiMission::resume() {
	return _drone->resumeMission();
}

uint8_t DjiMission::stop() {
	return _drone->stopMission();
}

void DjiMission::update() {

}

void DjiMission::inRecovery() {
	_inRecovery = true;
}

void DjiMission::waypointReached(uint8_t waypointIndex) {
	_wpReachedCb(waypointIndex);
}

uint8_t DjiMission::uploadMission(std::vector<Waypoint> mission, GpsPosition plannedHome, float cruiseSpeed) {
	_cruiseSpeed = cruiseSpeed;

	WayPointInitSettings missionData;
	setWaypointInitDefaults(&missionData);

	WayPointSettings djiWp;
	setWaypointDefaults(&djiWp);

	std::vector<WayPointSettings> djiMission;

	uint8_t wpIndex = 0;

	for (auto wp = mission.begin(); wp != mission.end(); ++wp) {
		djiWp.index = wpIndex;
		djiWp.longitude = wp->location.longitude;
		djiWp.latitude = wp->location.latitude;
		djiWp.altitude = wp->location.altitude - plannedHome.altitude;
		//TODO altitude conversion

		djiMission.push_back(djiWp);
		wpIndex++;
	}

	missionData.indexNumber = djiMission.size();

	return _drone->uploadWaypoints(djiMission, missionData);
}

void DjiMission::setWaypointDefaults(WayPointSettings* wp) {
	wp->damping         = 15.0;
	wp->yaw             = 0;
	wp->gimbalPitch     = 0;
	wp->turnMode        = 0;
	wp->hasAction       = 0;
	wp->actionTimeLimit = 100;
	wp->actionNumber    = 0;
	wp->actionRepeat    = 0;

	for (int i = 0; i < 16; ++i)
	{
		wp->commandList[i]      = 0;
		wp->commandParameter[i] = 0;
	}
}

//TODO, set parameters
void DjiMission::setWaypointInitDefaults(WayPointInitSettings* fdata) {
	fdata->maxVelocity    = _cruiseSpeed;
	fdata->idleVelocity   = _cruiseSpeed;
	fdata->finishAction   = 0;
	fdata->executiveTimes = 1;
	fdata->yawMode        = 0;
	fdata->traceMode      = 1;
	fdata->RCLostAction   = 1;
	fdata->gimbalPitch    = 0;
	fdata->latitude       = 0;
	fdata->longitude      = 0;
	fdata->altitude       = 0;
}

