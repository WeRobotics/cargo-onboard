#pragma once

#include <fstream>
#include <dji_vehicle.hpp>

class DjiLoader
{
public:
  DjiLoader();
  ~DjiLoader();

public:
  void setParams(int appId, std::string encKey, std::string device, unsigned int baudrate);
  bool initVehicle();
  bool validateSerialPort();
  bool testSerialConnection();

  DJI::OSDK::Vehicle* getVehicle();

private:
  DJI::OSDK::Vehicle*              vehicle;
  DJI::OSDK::LinuxSerialDevice*    testSerialDevice;
  DJI::OSDK::Vehicle::ActivateData activateData;
  int                              functionTimeout; // seconds
  unsigned int                     _baudrate;
  std::string                      _device;
};
