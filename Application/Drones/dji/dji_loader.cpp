/*! @file dji_linux_helpers.cpp
 *  @version 3.3
 *  @date Sep 12 2017
 *
 *  @brief
 *  Helper functions to handle serial port validation and vehicle initialization
 *
 *  @Copyright (c) 2017 DJI
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
#include "dji_loader.h"

#include <Library/logging/log.h>
//#include <dji_linux_helpers.hpp>

using namespace DJI::OSDK;

DjiLoader::DjiLoader()
{
  this->functionTimeout     = 5; // second
  this->vehicle             = nullptr;
  this->testSerialDevice    = nullptr;
}

DjiLoader::~DjiLoader()
{
  if (vehicle){
    delete (vehicle);
    vehicle = nullptr;
  }

  if (testSerialDevice){
    delete (testSerialDevice);
    testSerialDevice = nullptr;
  }
}

void DjiLoader::setParams(int appId, std::string encKey, std::string device, unsigned int baudrate) {
	_baudrate = baudrate;
	_device = device;
	activateData.ID = appId;

	char app_key[65];
    activateData.encKey = app_key;
    strcpy(activateData.encKey, encKey.c_str());

	this->testSerialDevice = new LinuxSerialDevice(_device.c_str(), baudrate);
}

bool
DjiLoader::initVehicle()
{
  bool threadSupport = true;
  if(vehicle == nullptr) {
    this->vehicle      = new Vehicle(_device.c_str(),
                                     _baudrate,
                                     threadSupport,
                                     false);

    // Check if the communication is working fine
    if (!vehicle->protocolLayer->getDriver()->getDeviceStatus())
    {
      LogErr() << "Getting drone status failed.";
      delete (vehicle);
      this->vehicle     = nullptr;
      return false;
    }

    // Activate
    ACK::DroneVersion droneVersion = vehicle->getDroneVersion(functionTimeout);
    ACK::ErrorCode ack   = vehicle->activate(&activateData, functionTimeout);

    if (ACK::getError(ack))
    {
      ACK::getErrorCodeMessage(ack, __func__);
      delete (vehicle);
      this->vehicle     = nullptr;
      return false;
    }
  }
  else {
      LogWarn() << "Vehicle already initialized.";
  }

  return true;
}

bool DjiLoader::testSerialConnection() {
  testSerialDevice->init();
  return testSerialDevice->getDeviceStatus();
}

bool DjiLoader::validateSerialPort()
{
  static const int BUFFER_SIZE = 2048;

  //! Check the serial channel for data
  uint8_t buf[BUFFER_SIZE];
  if (!testSerialDevice->setSerialPureTimedRead())
  {
    LogErr() << "Failed to set up port for timed read.";
    return (false);
  };
  usleep(100000);
  if (testSerialDevice->serialRead(buf, BUFFER_SIZE))
  {
    LogInfo() << "Succeeded to read from serial device";
  }
  else
  {
    LogErr() << "\"Failed to read from serial device. The Onboard SDK is not "
             "communicating with your drone. ";
    // serialDevice->unsetSerialPureTimedRead();
    return (false);
  }

  // If we reach here, _serialRead succeeded.
  LogInfo() << "Checking baudrate.";
  int baudCheckStatus = testSerialDevice->checkBaudRate(buf);
  if (baudCheckStatus == -1)
  {
    LogErr() << "No data on the line. Is your drone powered on?";
    return false;
  }
  if (baudCheckStatus == -2)
  {
    LogErr() << "Baud rate mismatch found. Make sure DJI Assistant 2 has the same "
             "baud setting as the one in User_Config.h";
    return (false);
  }

  // All the tests passed and the serial device is properly set up
  testSerialDevice->unsetSerialPureTimedRead();
  
  return (true);
}  

DJI::OSDK::Vehicle* DjiLoader::getVehicle()
{
  return this->vehicle;
}