#pragma once

#include "../base_mission.h"

class DjiMission : public BaseMission {
public:
	DjiMission(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::function<void(uint8_t)> wpReachedCb);

	uint8_t start();
	uint8_t pause();
	uint8_t resume();
	uint8_t stop();

	void inRecovery();

	void waypointReached(uint8_t waypointIndex);

	uint8_t uploadMission(std::vector<Waypoint> mission, GpsPosition plannedHome, float cruiseSpeed);
private:
	void update();

	bool _inRecovery;
	
	float _cruiseSpeed;
	void setWaypointDefaults(WayPointSettings* wp);
	void setWaypointInitDefaults(WayPointInitSettings* fdata);
};