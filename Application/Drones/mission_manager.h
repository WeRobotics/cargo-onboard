#pragma once

#include <Application/state.h>

#include "control_mode.h"
#include "modes/landing_mode.h"
#include "modes/manual_mission.h"

#include "dji/dji_mission.h"
#include "drone.h"

static constexpr float DEFAULT_CRUISE_SPEED = 10.0f;

class CargoMissionManager {
public:
	CargoMissionManager(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::shared_ptr<MissionStateData> missionData);

	uint8_t initMission(std::string filename);
	uint8_t initRallypoint();
	uint8_t initEmergencyLanding();
	uint8_t initLanding();

	uint8_t recoverMission(std::string missionFile, uint8_t missionIndex, float terrainAltitude, bool missionIsDji, bool missionIsFinished);

	uint8_t start();
	uint8_t pause();
	uint8_t resume();
	uint8_t stop();

	void update();

	void waypointReached(uint8_t waypointIndex);

	void getRemainingDistanceAndTime(float &distance, uint64_t &tof);
	void getEstimatedGroundDistance(float &height);

	float distanceToClosestRallyPoint();
	void getClosestRallyPoint(Waypoint &closestRallyPoint);

protected:
	GpsPosition _plannedHomeMissionWp;
	std::vector<Waypoint> _mission;
	std::vector<Waypoint> _rallypoints;

	std::atomic<uint8_t> 		_missionIndex;
	std::atomic<uint8_t> 		_missionSize;

	std::shared_ptr<MissionStateData> _missionData;
	uint16_t _missionRemainingDistance;
	float _terrain_altitude;
	//_missionData.hasPrecisionLanding
	//_missionData.isDjiMission
	//_missionData.cruiseSpeed
	//_missionData.destinationUnreachable
	//_missionData.missionFinished
	//_missionData.requestPrecisionLanding

	LandingMode 	_landingMode;
	DjiMission		_djiMission;
	ManualMission	_manualMission;

	ControlMode *_actualMode;

	std::shared_ptr<wer::hal::DistanceSensor> _lidar;
	std::shared_ptr<Drone> _drone;

	GpsPosition _position;

	uint8_t missionCheck();
	void checkSecurity();
	float remainingMissionDistance();
	uint8_t loadWaypointMission(std::string filename);
	void updatePosition();
	void setTerrainAltitude(float altitude);
};