#pragma once

#include <iostream>
#include <mutex>
#include <vector>
#include <atomic>
#include <memory>

#include "dji_status.hpp"
#include <dji_vehicle.hpp>

#include <Library/eigen/Eigen/Geometry>
#include <Application/state.h>
#include "location_struct.h"

//Land at current position
static constexpr uint8_t EMERGENCY_BAT_THRESHOLD = 10;
//Destination unreachable -> land at current position
static constexpr uint8_t CRITICAL_BAT_THRESHOLD = 15;
//We are now coming into a danger situation 
static constexpr uint8_t WARNING_BAT_THRESHOLD = 20;

class Drone {
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	virtual bool testSerialConnection() = 0;
	virtual void recoverStart(GpsPosition takeoffPosition, float offset) = 0;

	virtual bool initVehicle() = 0;
	virtual void initParachute() = 0;
	virtual bool configureVehicle() = 0;
	virtual bool releaseControl() = 0;
	virtual bool obtainControl() = 0;

	virtual bool connected() = 0;
	virtual bool landed() = 0;

	// Drone command functions
	virtual uint8_t takeoff() = 0;
	virtual uint8_t land() = 0;
	virtual uint8_t disarm() = 0;
	virtual uint8_t arm() = 0;
	virtual uint8_t goHome() = 0;
	virtual void emergencyBrake() = 0;
	virtual uint8_t killSwitch() = 0;
	virtual void triggerParachute() = 0;

	// Control functions
	virtual void setManualSp(float32_t velX, float32_t velY, float32_t velZ, float32_t yawRate) = 0;
	virtual void setManualSpAbs(float32_t velX, float32_t velY, float32_t velZ, float32_t yawRate) = 0;

	// Mission command related functions
	virtual uint8_t startMission() = 0;
	virtual uint8_t pauseMission() = 0;
	virtual uint8_t resumeMission() = 0;
	virtual uint8_t stopMission() = 0;
	virtual uint8_t uploadWaypoints(std::vector<WayPointSettings>& wp_list, WayPointInitSettings& missionSettings) = 0;
	virtual bool checkSecurity() = 0;

	//Telemetry related functions
	virtual void updateBroadcastData() = 0;
	virtual bool dataValid() = 0;
	virtual void getAcceleration(float32_t& ax, float32_t& ay, float32_t& az) = 0;
	virtual void getVelocity(float32_t& vx, float32_t& vy, float32_t& vz) = 0;
	virtual void getPosition(float32_t& latitude, float32_t& longitude, float32_t& altitude) = 0;
	virtual void getAttitude(float32_t& roll, float32_t& pitch, float32_t& yaw) = 0;
	virtual void getQuaternion(Eigen::Quaternion<float, Eigen::DontAlign> &q) = 0;
	virtual void getStatus(uint8_t &battery, uint8_t &errorCount, uint8_t &errorStatus) = 0;
	virtual void getMode(uint8_t &flightStatus, uint8_t &controlMode) = 0;
	virtual void getTimeOfFlight(uint64_t &tof) = 0;
	virtual void getEstimatedGroundDistance(float &groundDistance) = 0;

	// Waypoint callback functions
	virtual void setCbModeChanged(std::function<void(uint8_t, uint8_t)> callback) = 0;
	virtual void setCbWaypointReached(std::function<void(uint8_t)> callback) = 0;

	//DJI OFFSET...
	virtual void computeGpsOffset() = 0;
	virtual void activateOffset() = 0;
	virtual void disactivateOffset() = 0;

	virtual void makeAltitudeOffsetDroneToMap(float groundAltitude) = 0;
	virtual void setTerrainAltitudeFromMap(float altitude) = 0;
	virtual void setTerrainAltitudeFromDrone(float altitude) = 0;
};