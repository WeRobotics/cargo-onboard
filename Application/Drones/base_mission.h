#pragma once

#include <functional>
#include <vector>

#include "control_mode.h"

class BaseMission : public ControlMode {
public:
	BaseMission(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::function<void(uint8_t)> wpReachedCb) :
	ControlMode(lidar, drone), _wpReachedCb(wpReachedCb) {};

	virtual uint8_t start() = 0;
	virtual uint8_t resume() = 0;
	virtual uint8_t pause() = 0;
	virtual uint8_t stop() = 0;

	virtual void update() = 0;

	virtual void waypointReached(uint8_t waypointIndex) = 0;

	virtual uint8_t uploadMission(std::vector<Waypoint> mission, GpsPosition plannedHome, float cruiseSpeed) = 0;

protected:
	std::function<void(uint8_t)> _wpReachedCb;
};