#include "manual_mode.h"

#include <iostream>

using namespace std::placeholders;

#define MAX_H_VELOCITY 3.0f
#define MAX_V_VELOCITY 3.0f
#define MAX_YAW_RATE 30.0f

ManualMode::ManualMode(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::function<void(void)> requestControlCb) :
ControlMode(lidar, drone),
_requestControlCb(requestControlCb)
{
	wer::com::Handler::get()->registerMavlinkMessageHandler(MAVLINK_MSG_ID_MANUAL_CONTROL, std::bind(&ManualMode::processManualControl, this, _1), this);
    wer::com::Handler::get()->registerMavlinkMessageHandler(MAVLINK_MSG_ID_COMMAND_LONG, std::bind(&ManualMode::processCommandLong, this, _1), this);
}

ManualMode::~ManualMode() {
    wer::com::Handler::get()->unregisterMavlinkMessageHandlers(this);
}

uint8_t ManualMode::start() {
	//pass autopilot in manual mode
	_active = true;
	return _active;
}
uint8_t ManualMode::stop() {

	_active = false;
	return _active;
}

uint8_t ManualMode::isActive() {
	return _active;
}

void ManualMode::runOnce() {

}

void ManualMode::processManualControl(const mavlink_message_t &message) {
    mavlink_manual_control_t manualSp;
    mavlink_msg_manual_control_decode(&message, &manualSp);

    if(_active) {
        static uint16_t oldButtons = 0;

    	float32_t velX = stickExp(-manualSp.x) * MAX_H_VELOCITY;
    	float32_t velY = stickExp(manualSp.y) * MAX_H_VELOCITY;
    	float32_t velZ = stickExp(-manualSp.z) * MAX_V_VELOCITY;
    	float32_t yawRate = stickExp(manualSp.r) * MAX_YAW_RATE;

    	_drone->setManualSp(velX, velY, velZ, yawRate);

        if((manualSp.buttons & (1 << 0)) != (oldButtons & (1 << 0))) {
            _drone->land();
        }

        if((manualSp.buttons & (1 << 1)) != (oldButtons & (1 << 1))) {
            _drone->takeoff();
        }

        if((manualSp.buttons & (1 << 2)) != (oldButtons & (1 << 2))) {
            _drone->arm();
        }

        if((manualSp.buttons & (1 << 3)) != (oldButtons & (1 << 3))) {
            _drone->disarm();
        }

        oldButtons = manualSp.buttons;
        _requestControlCb();
    }
    else {
        _requestControlCb();
    }
}

float ManualMode::stickExp(int16_t input) {
    float input_norm = input / 1000.f;
    return ((4.0f * input_norm) + (6.0f * input_norm * input_norm * input_norm)) / 10.0f;
}

void ManualMode::processCommandLong(const mavlink_message_t &message) {
    mavlink_command_long_t command;
    mavlink_msg_command_long_decode(&message, &command);

    if(_active) {
        switch(command.command) {

            case MAV_CMD_COMPONENT_ARM_DISARM:
                //PARAM 1 : 0:disarm, 1:arm
                _drone->arm();
            break;

            case MAV_CMD_NAV_LAND: //0:disable, 1:enable, 2:release
            {
                //PARAM 1 : Abort Alt
                //PARAM 2 : 0:normal landing, 1:opportunistic precision landing, 2:required precision landing, 3: emergency landing
                //PARAM 4 : Desired yaw angle
                //PARAM 5 : Latitutde
                //PARAM 6 : Longitude
                //PARAM 6 : Altitude (Ground level)
                uint32_t type = *reinterpret_cast<uint32_t*>(&command.param2);
                if(type == 0)
                    _drone->land();
            }
            break;

            case MAV_CMD_NAV_TAKEOFF: //0:disable, 1:enable, 2:release
                //PARAM 1 : Minimum pitch
                //PARAM 4 : Desired yaw angle
                //PARAM 5 : Latitutde
                //PARAM 6 : Longitude
                //PARAM 6 : Altitude
                _drone->takeoff();
            break;
        }
    }
}