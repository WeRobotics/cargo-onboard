#pragma once

#include <mutex>

#include <Library/eigen/Eigen/Geometry>
#include <Library/filters/kalman_filter.h>

#include <Hardware/include/distance_sensor.h>

#include <Communication/handler.h>

#include <Application/LandingTarget/landing_target.h>
#include <Application/Drones/control_mode.h>
#include <Application/Drones/drone.h>

#define TIME_BUFFER_SIZE 50 	// 20ms per measure = 1s buffer

struct TargetEstimatorParams {
	float vel_unc;
	float meas_unc;
	float scale_x;
	float scale_y;

	float max_var;
};

class PrecisionLandingMode : public ControlMode {
public:
	PrecisionLandingMode(std::shared_ptr<Drone> drone, std::shared_ptr<wer::hal::DistanceSensor> lidar, 
						 std::shared_ptr<wer::cargo::LandingTarget> landingTarget, std::function<void(void)> _resetTimeoutCb, 
						 std::shared_ptr<CallEveryHandler> callEveryHandler, TargetEstimatorParams* params = nullptr);
	~PrecisionLandingMode();

	uint8_t start();
	uint8_t stop();
	uint8_t pause();
	uint8_t resume();

	uint8_t isActive();

	void setMeasurement(struct wer::cargo::landing_report_s report);

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

private:
	void update();

	void updateFilter(float groundDistance, uint8_t measTimeIndex);
	float control_p(float erreur, float gain, float limit);
	void control(float x, float y, float groundDistance, float estimatedGroundDistance);
	void updateBuffer();
		void predict();

	void send(float x, float y, float z);

	TargetEstimatorParams _params;

	std::shared_ptr<wer::cargo::LandingTarget> _landingTarget;

	std::function<void(void)> _resetTimeoutCb;

	uint8_t _active = false;
	std::atomic<uint8_t> _missionPaused = false;

	/* timeout after which filter is reset if target not seen */
	static constexpr uint32_t landing_target_estimator_TIMEOUT_US = 5000000;

	// keep track of which topics we have received
	bool _estimator_initialized[wer::cargo::MAX_LANDING_TARGETS];
	// keep track of whether last measurement was rejected
	bool _faulty[wer::cargo::MAX_LANDING_TARGETS];
	// Keep track if we got bad measurment during last update
	bool _faultyMeasurement[wer::cargo::MAX_LANDING_TARGETS];

	bool _forceLanding;

	Eigen::Matrix<float, 3, 3, Eigen::DontAlign> _R_att;
	Eigen::Matrix<float, 2, 1, Eigen::DontAlign> _rel_pos[wer::cargo::MAX_LANDING_TARGETS];

	KalmanFilter _kalman_filter_x[wer::cargo::MAX_LANDING_TARGETS];
	KalmanFilter _kalman_filter_y[wer::cargo::MAX_LANDING_TARGETS];

	std::chrono::time_point<std::chrono::steady_clock> _last_predict[wer::cargo::MAX_LANDING_TARGETS]; // timestamp of last filter prediction
	std::chrono::time_point<std::chrono::steady_clock> _last_update[wer::cargo::MAX_LANDING_TARGETS]; // timestamp of last filter update (used to check timeout)

	struct wer::cargo::landing_report_s _targetReport;
	bool _consumed;

	bool _groundDistanceError;
	bool _beaconError;
	bool _faultyDistance;

	float _altEstiOffset;
	std::chrono::time_point<std::chrono::steady_clock> _lastMeasureTime;

	std::mutex _report_mtx;

	struct TimingInfo {
		float x[wer::cargo::MAX_LANDING_TARGETS];
		float y[wer::cargo::MAX_LANDING_TARGETS];

		Eigen::Quaternion<float, Eigen::DontAlign> q_att;
		float estimatedGroundDistance;
		float measuredGroundDistance;
	};

	std::mutex _timing_mtx;
	TimingInfo _timeBuffer[TIME_BUFFER_SIZE];
	uint8_t _timingBufferIndex;

	std::shared_ptr<CallEveryHandler> _callEveryHandler;
    void* _timerCookie;
};