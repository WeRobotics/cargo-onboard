#pragma once

#include "../control_mode.h"

static constexpr float DEFAULT_APPROACH_SPEED = -3.0f;
static constexpr float DEFAULT_DESCENT_SPEED = -2.0f;
static constexpr float DEFAULT_FINAL_SPEED = -0.5f;

class LandingMode : public ControlMode {
public:
	LandingMode(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone);

	void reset();
	
	uint8_t start();
	uint8_t stop();
	uint8_t pause();
	uint8_t resume();

	void setLandingAltitude(float altitude);
	void setApproachSpeed(float speed);
	void setDescentSpeed(float speed);
	void setFinalSpeed(float speed);

protected:
	void update();

	std::atomic<uint8_t> _missionPaused = false;
	
	uint8_t _landingStep;

	float _landingAltitude;
	float _approachSpeed;
	float _descentSpeed;
	float _finalSpeed;
};