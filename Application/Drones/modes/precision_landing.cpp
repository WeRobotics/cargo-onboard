#include "precision_landing.h"

#include <iostream>
#include <cmath>
#include <cfloat>
#include <chrono>

//#include "../Library/flightLog.h"

#include <Library/logging/log.h>

#define LTEST_SCALE_X       1.0f
#define LTEST_SCALE_Y       1.0f

#define MEAS_ARUCO_UNC      0.01f
#define MEAS_DJI_VEL_UNC    0.1f

#define MAX_VAR_ESTI        1.0f

#define SEC2USEC 1000000.0f
/*
class enum AltitudeSource {
    ESTIMATED = 1,
    LIDAR = 2,
    TAGS = 4,
};

struct {
    AltitudeSource useAltitude;

    float approach_altitude;
    float descent_altitude;
    float final_altitude;

    float final_gain;
    float final_radius;
    float final_h_speed;
    float final_v_speed;

    float descent_gain;
    float descent_radius_1;
    float descent_radius_2;
    float descent_radius_3;
    float descent_h_speed;
    float descent_v_speed;

    float approach_v_spped;
};
*/
#define APPROACH_LANDING_HEIGHT     40.0f
#define DESCENT_LANDING_HEIGHT      20.0f
#define FINAL_LANDING_HEIGHT        1.0f

#define OBS_HEIGHT                  18.0f
#define OBS_CLIMB_V_SPEED           0.5f

#define FINAL_LANDING_GAIN          0.6f
#define FINAL_LANDING_RADIUS        0.5f
#define FINAL_LANDING_H_SPEED       0.5f    //[m/s]
#define FINAL_LANDING_V_SPEED       -0.5f

#define DESCENT_LANDING_GAIN        0.3f
#define DESCENT_LANDING_RADIUS_1    1.0f
#define DESCENT_LANDING_RADIUS_2    3.0f
#define DESCENT_LANDING_RADIUS_3    5.0f
#define DESCENT_LANDING_H_SPEED     1.0f
#define DESCENT_LANDING_V_SPEED     -1.5f

#define APPROACH_LANDING_V_SPEED    -3.0f

#define MAX_ALTITUDE_ERROR          20.0f

using namespace std::placeholders;

PrecisionLandingMode::PrecisionLandingMode(std::shared_ptr<Drone> drone, 
                                           std::shared_ptr<wer::hal::DistanceSensor> lidar, 
                                           std::shared_ptr<wer::cargo::LandingTarget> landingTarget, 
                                           std::function<void(void)> resetTimeoutCb,
                                           std::shared_ptr<CallEveryHandler> callEveryHandler,
                                           TargetEstimatorParams* params) :
ControlMode(lidar, drone),
_landingTarget(landingTarget),
_resetTimeoutCb(resetTimeoutCb),
_forceLanding(false),
_consumed(true),
_groundDistanceError(false),
_beaconError(false),
_timingBufferIndex(0),
_callEveryHandler(callEveryHandler)
{
    for(int i = 0; i < wer::cargo::MAX_LANDING_TARGETS; i++) {
        _faultyMeasurement[i] = false;
        _estimator_initialized[i] = false;
        _faulty[i] = false;
    }

    _faultyDistance = false;

    if(params) {
        _params.vel_unc = params->vel_unc;
        _params.meas_unc = params->meas_unc;
        _params.scale_x = params->scale_x;
        _params.scale_y = params->scale_y;
        _params.max_var = params->max_var;
    }
    else {
        _params.vel_unc = MEAS_DJI_VEL_UNC;
        _params.meas_unc = MEAS_ARUCO_UNC;
        _params.scale_x = LTEST_SCALE_X;
        _params.scale_y = LTEST_SCALE_Y;
        _params.max_var = MAX_VAR_ESTI;
    }
}

PrecisionLandingMode::~PrecisionLandingMode() {
    stop();
}

uint8_t PrecisionLandingMode::start() {
    _landingTarget->start();
    _lidar->enable();

	//Pass autopilot in manual mode
	_active = true;
    _missionPaused = false;

    for(int i = 0; i < wer::cargo::MAX_LANDING_TARGETS; i++) {
        _estimator_initialized[i] = false;
    }

    _timingBufferIndex = 0;
    _forceLanding = false;
    _altEstiOffset = 0.0f;
    _faultyDistance = false;
    _consumed = true;
    _groundDistanceError = false;
    _beaconError = false;

    Eigen::Quaternion<float, Eigen::DontAlign> currentAtt;
    _drone->getQuaternion(currentAtt);

    float estimatedGroundDistance;
    _drone->getEstimatedGroundDistance(estimatedGroundDistance);

    LogLanding() << "timestamp;mode;x;y;vx;vy;velX;velY;velZ;Lidar;EstGndDist;AltOffset;kal_1_x;kal_1_y;rel_1_x;rel_1_y;kal_1_cov_x;kal_1_cov_y;kal_2_x;kal_2_y;rel_2_x;rel_2_y;kal_2_cov_x;kal_2_cov_y;kal_3_x;kal_3_y;rel_3_x;rel_3_y;kal_3_cov_x;kal_3_cov_y";

    for(int i = 0; i < TIME_BUFFER_SIZE; i++) {
        for(int j = 0; j < wer::cargo::MAX_LANDING_TARGETS; j++) {
            _timeBuffer[i].x[j] = std::numeric_limits<float>::quiet_NaN();
            _timeBuffer[i].y[j] = std::numeric_limits<float>::quiet_NaN();
        }

        _timeBuffer[i].q_att = currentAtt;
        _timeBuffer[i].estimatedGroundDistance = estimatedGroundDistance;
        _timeBuffer[i].measuredGroundDistance = std::numeric_limits<float>::quiet_NaN();
    }

    _callEveryHandler->add(std::bind(&ControlMode::cycle, this), 0.02f, &_timerCookie);

    LogInfo() << "Precision landing started. Estimated ground distance = " << estimatedGroundDistance << "m.";

	return _active;
}
uint8_t PrecisionLandingMode::stop() {
    _landingTarget->stop();
    _lidar->disable();

    _callEveryHandler->remove(_timerCookie);

	_active = false;
	return _active;
}

uint8_t PrecisionLandingMode::pause() {
    _missionPaused = true;
    return 0;
}

uint8_t PrecisionLandingMode::resume() {
    _missionPaused = false;
    return 0;
}

uint8_t PrecisionLandingMode::isActive() {
	return _active;
}

void PrecisionLandingMode::predict()
{
    /* predict */
     auto now = std::chrono::steady_clock::now();

    for(int j = 0; j < wer::cargo::MAX_LANDING_TARGETS; j++) {
        if(_estimator_initialized[j]) {
            if (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - _last_update[j]).count() > landing_target_estimator_TIMEOUT_US) {
                LogWarn() << "Timeout of landing target estimator.";
                _estimator_initialized[j] = false;

            } 
            else if(_active && _drone->connected()) {
                float dt = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - _last_predict[j]).count() / SEC2USEC;

                _kalman_filter_x[j].predict(dt, -_velocity(0), _params.vel_unc);
                _kalman_filter_y[j].predict(dt, -_velocity(1), _params.vel_unc);
                _last_predict[j] = now;
            }
        }
    }
}

void PrecisionLandingMode::setMeasurement(wer::cargo::landing_report_s report) {
    std::lock_guard<std::mutex> lock_t(_report_mtx);
    _targetReport = report;
    _consumed = false;
}

void PrecisionLandingMode::updateFilter(float groundDistance, uint8_t measTimeIndex) {
    if(_active) {

        if (!_drone->connected() || !std::isfinite(groundDistance)) {
            if(_faultyDistance == false) {
                LogWarn() << "Ground not connected or ground distance not valid.";
                _faultyDistance = true;
            }
            
            // don't have the data needed for an update
            return;
        }

    
        _timing_mtx.lock();
        _R_att = _timeBuffer[measTimeIndex].q_att.toRotationMatrix();
        _timing_mtx.unlock();

        auto now = std::chrono::steady_clock::now();


        for(int j = 0; j < wer::cargo::MAX_LANDING_TARGETS; j++) {

            if(_targetReport.targets[j].valid) {

                if(!std::isfinite(_targetReport.targets[j].pos_y) || !std::isfinite(_targetReport.targets[j].pos_x)) {
                    if(_faultyMeasurement[j] == false) {
                        LogErr() << "The position of target id " << j << " IR Lock is not finite.";
                        _faultyMeasurement[j] = true;
                    }
                    continue;
                }
                else {
                    _faultyMeasurement[j] = false;
                }

                // Default orientation has camera x pointing in body y, camera y in body -x

                //LogLanding() << "IR Report POS_X = " << irLockReport.target.pos_x  << "\t POS_Y = " << irLockReport.target.pos_y << "\t Hgt = " << groundDistance;

                Eigen::Matrix<float, 3, 1, Eigen::DontAlign> sensor_ray; // ray pointing towards target in body frame
                sensor_ray(0) = -_targetReport.targets[j].pos_y * _params.scale_y; // forward
                sensor_ray(1) = _targetReport.targets[j].pos_x * _params.scale_x; // right
                sensor_ray(2) = 1.0f;

                // rotate the unit ray into the navigation frame, assume sensor frame = body frame
                sensor_ray = _R_att * sensor_ray;

                //LogLanding() << "Sensor ray 0 = " << sensor_ray(0)  << "\t 1 = " << sensor_ray(1) << "\t 2 = " << sensor_ray(2);

                if (fabsf(sensor_ray(2)) < 1e-6f) {
                    // z component of measurement unsafe, don't use this measurement
                    LogErr() << "Sensor ray is not safe on estimator id " << j << ", value of axis 3 close to 0.";
                    continue;
                }

                // scale the ray s.t. the z component has length of dist
                //_rel_pos(0) = sensor_ray(0) / sensor_ray(2) * groundDistance;
                _rel_pos[j](0) = sensor_ray(0) * groundDistance;
                //_rel_pos(1) = sensor_ray(1) / sensor_ray(2) * groundDistance;
                _rel_pos[j](1) = sensor_ray(1) * groundDistance;

                //LogLanding() << "Relative position X = " << _rel_pos(0) << "\t Y = " << _rel_pos(1);

                if (!_estimator_initialized[j]) {
                    LogInfo() << "Init precision landing estimator **" << j << "**";

                    _kalman_filter_x[j].init(_rel_pos[j](0), _params.meas_unc * groundDistance);
                    _kalman_filter_y[j].init(_rel_pos[j](1), _params.meas_unc * groundDistance);

                    _estimator_initialized[j] = true;
                    _last_update[j] = now;
                    _last_predict[j] = _last_update[j];

                } else {
                    // update
                    //LogLanding() << "Updating with POS_0 = " << _rel_pos(0) << "\t POS_1 = " << _rel_pos(1) << "\t Height = " << groundDistance;
                    
                    //We could use minus the movement of the drone instead (it actually the same but valid before...)
                    _timing_mtx.lock();
                    float dx = _timeBuffer[_timingBufferIndex].x[j] - _timeBuffer[measTimeIndex].x[j];
                    float dy = _timeBuffer[_timingBufferIndex].y[j] - _timeBuffer[measTimeIndex].y[j];
                    _timing_mtx.unlock();

                    // Adjust the position of the target from any movement between measurment and now.
                    if(isfinite(dx) && isfinite(dy)) {
                        _rel_pos[j](0) += dx;
                        _rel_pos[j](1) += dy;
                    }

                    bool update_x = _kalman_filter_x[j].update(_rel_pos[j](0), _params.meas_unc * groundDistance * groundDistance);
                    bool update_y = _kalman_filter_y[j].update(_rel_pos[j](1), _params.meas_unc * groundDistance * groundDistance);

                    if (!update_x || !update_y) {
                        if (!_faulty[j]) {
                            _faulty[j] = true;
                            LogWarn() << "Landing target measurement **" << j << "** rejected:" << (update_x ? "" : " x") << (update_y ? "" : " y");
                        }

                    } else {
                        if(_faulty[j]) {
                            _faulty[j] = false;
                            LogInfo() << "Landing target measurement **" << j << "** good.";
                        }

                        _last_update[j] = now;
                        _last_predict[j] = now;
                    }
                }
            }
        }
    }
}

void PrecisionLandingMode::updateBuffer() {
    Eigen::Quaternion<float, Eigen::DontAlign> currentAtt;
    _drone->getQuaternion(currentAtt);

    std::lock_guard<std::mutex> lock_t(_timing_mtx);

    for(int j = 0; j < wer::cargo::MAX_LANDING_TARGETS; j++) {
        if(_estimator_initialized[j]) {
            _timeBuffer[_timingBufferIndex].x[j] = _kalman_filter_x[j].getState();
            _timeBuffer[_timingBufferIndex].y[j] = _kalman_filter_y[j].getState();
        }
        else {
            _timeBuffer[_timingBufferIndex].x[j] = std::numeric_limits<float>::quiet_NaN();
            _timeBuffer[_timingBufferIndex].y[j] = std::numeric_limits<float>::quiet_NaN();
        }
    }

    _timeBuffer[_timingBufferIndex].q_att = currentAtt;
    _drone->getEstimatedGroundDistance(_timeBuffer[_timingBufferIndex].estimatedGroundDistance);
    _timeBuffer[_timingBufferIndex].measuredGroundDistance = _lidar->getMeasure();

    _timingBufferIndex++;
    _timingBufferIndex = _timingBufferIndex % TIME_BUFFER_SIZE;
}

void PrecisionLandingMode::update() {
    if(_active) {

        // In any case we try an update
        predict();

        // Update the time buffer
        updateBuffer();

        _report_mtx.lock();
        // Then is a new measurment is available try an update
        if(!_consumed) {

            uint8_t measTimeIndex;
            auto now = std::chrono::steady_clock::now();
            auto dtMeasure = std::chrono::duration_cast<std::chrono::milliseconds>(now - _targetReport.epochOfMeasure).count() / 20;
            
            if(dtMeasure < TIME_BUFFER_SIZE) {

                //Get the time index
                _timing_mtx.lock();
                if(_timingBufferIndex >= dtMeasure) {
                    measTimeIndex = (_timingBufferIndex - dtMeasure) % TIME_BUFFER_SIZE;
                }
                else {
                    measTimeIndex = TIME_BUFFER_SIZE - (dtMeasure - _timingBufferIndex);
                }
                if(measTimeIndex >= TIME_BUFFER_SIZE || measTimeIndex < 0) {
                    measTimeIndex = _timingBufferIndex;
                }

                //Get the altitude at landing target measure time
                float altitudeError = _timeBuffer[measTimeIndex].estimatedGroundDistance - _targetReport.distance;
                _timing_mtx.unlock();

                if(isfinite(altitudeError) && fabsf(altitudeError) < MAX_ALTITUDE_ERROR) {
                    //Compute time delta from last itteration of this loop.
                    float dt = std::chrono::duration_cast<std::chrono::milliseconds>(now - _lastMeasureTime).count() / 1000.0f;
                    _lastMeasureTime = now;

                    //Bound dt to 1/5 times the frequency cutoff.
                    if(dt > 2.0f) {
                        dt = 2.0f;
                    }

                    float fc = 0.1f;   //Frequency cutoff
                    float alpha = 2.0f * M_PI * fc * dt / (2.0f * M_PI * fc * dt + 1.0f);

                    alpha = alpha > 0.5f ? 0.5f : alpha;

                    // Filter the altitude offset (Offset only logged !!!)
                    _altEstiOffset =  alpha * altitudeError + (1.0f - alpha) * _altEstiOffset;
                }

                if(isfinite(_timeBuffer[measTimeIndex].measuredGroundDistance)) {
                    updateFilter(_timeBuffer[measTimeIndex].measuredGroundDistance, measTimeIndex);
                }
                else if(    isfinite(_timeBuffer[measTimeIndex].estimatedGroundDistance) 
                        &&  _timeBuffer[measTimeIndex].estimatedGroundDistance < 50.0f 
                        &&  _timeBuffer[measTimeIndex].estimatedGroundDistance > 20.0f) {
                    //FOR TEST ONLY BE CAREFULL ! (Use only if between 50m and 20m) Could be use extensively if good altitude offset computation
                    updateFilter(_timeBuffer[measTimeIndex].estimatedGroundDistance, measTimeIndex);
                }
                else {
                    updateFilter(_timeBuffer[measTimeIndex].measuredGroundDistance, measTimeIndex);
                }
            }
            else {
                LogWarn() << "Measurment is too old to be used (" << unsigned(dtMeasure) * 20 << "ms), time buffer not large enough.";
            }
            _consumed = true;
        }
        _report_mtx.unlock();

        float x = std::numeric_limits<float>::quiet_NaN();
        float y = std::numeric_limits<float>::quiet_NaN();

        //If there is a valid estimation let's use it
        for(int j = 0; j < wer::cargo::MAX_LANDING_TARGETS; j++) {
            if(_estimator_initialized[j] && _kalman_filter_y[j].getCovariance() < _params.max_var  && _kalman_filter_x[j].getCovariance() < _params.max_var) {
                x = _kalman_filter_x[j].getState();
                y = _kalman_filter_y[j].getState();
                break;
            }
        }

        float groundDistance, estimatedGroundDistance;
        //Update ground distances
        _drone->getEstimatedGroundDistance(estimatedGroundDistance);
        groundDistance = _lidar->getMeasure();

        control(x, y, groundDistance, estimatedGroundDistance);

        /*
        double targetPosition[2] = {x, y};
        float targetCov[6];
        for(int j = 0; j < wer::cargo::MAX_LANDING_TARGETS; j++) {
            targetCov[j*2] = _kalman_filter_x[j].getCovariance();
            targetCov[j*2+1] = _kalman_filter_y[j].getCovariance();
        }

        FlightLog::getInstance()->logPrecisionLanding(targetPosition, targetCov);
        */

    }
}

void PrecisionLandingMode::control(float x, float y, float groundDistance, float estimatedGroundDistance) {
    float velX = 0.0f, velY = 0.0f, velZ = 0.0f;
    static uint8_t last_mode = 0;
    uint8_t mode = 0;

    if(_forceLanding) {
        _groundDistanceError = false;

        _resetTimeoutCb();

        //We don't have ground distance but we know we are close to ground let's land whatever happen.
        velZ = FINAL_LANDING_V_SPEED;

        if(std::isfinite(x) && std::isfinite(y)) {
            mode = 6;
            _beaconError = false;
            //We have estimated target position let's follow it.
            velX = control_p(x, FINAL_LANDING_GAIN, FINAL_LANDING_H_SPEED);
            velY = control_p(y, FINAL_LANDING_GAIN, FINAL_LANDING_H_SPEED);
        }
        else {
            mode = 7;
            //We don't have traget position let's land straight.
            velX = 0.0f;
            velY = 0.0f;
        }
    }
    else if(std::isfinite(groundDistance) && std::isfinite(x) && std::isfinite(y)) {
        float distance = sqrtf(pow(x, 2) + pow(y, 2));
        _groundDistanceError = false;
        _beaconError = false;

        _resetTimeoutCb();

        //Everything is fine let's control normally
        if(groundDistance < FINAL_LANDING_HEIGHT) {
            mode = 1;
            //Are we to low to check the distance with the target ?
            _forceLanding = true;
            velX = control_p(x, FINAL_LANDING_GAIN, FINAL_LANDING_H_SPEED);
            velY = control_p(y, FINAL_LANDING_GAIN, FINAL_LANDING_H_SPEED);
            velZ = FINAL_LANDING_V_SPEED;
        }
        else if(groundDistance < DESCENT_LANDING_HEIGHT) {
            mode = 2;

            float v_coef = (groundDistance - FINAL_LANDING_HEIGHT) / (DESCENT_LANDING_HEIGHT - FINAL_LANDING_HEIGHT);
            float currrent_radius_1 = FINAL_LANDING_RADIUS + v_coef * (DESCENT_LANDING_RADIUS_1 - FINAL_LANDING_RADIUS);
            float currrent_radius_2 = FINAL_LANDING_RADIUS + v_coef * (DESCENT_LANDING_RADIUS_2 - FINAL_LANDING_RADIUS);

            float h_coef = 1.0f;
            if(distance > currrent_radius_1) {
                if(distance > currrent_radius_2) {
                    h_coef = 0.0f;
                }
                else {
                    h_coef = 1.0f - ((distance - currrent_radius_1) / (currrent_radius_2 - currrent_radius_1));
                }
            }

            velX = control_p(x, DESCENT_LANDING_GAIN, FINAL_LANDING_H_SPEED);
            velY = control_p(y, DESCENT_LANDING_GAIN, FINAL_LANDING_H_SPEED);
            velZ = h_coef * (FINAL_LANDING_V_SPEED + v_coef * (DESCENT_LANDING_V_SPEED - FINAL_LANDING_V_SPEED));
        }
        else if(groundDistance < APPROACH_LANDING_HEIGHT) {
            mode = 3;
            //We detect the beacons from quiet a high altiude.
                        //We detect the beacons from a high altiude, let's center before going down
            if(distance > DESCENT_LANDING_RADIUS_3) {
                velZ = 0.0f;
            }
            else if(distance > DESCENT_LANDING_RADIUS_1){
                float h_coef = 1.0f - ((distance - DESCENT_LANDING_RADIUS_1) / (DESCENT_LANDING_RADIUS_3 - DESCENT_LANDING_RADIUS_1));
                velZ = h_coef * DESCENT_LANDING_V_SPEED;
            }
            else {
                velZ = DESCENT_LANDING_V_SPEED;
            }

            velX = control_p(x, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
            velY = control_p(y, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
        }
        else {
            /*
            velX = control_p(x, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
            velY = control_p(y, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
            */
            //TODO : do we trust this measurment ???? not trust the measurment we are way to high...
            velX = 0.0f;
            velY = 0.0f;
            velZ = DESCENT_LANDING_V_SPEED;
            LogWarn() << "Lidar higher than approach altitude, should not happen.";
        }
    }
    else if(std::isfinite(groundDistance)) {
        _groundDistanceError = false;

        //We have ground distance but no lock on target estimation.
        if(groundDistance > APPROACH_LANDING_HEIGHT) {
            mode = 4;

            _resetTimeoutCb();
            //We are still height so let's go down and hope we will find the beacon.
            velZ = APPROACH_LANDING_V_SPEED;
            velX = 0.0f;
            velY = 0.0f;
        }
        else if(groundDistance > DESCENT_LANDING_HEIGHT) {
            mode = 21;

            _resetTimeoutCb();
            //We are still height so let's go down and hope we will find the beacon.
            velZ = DESCENT_LANDING_V_SPEED;
            velX = 0.0f;
            velY = 0.0f;
        }
        else if(groundDistance < OBS_HEIGHT) {
            //We might be to low to detect the marker. Let's climb a bit.
            mode = 20;
            
            velZ = OBS_CLIMB_V_SPEED;
            velX = 0.0f;
            velY = 0.0f;
        }
        else {
            mode = 5;
            //This is not good... The altitude is low but not beacons detected.
            if(_beaconError == false) {
                LogErr() << "Precision landing : no beacon detected altitude low";
                _beaconError = true;
            }
            velZ = 0.0f;
            velX = 0.0f;
            velY = 0.0f;
        }
    }
    else if(std::isfinite(estimatedGroundDistance) && estimatedGroundDistance > APPROACH_LANDING_HEIGHT) {
        _groundDistanceError = false;

        _resetTimeoutCb();

        //We estimate to be to high to get groundDistance. Let's go down if no beacon, go closer if beacon.
        

        if(std::isfinite(x) && std::isfinite(y)) {
            float distance = sqrtf(pow(x, 2) + pow(y, 2));
            mode = 8;
            _beaconError = false;

            if(distance < DESCENT_LANDING_RADIUS_2) {
                velZ = APPROACH_LANDING_V_SPEED;
            }
            else if(distance < DESCENT_LANDING_RADIUS_3) {
                velZ = DESCENT_LANDING_V_SPEED;
            }
            else {
                velZ = 0.0f;
            }
            
            //If the target estimation is there let's still move into that direction slowly.
            velX = control_p(x, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
            velY = control_p(y, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
        }
        else {
            mode = 9;
            velZ = APPROACH_LANDING_V_SPEED;
            //We don't have traget position let's go down straight.
            velX = 0.0f;
            velY = 0.0f;
        }
    }
    else if(std::isfinite(estimatedGroundDistance) && estimatedGroundDistance > DESCENT_LANDING_HEIGHT && std::isfinite(x) && std::isfinite(y)) {
        float distance = sqrtf(pow(x, 2) + pow(y, 2));
        mode = 12;
        _beaconError = false;

        _resetTimeoutCb();

        if(distance < DESCENT_LANDING_RADIUS_2) {
            velZ = DESCENT_LANDING_V_SPEED;
        }
        else {
            velZ = 0.0f;
        }

        velX = control_p(x, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
        velY = control_p(y, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
    }
    else if(std::isfinite(estimatedGroundDistance) && estimatedGroundDistance > DESCENT_LANDING_HEIGHT) {
        mode = 13;

        _resetTimeoutCb();

        velZ = DESCENT_LANDING_V_SPEED;
        velX = 0.0f;
        velY = 0.0f;
    }
    else {
        mode = 10;
        //CASE 1 : We don't have estimated ground distance -> Strange
        //CASE 2 : We are lower than the approach altitude (8 meters)
        //          - The lidar (still not detect | does not detect anymore) the ground -> Strange
        //          - The lidar is to close to ground to get a measurment but the force landing was not triggered.
        if(_groundDistanceError == false) {
            _groundDistanceError = true;
            LogErr() << "Precision landing : no ground distance";
        }
        velZ = 0.0f;
        velX = 0.0f;
        velY = 0.0f;
    }

    float vx, vy, vz;
    _drone->getVelocity(vx, vy, vz);

    std::stringstream logData;
    logData   << ";" << unsigned(mode)
              << ";" << x << ";" << y <<";" 
              << vx << ";"<< vy << ";" 
              << velX << ";" << velY << ";" << velZ << ";" 
              << groundDistance << ";" << estimatedGroundDistance << ";" << _altEstiOffset;

    for(int j = 0; j < wer::cargo::MAX_LANDING_TARGETS; j++) {
        logData << ";" << _kalman_filter_x[j].getState()
                << ";" << _kalman_filter_y[j].getState()
                << ";" << _rel_pos[j](0)
                << ";" << _rel_pos[j](1)
                << ";" << _kalman_filter_x[j].getCovariance()
                << ";" << _kalman_filter_y[j].getCovariance();
    }

    LogLanding() << logData.str();

    if(last_mode != mode) {
        last_mode = mode;
        //FlightLog::getInstance()->logPrecisionLandingMode(mode);
    }

    if(_missionPaused) {
        _drone->setManualSpAbs(0.0f, 0.0f, 0.0f, 0.0f);
    }
    else {
        _drone->setManualSpAbs(velX, velY, velZ, 0.0f);
    }
    //_drone->setManualSpAbs(velX, velY, 0.0f, 0.0f);
}

float PrecisionLandingMode::control_p(float erreur, float gain, float limit) {
    float cmd = erreur * gain;
    if(cmd > limit) {
        return limit;
    }
    else if(cmd < -limit) {
        return -limit;
    }
    else {
        return cmd;
    }
}

void PrecisionLandingMode::send(float x, float y, float z) {
    mavlink_debug_vect_t vector;

    vector.x = x;
    vector.y = y;
    vector.z = z;

    mavlink_message_t message;
    mavlink_msg_debug_vect_encode(wer::com::Handler::get()->getThisSysId(), wer::com::Handler::get()->getThisCompId(), &message, &vector);

    wer::com::Handler::get()->sendMessage(message);
}

/*
uint8_t PrecisionLandingMode::align(Waypoint pos_sp, float vSpeed) {
    float vel = sqrtf(pow(_velocity(0), 2) + pow(_velocity(1), 2) + pow(_velocity(2), 2));
    
    float hDistToWp = get_distance_to_next_waypoint(_position.latitude, _position.longitude, 
                                                    pos_sp.location.latitude, pos_sp.location.longitude);
    float bearingToWp = get_bearing_to_next_waypoint(_position.latitude, _position.longitude, 
                                                    pos_sp.location.latitude, pos_sp.location.longitude);

    float velX = control_p(cos(bearingToWp) + hDistToWp, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);
    float velY = control_p(sin(bearingToWp) + hDistToWp, DESCENT_LANDING_GAIN, DESCENT_LANDING_H_SPEED);

}
*/