#include "landing_mode.h"

#include <Library/logging/log.h>

LandingMode::LandingMode(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone) :
ControlMode(lidar, drone),
_landingStep(0),
_landingAltitude(std::numeric_limits<float>::quiet_NaN()),
_approachSpeed(DEFAULT_APPROACH_SPEED),
_descentSpeed(DEFAULT_DESCENT_SPEED),
_finalSpeed(DEFAULT_FINAL_SPEED)
{

}

void LandingMode::reset() {
	_approachSpeed = DEFAULT_APPROACH_SPEED;
	_descentSpeed = DEFAULT_DESCENT_SPEED;
	_finalSpeed = DEFAULT_FINAL_SPEED;
	_landingAltitude = std::numeric_limits<float>::quiet_NaN();
	_landingStep = 0;
}

uint8_t LandingMode::start() {
	_missionPaused = false;
	return 0;
}

uint8_t LandingMode::stop() {
	reset();
	return 0;
}

uint8_t LandingMode::pause() {
	_missionPaused = true;
	return 0;
}

uint8_t LandingMode::resume() {
	_missionPaused = false;
	return 0;
}

void LandingMode::setLandingAltitude(float altitude) {
	_landingAltitude = altitude;
}

void LandingMode::setApproachSpeed(float speed) {
	_approachSpeed = speed;
}

void LandingMode::setDescentSpeed(float speed) {
	_descentSpeed = speed;
}

void LandingMode::setFinalSpeed(float speed) {
	_finalSpeed = speed;
}

void LandingMode::update() {

	if(_missionPaused) {
		_drone->setManualSpAbs(0.0f, 0.0f, 0.0f, 0.0f);
	}
	else {
		float altitudeToGround = std::numeric_limits<float>::quiet_NaN();

		if(_lidar) {
			altitudeToGround = _lidar->getMeasure();
		}

		LogLanding() << "Landing lidar = " << altitudeToGround;

		if(!std::isfinite(altitudeToGround) && std::isfinite(_landingAltitude)) {
			altitudeToGround = _position.altitude - _landingAltitude;
			LogLanding() << "Landing calculated = " << altitudeToGround;
		}

		float velZ;
		if(std::isfinite(altitudeToGround)) {
			if((altitudeToGround > 20.0 && _landingStep == 0) || (altitudeToGround > 23.0 )) {
				_landingStep = 0;
				velZ = _approachSpeed;
			}
			else if((altitudeToGround > 4.0 && _landingStep == 1) || (altitudeToGround > 5.0 )) {
				_landingStep = 1;
				velZ = _descentSpeed;
			}
			else {
				_landingStep = 2;
				velZ = _finalSpeed;
			}
		}
		else {
			velZ = _descentSpeed;
		}

		_drone->setManualSpAbs(0.0f, 0.0f, velZ, 0.0f);
	}
}
