#include "manual_mission.h"

#include <chrono>
#include <thread>

#include <Library/logging/log.h>
#include <Library/geo.h>

#include "../geo_helper.h"

#define POS_P_GAIN				0.6f
#define NAV_P_GAIN				0.6f

#define V_P_GAIN				0.3f
#define H_P_GAIN				0.3f
#define V_MAX_SPEED				3.0f
#define NAV_ACCEPTANCE_RADIUS	10.0f
#define WP_ACCEPTANCE_RADIUS	0.7f
#define YAW_RATE_MAX			50.0f
#define YAW_RATE_P_GAIN			0.5f
#define TRACKING_DIST			5.0f

#define TAKEOFF_TIMEOUT			10.0f

#define M_PI_F     ((float)M_PI)
#define M_PI_2_F   ((float)M_PI_2)
#define FLT_EPSILON 1.1920929e-07F  /* 1E-5 */

using namespace std::chrono;

ManualMission::ManualMission(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::function<void(uint8_t)> wpReachedCb, LandingMode *landingMode) :
BaseMission(lidar, drone, wpReachedCb),
_missionState(STOPPED),
_landingMode(landingMode),
_altitudeReached(false),
_positionReached(false),
_lastPrecise(false)
{

}

uint8_t ManualMission::start() {
	_lastPrecise = false;
	_drone->disactivateOffset();

	if(_drone->landed()) {
		if(_drone->takeoff()) {
			LogErr() << "Drone refused to takeoff, restart the drone.";
			return 1;
		}

		auto cmdTime = steady_clock::now();

		while(_drone->landed() && duration_cast<std::chrono::seconds>(steady_clock::now() - cmdTime).count() < TAKEOFF_TIMEOUT) {
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}

		if(_drone->landed()) {
			LogErr() << "Takeoff not detected, restart the drone.";
			_drone->land();
			return 1;
		}

		LogInfo() << "Mission start : Climbing";
		_missionState = CLIMBING;
		_altitudeReached = false;
	}
	else {
		_missionState = RUNNING;
		LogInfo() << "Mission start : Running";
	}
	//TODO check if we have authority

	return 0;
}

uint8_t ManualMission::pause() {
	MissionState temp = _missionState;
	_lastMissionState = temp;
	_missionState = PAUSED;
	LogInfo() << "Mission : Paused";
	return 0;
}

uint8_t ManualMission::resume() {
	MissionState temp = _lastMissionState;
	_missionState = temp;
	LogInfo() << "Mission : Resumed";
	return 0;
}

uint8_t ManualMission::stop() {
	_missionState = STOPPED;
	_drone->disactivateOffset();
	LogInfo() << "Mission : Stopped";
	return 0;
}

void ManualMission::waypointReached(uint8_t waypointIndex) {
	LogInfo() << "Mission : Call to waypoint reached " << unsigned(waypointIndex);
	_wpReachedCb(waypointIndex);

	if(waypointIndex + 1 != _missionIndex && waypointIndex + 1 < _missionSize) {
		_missionIndex = waypointIndex + 1;
		LogInfo() << "Waypoint increased.";

		if(_missionIndex + 1 < _missionSize) {
			//This is not the last waypoint let's check the type.
			if(_mission[_missionIndex].type == WP_LANDING) {
				// If we are now on a landing waypoint let initialize the mode.
				_landingMode->reset();
				_landingMode->setLandingAltitude(_mission[_missionIndex].groundAltitude);
				LogInfo() << "Mission : Init landing";

				_positionReached = false;
			}
			else if(_drone->landed() && _mission[_missionIndex].type == WP_TAKEOFF) {
				if(_drone->takeoff()) {
					_altitudeReached = false;
					LogInfo() << "Mission : Takeoff";
				}
				else {
					LogErr() << "Unable to takeoff.";
				}
			}
		}
	}
}

uint8_t ManualMission::uploadMission(std::vector<Waypoint> mission, GpsPosition plannedHome, float cruiseSpeed) {
	_cruiseSpeed = cruiseSpeed;
	_plannedHome = plannedHome;
	_mission = mission;

	_missionIndex = 0;
	_missionSize = _mission.size();

	_droneMaxVSpeed = V_MAX_SPEED;
	_droneMaxHSpeed = _cruiseSpeed;
	_droneMaxTotalSpeed = sqrtf(pow(_droneMaxVSpeed, 2) + pow(_droneMaxHSpeed, 2));

	return 0;
}

uint8_t ManualMission::recoverMission(std::vector<Waypoint> mission, GpsPosition plannedHome, float cruiseSpeed, uint8_t missionIndex) {
	uploadMission(mission, plannedHome, cruiseSpeed);
	_missionIndex = missionIndex;

	if(_missionIndex == 0) {
		_missionState = CLIMBING;
	}
	else {
		_missionState = RUNNING;
		if(_mission[_missionIndex].type == WP_LANDING && _missionIndex + 1 < _missionSize) {
			_landingMode->reset();
			_landingMode->setLandingAltitude(_mission[_missionIndex].groundAltitude);
		}
	}

	return 0;
}

void ManualMission::update() {
	if(_missionState == RUNNING && _mission[_missionIndex].type != WP_TAKEOFF) {
		//TODO check if running, check other waypoint type
		if(_mission[_missionIndex].type == WP_WAYPOINT || (_mission[_missionIndex].type == WP_LANDING && _missionIndex == _missionSize-1)) {
			// We have a standard waypoint or a land waypoint at the end of a mission (then we are not responsible of handling it, just noticed the waypoint is reached)
			bool doPrecisePos = (_missionIndex == _missionSize - 1);
			//if(_missionIndex == 0) {
			if(goGoToPosition(_mission[_missionIndex], doPrecisePos)) {
				// We just reached the waypoint
				LogInfo() << "At WP 1.";
				waypointReached(_missionIndex);
			}
			/*}
			else {
				if(followLine(_mission[_missionIndex-1], _mission[_missionIndex], doPrecisePos)) {
					// We just reached the waypoint
					LogInfo() << "At WP 2.";
					waypointReached(_missionIndex);
				}
			}*/
		}
		else if(_mission[_missionIndex].type == WP_LANDING) {
			// The landing mode should be initialized from the waypoint reached function.
			if(_positionReached) {
				_landingMode->cycle();

				if(_drone->landed()) {
					_landingMode->stop();
					LogInfo() << "At WP 3.";
					waypointReached(_missionIndex);
				}
			}
			else {
				_positionReached = goGoToPosition(_mission[_missionIndex], true);
				//_positionReached = followLine(_mission[_missionIndex-1], _mission[_missionIndex], true);
				if(_positionReached) {
					_landingMode->start();
				}
			}
		}
	}
	// Equivalent to a takeoff waypoint but a mission could miss the first takeoff
	else if(_missionState == CLIMBING || (_mission[_missionIndex].type == WP_TAKEOFF && _missionState == RUNNING)) {
		if(_altitudeReached) {
			if(goGoToPosition(_mission[_missionIndex], false)) {
				_missionState = RUNNING;
				LogInfo() << "Takeoff WP reached.";
				LogInfo() << "At WP 4.";
				waypointReached(_missionIndex);
			}
		}
		else {
			_altitudeReached = climbTo(_mission[_missionIndex].location.altitude);
			if(_altitudeReached) {
				LogInfo() << "Altitude reached.";
			}
		}
	}
	else if(_missionState == PAUSED) {
		_drone->setManualSpAbs(0.0f, 0.0f, 0.0f, 0.0f);
	}
}

uint8_t ManualMission::climbTo(float altitude) {
	float alt_diff = altitude - _position.altitude;
	float vz = limit(alt_diff * V_P_GAIN, V_MAX_SPEED);
	_drone->setManualSpAbs(0.0f, 0.0f, vz, 0.0f);

	if(alt_diff < WP_ACCEPTANCE_RADIUS) {
		return 1;
	}
	else {
		return 0;
	}
}
/*
uint8_t ManualMission::goGoToPosition(Waypoint pos_sp, bool precise) {
	// P-position controller

	// TODO dynamic acceptance radius.

	float alt_diff = pos_sp.location.altitude - _position.altitude;

	//float dist = calculateDistance(pos, pos_sp);
	float dist = get_distance_to_next_waypoint(_position.latitude, _position.longitude, pos_sp.location.latitude, pos_sp.location.longitude);
	float bearing = get_bearing_to_next_waypoint(_position.latitude, _position.longitude, pos_sp.location.latitude, pos_sp.location.longitude);

	float time_to_dest_v = fabs(alt_diff) / V_MAX_SPEED;
	float time_to_dest_h = dist / _cruiseSpeed;
	
	float heading_err = degrees(wrap_pi<float>(bearing - _attitude(2)));

	float vx, vy, vz, yawRate;

	if(time_to_dest_v > time_to_dest_h) {
		// The altitude is the limitting factor
		vz = limit(alt_diff * V_P_GAIN, V_MAX_SPEED);

		if(dist * H_P_GAIN < dist / time_to_dest_v) {
			vx = cos(bearing) * limit(dist * H_P_GAIN, _cruiseSpeed);
			vy = sin(bearing) * limit(dist * H_P_GAIN, _cruiseSpeed);
		}
		else {
			vx = cos(bearing) * limit(dist / time_to_dest_v, _cruiseSpeed);
			vy = sin(bearing) * limit(dist / time_to_dest_v, _cruiseSpeed);
		}
	}
	else {
		// The distance is the limitting factor
		if(alt_diff * V_P_GAIN < alt_diff / time_to_dest_h) {
			vz = limit(alt_diff * V_P_GAIN, V_MAX_SPEED);
		}
		else {
			vz = limit(alt_diff / time_to_dest_h, V_MAX_SPEED);
		}

		vx = cos(bearing) * limit(dist * H_P_GAIN, _cruiseSpeed);
		vy = sin(bearing) * limit(dist * H_P_GAIN, _cruiseSpeed);
	}

	yawRate = limit(heading_err * YAW_RATE_P_GAIN, YAW_RATE_MAX);

	_drone->setManualSpAbs(vx, vy, vz, yawRate);

	
	//static int i_log = 0;
	//if(i_log == 10) {
//		i_log = 0;
		//LogInfo() << "ManulSP X:" << vx << "\tY:" << vy << "\tZ:" << vz << "\tYaw:" << yawRate << "\t dist:" << dist << "\tbearing:" << degrees(bearing) << "\theading_err:" << heading_err;
	//}
	//i_log++;
	
	

	if(precise && sqrtf(dist*dist+alt_diff*alt_diff) < WP_ACCEPTANCE_RADIUS) {
		return true;
	}
	else if(!precise && sqrtf(dist*dist+alt_diff*alt_diff) < NAV_ACCEPTANCE_RADIUS) {
		return true;
	}
	else {
		return false;
	}
}
*/
/*
uint8_t ManualMission::followLine(Waypoint prev_sp, Waypoint pos_sp, bool precise) {

	float actualToGoalDistance = get_distance_to_next_waypoint(_position.latitude, _position.longitude, pos_sp.location.latitude, pos_sp.location.longitude);
	float actualToGoalBearing = get_bearing_to_next_waypoint(_position.latitude, _position.longitude, pos_sp.location.latitude, pos_sp.location.longitude);
	
	float previousToGoalDistance = get_distance_to_next_waypoint(prev_sp.location.latitude, prev_sp.location.longitude, pos_sp.location.latitude, pos_sp.location.longitude);
	float previousToGoalBearing = get_bearing_to_next_waypoint(prev_sp.location.latitude, prev_sp.location.longitude, pos_sp.location.latitude, pos_sp.location.longitude);
	float previousToGoalAltitude = pos_sp.location.altitude - prev_sp.location.altitude;

	if(previousToGoalDistance < fabsf(previousToGoalAltitude)) {
		//Garde de fou
		return goGoToPosition(pos_sp, precise);
	}

	// If we are at the point return true
	if (actualToGoalDistance < 0.1f) {
		_drone->setManualSpAbs(0.0f, 0.0f, 0.0f, 0.0f);
		LogInfo() << "Too close to destinsation.";
		return true;
	}

	float bearing_diff = wrap_pi(previousToGoalBearing - actualToGoalBearing);

	// We passed the waypoint... let's try to go back with a position control
	if (bearing_diff > M_PI_2_F || bearing_diff < -M_PI_2_F) {
		return goGoToPosition(pos_sp, precise);
	}

	float vDistToGoal = pos_sp.location.altitude - _position.altitude;
	float hDistToGoalOnLine = actualToGoalDistance * cosf(bearing_diff);
	float totalDistToGoOnline = sqrtf(pow(hDistToGoalOnLine, 2) + pow(vDistToGoal,2));

	precise &= totalDistToGoOnline < _droneMaxTotalSpeed / H_P_GAIN;

	//Let's create a waypoint in front of us on the line
	if(precise) {
		return goGoToPosition(pos_sp, true);
	}
	else {
		double provWpLat, provWpLon;

		float distanceTraveledFromPrevious = previousToGoalDistance - hDistToGoalOnLine;

		create_waypoint_from_line_and_dist(prev_sp.location.latitude, prev_sp.location.longitude, 
										   pos_sp.location.latitude, pos_sp.location.longitude, 
										   distanceTraveledFromPrevious + _cruiseSpeed / H_P_GAIN, 
										   &provWpLat, &provWpLon);

		float provWpAlt = prev_sp.location.altitude + previousToGoalAltitude * (distanceTraveledFromPrevious + TRACKING_DIST) / previousToGoalDistance;

		float hDistToWpOnLine = get_distance_to_next_waypoint(_position.latitude, _position.longitude, provWpLat, provWpLon);
		float bearingToWpOnLine = get_bearing_to_next_waypoint(_position.latitude, _position.longitude, provWpLat, provWpLon);
		float altDistToWpOnLine = provWpAlt - _position.altitude;

		float totalDist = sqrtf(pow(hDistToWpOnLine, 2) + pow(altDistToWpOnLine,2));
		float vSpeed = altDistToWpOnLine / totalDist * _droneMaxTotalSpeed;
		float hSpeed = hDistToWpOnLine / totalDist * _droneMaxTotalSpeed;

		if(vSpeed > _droneMaxVSpeed || vSpeed < -_droneMaxVSpeed) {
			hSpeed = _droneMaxVSpeed / vSpeed * hSpeed;
			vSpeed = vSpeed > 0 ? _droneMaxVSpeed : -_droneMaxVSpeed;
		}
		else if(hSpeed > _droneMaxHSpeed) {
			vSpeed = _droneMaxHSpeed / hSpeed * vSpeed;
			hSpeed = _droneMaxHSpeed;
		}

		_drone->setManualSpAbs(cos(bearingToWpOnLine) * hSpeed, sin(bearingToWpOnLine) * hSpeed, vSpeed, 0.0f);

		if(totalDistToGoOnline < NAV_ACCEPTANCE_RADIUS) {
			return true;
		}
		else {
			return false;
		}
	}

	//static int i_log = 0;
	//if(i_log == 5) {
	//	i_log = 0;
		//LogInfo() << "TargetOnLine:" << distanceToGoalOnLine << "\tTargetBearing:" << previousToGoalBearing << "\ttoTarget:" << actualToGoalBearing << "\tTraveled:" << distanceTraveledFromPrevious << "\t dist2wp:" << previousToGoalDistance << "\tbearingDiff:" << degrees(bearing_diff) << "\t attitude:" << degrees(_attitude(2));
	//}
	//i_log++;
}*/

uint8_t ManualMission::goGoToPosition(Waypoint pos_sp, bool precise) {
	float vel = sqrtf(pow(_velocity(0), 2) + pow(_velocity(1), 2) + pow(_velocity(2), 2));
	
	float vDistToWp = pos_sp.location.altitude - _position.altitude;
	float hDistToWp = get_distance_to_next_waypoint(_position.latitude, _position.longitude, 
													pos_sp.location.latitude, pos_sp.location.longitude);
	float bearingToWp = get_bearing_to_next_waypoint(_position.latitude, _position.longitude, 
													pos_sp.location.latitude, pos_sp.location.longitude);

	float totalDist = sqrtf(pow(hDistToWp, 2) + pow(vDistToWp,2));

	float droneMaxHSpeed = _droneMaxHSpeed;
	float droneMaxVSpeed = _droneMaxVSpeed;
	float droneMaxTotalSpeed = _droneMaxTotalSpeed;

	float gain;
	if(precise) {
		if(totalDist > 150.0f) {
			droneMaxHSpeed = 18.0f < _droneMaxHSpeed ? 18.0f : _droneMaxHSpeed;
		}
		else if(totalDist > 50.0f){
			droneMaxHSpeed = 10.0f < _droneMaxHSpeed ? 10.0f : _droneMaxHSpeed;
		}
		else if(totalDist > 5.0f){
			droneMaxHSpeed = 5.0f < _droneMaxHSpeed ? 5.0f : _droneMaxHSpeed;
		}
		else {
			droneMaxHSpeed = 1.0f;
		}

		gain = POS_P_GAIN;
		droneMaxTotalSpeed = sqrtf(pow(droneMaxHSpeed, 2) + pow(droneMaxVSpeed, 2));
	}
	else {
		gain = NAV_P_GAIN;
	}

	if(precise == true && _lastPrecise == false && totalDist < 30.0f) {
		_drone->activateOffset();
		_lastPrecise = true;
	}
	else if(precise == false && _lastPrecise == true) {
		_drone->disactivateOffset();
		_lastPrecise = false;
	}

	float vSpeed, hSpeed;
	if(totalDist > droneMaxTotalSpeed / gain) {
		vSpeed = vDistToWp / totalDist * droneMaxTotalSpeed;
		hSpeed = hDistToWp / totalDist * droneMaxTotalSpeed;
	}
	else {
		vSpeed = vDistToWp * gain;
		hSpeed = hDistToWp * gain;
	}

	if(fabsf(vSpeed) > _droneMaxVSpeed) {
		hSpeed = _droneMaxVSpeed / fabsf(vSpeed) * hSpeed;
		vSpeed = vSpeed > 0 ? _droneMaxVSpeed : -_droneMaxVSpeed;
	}
	else if(hSpeed > _droneMaxHSpeed) {
		vSpeed = _droneMaxHSpeed / hSpeed * vSpeed;
		hSpeed = _droneMaxHSpeed;
	}

	float heading_err = degrees(wrap_pi<float>(bearingToWp - _attitude(2)));
	float yawRate = limit(heading_err * YAW_RATE_P_GAIN, YAW_RATE_MAX);

	if(totalDist < 10.0f) {
		yawRate = 0.0f;
	}

	_drone->setManualSpAbs(cos(bearingToWp) * hSpeed, sin(bearingToWp) * hSpeed, vSpeed, yawRate);

	static int i_log = 0;
	if(i_log == 30) {
		i_log = 0;
		LogInfo() << "ManulSP X:" << cos(bearingToWp) * hSpeed << "\tY:" << sin(bearingToWp) * hSpeed 
				  << "\tZ:" << vSpeed << "\t dist:" << totalDist << "\tbearing:" << degrees(bearingToWp) << "\tvel:" << vel;
	}
	i_log++;

	if(precise && totalDist < WP_ACCEPTANCE_RADIUS && vel < WP_ACCEPTANCE_RADIUS) {
		return true;
	}
	else if(!precise && hDistToWp < _droneMaxHSpeed && fabsf(vDistToWp) < _droneMaxVSpeed) {
		return true;
	}
	else {
		return false;
	}
}