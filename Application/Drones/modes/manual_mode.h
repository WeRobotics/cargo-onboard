#pragma once

#include <Communication/handler.h>
#include <Application/Drones/drone.h>
#include <Application/Drones/control_mode.h>
#include <Hardware/include/distance_sensor.h>

class ManualMode : public ControlMode {
public:
	ManualMode(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::function<void(void)> _requestControlCb);
	~ManualMode();

	void runOnce();

	uint8_t start();
	uint8_t stop();

	uint8_t isActive();
private:
	void update() {};

	void processManualControl(const mavlink_message_t &message);
	void processCommandLong(const mavlink_message_t &message);

	float stickExp(int16_t input);

	std::function<void(void)> _requestControlCb;

	uint8_t _active = false;
};