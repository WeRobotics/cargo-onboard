#pragma once

#include "../base_mission.h"
#include "landing_mode.h"

enum MissionState {CLIMBING, RUNNING, PAUSED, STOPPED};

class ManualMission : public BaseMission {
public:
	ManualMission(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::function<void(uint8_t)> wpReachedCb, LandingMode *landingMode);

	uint8_t start();
	uint8_t pause();
	uint8_t resume();
	uint8_t stop();

	void waypointReached(uint8_t waypointIndex);

	uint8_t uploadMission(std::vector<Waypoint> mission, GpsPosition plannedHome, float cruiseSpeed);
	uint8_t recoverMission(std::vector<Waypoint> mission, GpsPosition plannedHome, float cruiseSpeed, uint8_t missionIndex);
private:
	void update();

	std::vector<Waypoint> _mission;
	GpsPosition _plannedHome;
	float _cruiseSpeed;
	uint8_t _missionIndex;
	uint8_t _missionSize;
	std::atomic<MissionState> _missionState;
	std::atomic<MissionState> _lastMissionState;

	LandingMode *_landingMode;

	bool _altitudeReached;
	bool _positionReached;

	float _droneMaxVSpeed;
	float _droneMaxHSpeed;
	float _droneMaxTotalSpeed;

	bool _lastPrecise;

	uint8_t goGoToPosition(Waypoint pos_sp, bool precise);
	//uint8_t followLine(Waypoint prev_sp, Waypoint pos_sp, bool precise);
	uint8_t climbTo(float altitude);
};