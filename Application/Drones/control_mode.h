#pragma once

#include <Library/eigen/Eigen/Geometry>

#include <Hardware/include/distance_sensor.h>

#include "drone.h"
#include "location_struct.h"

class ControlMode {
public:
	ControlMode(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone) {
		_lidar = lidar;
		_drone = drone;
	};

	virtual uint8_t start() = 0;
	virtual uint8_t pause() {return 0;};
	virtual uint8_t resume() {return 0;};
	virtual uint8_t stop() = 0;

	void cycle() {
		_drone->getPosition(_position.latitude, _position.longitude, _position.altitude);
		_drone->getAttitude(_attitude(0), _attitude(1), _attitude(2));
		_drone->getVelocity(_velocity(0), _velocity(1), _velocity(2));
		_drone->getMode(_flightStatus, _controlMode);

		update();
	};

protected:
	virtual void update() = 0;
	
	GpsPosition _position;
	Eigen::Matrix<float, 3, 1, Eigen::DontAlign> _attitude;
	Eigen::Matrix<float, 3, 1, Eigen::DontAlign> _velocity;
	uint8_t _controlMode;
	uint8_t _flightStatus;

	bool _active;

	std::shared_ptr<wer::hal::DistanceSensor> _lidar;
	std::shared_ptr<Drone> _drone;
};