#include "mission_manager.h"

#include <limits>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <Library/logging/log.h>
//#include <Library/logging/flight_log.h>
#include <Application/state_recovery.h>

#include "../geo_helper.h"

#define DEFAULT_WP_RADIUS 15.0f
#define DISTANCE_PER_BAT_PERCENTAGE 185

#define RALLYPOINT_CRUISE_SPEED 10.0f

#define H_ABS_MAX_SPEED	18.0f
#define H_DEFAULT_SPEED 18.0f

CargoMissionManager::CargoMissionManager(std::shared_ptr<wer::hal::DistanceSensor> lidar, std::shared_ptr<Drone> drone, std::shared_ptr<MissionStateData> missionData) :
_missionData(missionData),
_terrain_altitude(std::numeric_limits<float>::quiet_NaN()),
_landingMode(lidar, drone),
_djiMission(lidar, drone, std::bind(&CargoMissionManager::waypointReached, this, std::placeholders::_1)),
_manualMission(lidar, drone, std::bind(&CargoMissionManager::waypointReached, this, std::placeholders::_1), &_landingMode),
_actualMode(nullptr),
_lidar(lidar),
_drone(drone)
{

}

void CargoMissionManager::update() {
	if(_actualMode) {
		//WaypointType old_type = _mission[_missionIndex].type;
		_actualMode->cycle();
		checkSecurity();
	}
}

uint8_t CargoMissionManager::start() {
	if(_actualMode) {
		if(_actualMode->start()) {
			return 1;	
		}
		else {
			_lidar->enable();
			return 0;
		}
	}
	return 1;
}

uint8_t CargoMissionManager::pause() {
	if(_actualMode) {
		if(_actualMode->pause()) {
			return 1;	
		}
		else {
			return 0;
		}
	}
	return 1;
}

uint8_t CargoMissionManager::resume() {
	if(_actualMode) {
				if(_actualMode->resume()) {
			return 1;	
		}
		else {
			return 0;
		}
	}
	return 1;
}

uint8_t CargoMissionManager::stop() {
	if(_actualMode) {
		if(_actualMode->stop()) {
			return 1;	
		}
		else {
			_lidar->disable();
			_mission.clear();
			_actualMode = nullptr;
			return 0;
		}
	}
	return 1;
}

uint8_t CargoMissionManager::initEmergencyLanding() {
	_landingMode.reset();
	_landingMode.setDescentSpeed(-2.0f);

	_actualMode = &_landingMode;
	return 0;
}

uint8_t CargoMissionManager::initLanding() {
	_landingMode.reset();

	_landingMode.setLandingAltitude(_terrain_altitude);

	_actualMode = &_landingMode;
	return 0;
}

uint8_t CargoMissionManager::initMission(std::string filename)  {
	if(loadWaypointMission(filename) || missionCheck()) {
		return 1;
	}
	else {
		if(_missionData->isDjiMission && !_djiMission.uploadMission(_mission, _plannedHomeMissionWp, _missionData->cruiseSpeed)) {
			LogInfo() << "Upload to dji drone sucess.";
			_actualMode = &_djiMission;
		}
		else if(!_manualMission.uploadMission(_mission, _plannedHomeMissionWp, _missionData->cruiseSpeed)) {
			_missionData->isDjiMission = false;
			StateRecovery::getInstance()->setMissionIsDji(_missionData->isDjiMission);
			LogInfo() << "Manual mission upload sucess.";
			_actualMode = &_manualMission;
		}
		else {
			return 1;
		}

		_missionIndex = 0;
		StateRecovery::getInstance()->setMissionIndex(_missionIndex);
		//FlightLog::getInstance()->logMissionStatus(_missionIndex);
		_missionSize = _mission.size();
		_missionData->finished = false;
		StateRecovery::getInstance()->setMissionIsFinished(_missionData->finished);

		_drone->makeAltitudeOffsetDroneToMap(_mission[_missionIndex].groundAltitude);
		setTerrainAltitude(_plannedHomeMissionWp.altitude);
		//setTerrainAltitude(_mission[_missionIndex].groundAltitude);

		//The drone should be ready at this point

		return 0;
	}
}

uint8_t CargoMissionManager::recoverMission(std::string missionFile, uint8_t missionIndex, float terrainAltitude, bool missionIsDji, bool missionIsFinished) {
	if(loadWaypointMission(missionFile)) {
		return 1;
	}
	else {
		_missionData->isDjiMission = missionIsDji;
		StateRecovery::getInstance()->setMissionIsDji(_missionData->isDjiMission);
		_missionData->finished = missionIsFinished;
		StateRecovery::getInstance()->setMissionIsFinished(_missionData->finished);

		_missionSize = _mission.size();
		_missionIndex = missionIndex;
		StateRecovery::getInstance()->setMissionIndex(_missionIndex);
		//FlightLog::getInstance()->logMissionStatus(_missionIndex);
		setTerrainAltitude(terrainAltitude);

		if(!_missionData->isDjiMission) {
			_manualMission.recoverMission(_mission, _plannedHomeMissionWp, _missionData->cruiseSpeed, missionIndex);

			_actualMode = &_manualMission;
		}
		else {
			_djiMission.inRecovery();
			_actualMode = &_djiMission;
		}
		return 0;
	}
}

uint8_t CargoMissionManager::initRallypoint()  {
	if(_rallypoints.size() > 0) {
		float cruiseSpeed = RALLYPOINT_CRUISE_SPEED;

		std::vector<Waypoint> mission;
		
		Waypoint closestRp;
		getClosestRallyPoint(closestRp);
		closestRp.type = WP_WAYPOINT;

		// We need the closest rally point
		Waypoint waypoint;
		waypoint.radius = DEFAULT_WP_RADIUS;
		waypoint.speed = cruiseSpeed;
		waypoint.groundAltitude = std::numeric_limits<float>::quiet_NaN();
		waypoint.type = WP_WAYPOINT;

		LogInfo() << "Closest Rally Point : "
		<< "\tLat : " << closestRp.location.latitude
		<< "\tLon : " << closestRp.location.longitude
		<< "\tAlt : " << closestRp.location.altitude;

		updatePosition();

		// First mission waypoint is the actual location with a safe approach altitude 
		waypoint.location.latitude = _position.latitude;
		waypoint.location.longitude = _position.longitude;
		waypoint.location.altitude = closestRp.location.altitude > _position.altitude ? closestRp.location.altitude : _position.altitude;

		closestRp.location.altitude = waypoint.location.altitude;

		mission.push_back(waypoint);
		mission.push_back(closestRp);

		_missionIndex = 0;
		StateRecovery::getInstance()->setMissionIndex(_missionIndex);
		//FlightLog::getInstance()->logMissionStatus(_missionIndex);
		_missionSize = _mission.size();
		_actualMode = &_manualMission;

		_missionData->precisionLanding = false;
		_missionData->isDjiMission = false;
		StateRecovery::getInstance()->setMissionIsDji(_missionData->isDjiMission);
		_missionData->cruiseSpeed = cruiseSpeed;
		_mission = mission;

		if(_manualMission.uploadMission(_mission, _plannedHomeMissionWp, _missionData->cruiseSpeed)) {
			LogErr() << "Rallypoint upload failed.";
			return true;
		}
		else {
			LogInfo() << "Rallypoint mission created successfuly.";
			_actualMode = &_manualMission;

			_missionIndex = 0;
			StateRecovery::getInstance()->setMissionIndex(_missionIndex);
			//FlightLog::getInstance()->logMissionStatus(_missionIndex);
			_missionSize = _mission.size();
			_missionData->finished = false;
			StateRecovery::getInstance()->setMissionIsFinished(_missionData->finished);

			setTerrainAltitude(closestRp.groundAltitude);

			return false;
		}
	}
	else {
		LogErr() << "Failed to initialize Rally points : No rally point specified... Switching to emergency landing.";
		return true;
	}
}

void CargoMissionManager::waypointReached(uint8_t waypointIndex) {
	if(_missionIndex != waypointIndex + 1 && waypointIndex < _missionSize) {
		_missionIndex = waypointIndex + 1;
		StateRecovery::getInstance()->setMissionIndex(_missionIndex);
		//FlightLog::getInstance()->logMissionStatus(_missionIndex);

		if((_missionIndex == _missionSize - 1 && _missionData->isDjiMission) || _missionIndex == _missionSize) {
			LogInfo() << "Waypoint "<< unsigned(_missionIndex) << "/" << unsigned(_missionSize) << " reached. Requesting landing.";
			setTerrainAltitude(_mission[_missionSize - 1].groundAltitude);
			_missionData->finished = true;
			StateRecovery::getInstance()->setMissionIsFinished(_missionData->finished);
		}
		else {
			
			LogInfo() << "Waypoint " << unsigned(_missionIndex) << "/" << unsigned(_missionSize) << " reached.";
			setTerrainAltitude(_mission[_missionIndex].groundAltitude);
		}
	}
}

float CargoMissionManager::distanceToClosestRallyPoint() {
	GpsPosition pos1;
	GpsPosition pos2;
	//TODO initialize with home distance ???
	float minDistance = -1.0f;

	updatePosition();

	pos1.latitude = _position.latitude;
	pos1.longitude = _position.longitude;
	pos1.altitude = _position.altitude;

	for(auto it = _rallypoints.begin(); it != _rallypoints.end(); it++) {
		pos2.latitude = it->location.latitude;
		pos2.longitude = it->location.longitude;
		pos2.altitude = it->location.altitude;

		float tempDistance = calculateDistance(pos1, pos2);

		if(tempDistance < minDistance || minDistance < 0.0f) {
			minDistance = tempDistance;
		}
	}

	return minDistance;
}

void CargoMissionManager::getClosestRallyPoint(Waypoint &closestRallyPoint) {
	GpsPosition pos1;
	GpsPosition pos2;
	//TODO initialize with home distance ???
	float minDistance = -1.0f;

	updatePosition();

	pos1.latitude = _position.latitude;
	pos1.longitude = _position.longitude;
	pos1.altitude = _position.altitude;

	for(auto it = _rallypoints.begin(); it != _rallypoints.end(); it++) {
		pos2.latitude = it->location.latitude;
		pos2.longitude = it->location.longitude;
		pos2.altitude = it->location.altitude;

		float tempDistance = calculateDistance(pos1, pos2);

		if(tempDistance < minDistance || minDistance < 0.0f) {
			minDistance = tempDistance;
			closestRallyPoint = *it;
		}
	}
}

void CargoMissionManager::getRemainingDistanceAndTime(float &distance, uint64_t &tof) {
	distance = remainingMissionDistance();
	_drone->getTimeOfFlight(tof);
}

void CargoMissionManager::getEstimatedGroundDistance(float &distance) {
	distance = _position.altitude - _terrain_altitude;
}

void CargoMissionManager::setTerrainAltitude(float altitude) {
	if(std::isfinite(altitude)) {
		_terrain_altitude = altitude;
		_drone->setTerrainAltitudeFromMap(_terrain_altitude);
		StateRecovery::getInstance()->setTerrainAltitude(_terrain_altitude);
		//FlightLog::getInstance()->logGroundAltitude(_terrain_altitude);
	}
}

void CargoMissionManager::checkSecurity() {

	_drone->checkSecurity();

	float distanceToGoal = remainingMissionDistance();

	uint8_t battery, errorCount, errorStatus;
	_drone->getStatus(battery, errorCount, errorStatus);
	
	if(distanceToGoal > (battery - EMERGENCY_BAT_THRESHOLD) * DISTANCE_PER_BAT_PERCENTAGE) {
		if(_missionData->destinationUnreachable != true) {
			_missionData->destinationUnreachable = true;
			LogWarn() << "[WARN] Destination is unreachable (" << distanceToGoal 
					  << " meters) with the remaining battery : "<< unsigned(battery)
					  << "%.";
		}
	}
	else {
		if(_missionData->destinationUnreachable != false) {
			_missionData->destinationUnreachable = false;
			LogWarn() << "Destination is now reachable with the remaining battery.";
		}
	}
}

float CargoMissionManager::remainingMissionDistance() {
	float distance = 0.0f;
	if(_mission.size() > 0) {
		GpsPosition* pos1 = new GpsPosition();
		GpsPosition* pos2 = new GpsPosition();

		updatePosition();

		pos1->latitude = _position.latitude;
		pos1->longitude = _position.longitude;
		pos1->altitude = _position.altitude;
		
		//calculating distance from current waypoint to end waypoint
		uint8_t index = 0;
		for(auto it = _mission.begin(); it != _mission.end(); it++) {
			if(index >= _missionIndex) {
				pos2->latitude = it->location.latitude;
				pos2->longitude = it->location.longitude;
				pos2->altitude = it->location.altitude;

				distance += calculateDistance(*pos1, *pos2);

				//Switch pointer, let be computional friendly
				GpsPosition* temp = pos2;
				pos2 = pos1;
				pos1 = temp;
			}
			index++;
		}

		delete pos1;
		delete pos2;

		//std::cout << "Remaining missionDistance " << distance << std::endl;
	}
	return distance;
}

uint8_t CargoMissionManager::missionCheck() {
	updatePosition();

	float distDroneToMission = calculateDistance(_position, _plannedHomeMissionWp);

	checkSecurity();

	if(_drone->checkSecurity()) {
		LogErr() << "Drone issue, probably battery is low.";
		return true;
	}
	else if(distDroneToMission > 50.f) {
		LogErr() << "Drone is more than 50 meters of planned home position (" << distDroneToMission << " meters).";
		return true;
	}
	else if(_missionData->destinationUnreachable) {
		LogErr() << "Destination is to far with remaining battery. Not starting.";
		return true;
	}
	else {
		LogInfo() << "All test passed. Drone at " << distDroneToMission << " meters from planned home.";
	}

	return false;
}

void CargoMissionManager::updatePosition() {
	_drone->getPosition(_position.latitude, _position.longitude, _position.altitude);
}

uint8_t CargoMissionManager::loadWaypointMission(std::string filename) {
	//TODO : Error management
	namespace pt = boost::property_tree;
	pt::ptree root;
	
	try{
		pt::read_json(filename, root);
		LogInfo() << "Reading json successful";
	}
	catch(std::exception &e){
		LogErr() << "Error while reading json";
		LogErr() << e.what();
		return 1;
	}

	try{
		
		// Get the home position
		int x = 0;
		GpsPosition home;
		for(pt::ptree::value_type &item : root.get_child("mission.plannedHomePosition")) {
			if(x == 0) {
				home.latitude = (item.second.get_value<float>()*M_PI)/180;
			}
			if(x == 1) {
				home.longitude = (item.second.get_value<float>()*M_PI)/180;
			}
			if(x == 2) {
				home.altitude = item.second.get_value<float>();
			}
			x++;
		}

		LogInfo() << "Home "
		  << "Lat : " << home.latitude
		  << "\tLon : " << home.longitude
		  << "\tAlt : " << home.altitude;

		// Get the cruise speed
		double cruiseSpeed = root.get<double>("mission.cruiseSpeed");
		if(cruiseSpeed < 0.0 || cruiseSpeed > H_ABS_MAX_SPEED) {
			cruiseSpeed = H_DEFAULT_SPEED;
		}
		
		// Intermediate mission storing
		std::vector<Waypoint> rallypoints;
		std::vector<Waypoint> mission;
		bool precisionLanding = false;
		bool isDjiMission = false;


		//iterator over all waypoints
		int wpIndex = 0;
		uint8_t last_command = 0;

		for(pt::ptree::value_type &item : root.get_child("mission.items")) {
			Waypoint waypoint;

			waypoint.radius = DEFAULT_WP_RADIUS;
			waypoint.speed = cruiseSpeed;

			uint8_t command = item.second.get<uint8_t>("command");
			uint8_t altMode = item.second.get<uint8_t>("AltitudeMode");
			double altAboveGround = item.second.get<double>("Altitude");



			if((command == 16 || command == 21 || command == 22) && (altMode == 2 || altMode == 3)) {	
		
				if(command == 16) {
					waypoint.type = WP_WAYPOINT;
				}
				else if(command == 21) {
					waypoint.type = WP_LANDING;
				}
				else {
					waypoint.type = WP_TAKEOFF;
				}

				last_command = command;

				int z = 0;
				// This is a standard waypoint
				for(pt::ptree::value_type &param : item.second.get_child("params")) {
					//TODO : Replace with math functions
					if(z == 4) {
						waypoint.location.latitude  = (param.second.get_value<double>()*M_PI)/180;
					}
					if(z == 5) {
						waypoint.location.longitude  = (param.second.get_value<double>()*M_PI)/180;
					}
					if(z == 6) {
						waypoint.location.altitude  = param.second.get_value<double>();
						break;
					}
					z++;
				}

				waypoint.groundAltitude = waypoint.location.altitude - altAboveGround;

				if(waypoint.location.altitude < home.altitude) {
					if(isDjiMission) {
						isDjiMission = false;
						LogWarn() << "The mission will not be a DJI mission.";
					}
				}
				else if(waypoint.location.altitude - home.altitude > 500.0f) {
					LogWarn() << "Mission is higher than the 500 meters limit.";
				}


				LogInfo() << "Waypoint " << wpIndex
					  << " Lat : " << static_cast<float>(waypoint.location.latitude)
					  << "\tLon : " << static_cast<float>(waypoint.location.longitude)
					  << "\tAlt : " << static_cast<float>(waypoint.location.altitude);

				mission.push_back(waypoint);
				wpIndex++;
			}
			else if((command == 195) && (altMode == 2 || altMode == 3)) {

				waypoint.type = WP_NOT_DEFINED;

				int z = 0;
				for(pt::ptree::value_type &param : item.second.get_child("params")) {
					//TODO : Replace with math functions
					if(z == 4) {
						waypoint.location.latitude = param.second.get_value<double>()*M_PI/180;
					}
					if(z == 5) {
						waypoint.location.longitude = param.second.get_value<double>()*M_PI/180;
					}
					if(z == 6) {
						waypoint.location.altitude = param.second.get_value<double>();
					}
					z++;
				}

				LogInfo() << "Rally point "
						  << "Lat : " << waypoint.location.latitude
						  << "\tLon : " << waypoint.location.longitude
						  << "\tAlt : " << waypoint.location.altitude
						  << "\tGAlt : " << waypoint.groundAltitude;

				rallypoints.push_back(waypoint);
			}
			else if(command == 178) {
				if(wpIndex == 0) {
					int z = 0;
					for(pt::ptree::value_type &param : item.second.get_child("params")) {
						if(z == 1) {
							//cruiseSpeed = param.second.get_value<float>();
							LogInfo() << "Mission speed set to : " << cruiseSpeed << "m/s.";
						}
						z++;
					}
				}
			}
			else {
				LogErr() << "Waypoint not correctly formatted, aborting mission loading...";
				return 1;
			}
		}

		if(last_command == 21) {
			precisionLanding = true;
		}

		LogInfo() << "Mission item loaded.";

		// Initialization of the waypoint missions parameters
		_missionData->precisionLanding = precisionLanding;
		_missionData->isDjiMission = isDjiMission;
		StateRecovery::getInstance()->setMissionIsDji(_missionData->isDjiMission);
		_missionData->cruiseSpeed = cruiseSpeed;
		//_missionData->cruiseSpeed = 18.0f;

		LogWarn() << "Mission loaded, cruise speed is" << _missionData->cruiseSpeed << "m/s.";

		_plannedHomeMissionWp = home;

		//TODO : Append ???
		_rallypoints = rallypoints;

		// Saving the loaded mission
		_mission = mission;

		return 0;
	}
	catch(std::exception &e){
		LogErr() << "JSON Error : Mission is badly formated.";
		LogErr() << e.what();
		return 1;
	}
}