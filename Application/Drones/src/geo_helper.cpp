#include "../geo_helper.h"

#include <cmath>

static constexpr float EARTH_RADIUS = 6371000.f;

float limit(float data, float limit) {
	if(data > limit)
		return limit;
	else if(data < -limit)
		return -limit;
	else
		return data;
}

//distance is in meters
//probably some radians/deg issues
float calculateDistance(GpsPosition pos1, GpsPosition pos2) {
	
	// float phi_1 = toRadians(pos1.latitude);
	// float phi_2 = toRadians(pos2.latitude);
	// float delta_phi = toRadians(pos2.latitude-pos1.latitude);
	// float delta_lambda = toRadians(pos2.longitude-pos1.longitude);

	float phi_1 = (pos1.latitude);
	float phi_2 = (pos2.latitude);
	float delta_phi = (pos2.latitude-pos1.latitude);
	float delta_lambda = (pos2.longitude-pos1.longitude);

	float a = sinf(delta_phi/2) * sinf(delta_phi/2) +
	        cosf(phi_1) * cosf(phi_2) *
	        sinf(delta_lambda/2) * sinf(delta_lambda/2);
	float c = 2 * atan2f(sqrtf(a), sqrtf(1-a));

	return EARTH_RADIUS * c;
}

float toRadians(float angledeg) {
	return angledeg * M_PI / 180.0f;
}

float toDegree(float anglerad) {
	return anglerad / M_PI * 180.0f;
}