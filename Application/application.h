#pragma once

#include <atomic>

#include <Hardware/include/hardware.h>

#include <Application/instances.h>
#include <Application/commander.h>

namespace wer {
	namespace cargo {

		class Application {
		public:
			Application(std::shared_ptr<Instances> instances, 
						std::shared_ptr<Commander> commander,
						std::atomic<uint8_t>* shouldExit);
			~Application();

			void run();
			void start();
			void stop();

			void cargoShieldMonitoring(WER::HW::IO pin, WER::HW::INTERFACE::GpioEvent event);

		private:
			void systemThread();
			void sensorsThread();
			void stateThread();
			void droneThread();

			void shutdownButtonPressed();
			void buttonInterval(uint8_t interval);

			std::atomic<uint8_t>* _shouldExit;
			std::shared_ptr<Instances> _instances;
			std::shared_ptr<Commander> _commander;

			std::thread* _sensorsThread;
			std::thread* _systemThread;
			std::thread* _stateThread;
			std::thread* _droneThread;

			void* _updateDroneDataCookie;
			void* _sendGlobalPositionCookie;
			void* _sendSystemStatusCookie;
			void* _sendSensorCookie;

			void* _button_int_cookie;
		};
	}
}
/*
void sensorReading();
void recoveryStart();
*/

/*
* Cookie for function which need to be call periodically
*/
/*
void* _updateDroneBroadcastedData_cookie;
void* _sendGlobalPosition_cookie;
void* _sendSystemStatus_cookie;
void* _missionMonitoring_cookie;
void* _receiveMission_cookie;
void *_pingCookie;
void *_sensorCookie;
void *_sendSensor_cookie;
void *_powerButton_cookie;
void *_log_cookie;

//Precision landing
void* _landingPredict_cookie;
void* _irlockMeasure_cookie;
void* _lidarMeasure_cookie;

//Drone related cookie
void* _m100VirtualRC_cookie;

//State timeout cookie
void* _manualControl_cookie;
void* _precisionLanding_cookie;
*/

/*
* Mission upload trough Mavlink protocol (Image transmission protocol)
*/
/*
bool _missionReceptionStarted;
uint8_t _missionReceptionSeq;
uint32_t _missionReceptionSize;
uint8_t _missionReceptionPack;
uint8_t _missionReceptionId;
std::ofstream _missionReceptionFile;

void startReceiveMission(mavlink_message_t message);
void receiveMission(mavlink_message_t message);
void receiveMissionTimeout();
*/