## Description

OnBoard application for the cargo project. The code is written in C++.

## Dependencies

sudo apt install build-essential cmake git libncurses-dev libboost-container-dev wiringpi
sudo usermod -a -G dialout $USER

## Builing instructions

mkdir build 
cd build  
cmake ..  
make  

## Excution instructions

Once the program is build :  
cd bin  
./app ground_station_connection dji_config_file  

The ground station connection url should look like this :  

UDP 	udp://[Bind_host][:Bind_port]  
TCP 	tcp://[Server_host][:Server_port]  
Serial 	serial://[Dev_Node][:Baudrate]  

The DJI config file should have a txt extension and be placed in the same directory as the executable.  
Standart format for the file :  

>app_id : 1047461  
>app_key : 8fa4e7222eb8d33c192be7adaf18f9710e926d120e4d7c4a86f25e2479a9166d  
>device : /dev/ttyUSB0  
>baudrate : 230400

## Autostart service

>sudo cp ./Scripts/wer-cargo-onboard.servoce /home/etc/systemd/system/
>sudo systemctl daemon-reload
>sudo systemctl enable wer-cargo-onboard
>sudo systemctl start wer-cargo-onboard

To see the logs : 
>journalctl -u wer-cargo-onboard

## Grant permission for auto poweroff (ssh trick)

1. Generate a ssh key :
>ssh-keygen
2. Copy the generated key to allowed keys :
>ssh-copy-id -i /root/.ssh/rsa.pub pi@localhost

## Modify screen
Add the following lines to /etc/screenrc
- multiuser on
- acladd pi
- acladd root
