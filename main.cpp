#include <Application/configuration.h>
#include <Application/application.h>

#include <Hardware/include/hardware.h>

#include <iostream>
#include <unistd.h>
#include <thread>
#include <atomic>
#include <csignal>

void signalHandler(int sig);

std::atomic<uint8_t> should_exit{false};

int main(int argc, char const *argv[])
{
	if(argc >= 2) {

		signal(SIGTERM, signalHandler);
		signal(SIGINT, signalHandler);
		signal(SIGQUIT, signalHandler);

		WER::Hardware::get()->init();

		wer::lib::ProcessResult result;

		wer::cargo::Configuration applicationConfig;
		result = applicationConfig.load(argc, argv);

		if(result != wer::lib::ProcessResult::SUCCESS) {
			return 1;
		}

		wer::cargo::Application app(applicationConfig.getInstances(),
									applicationConfig.getCommander(),
									&should_exit);

		app.run();
	}

	if(should_exit == 2) { 
		//system("poweroff&");
		system("ssh pi@localhost \"(sleep 1; sudo poweroff)&\"");
	}
	
	return 0;
}

void signalHandler(int sig) {
	should_exit = true;
}

void configureHardware() {

}
